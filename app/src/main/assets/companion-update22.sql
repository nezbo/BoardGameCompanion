ALTER TABLE 'game' ADD 'users_rated' integer;
UPDATE 'game' SET 'users_rated'=50;
CREATE TABLE 'link' ('_id' integer primary key autoincrement, 'type' text, 'game_id' integer, 'name' text, 'value' integer);
DROP TABLE 'category';
DROP TABLE 'mechanic';
-- Clear the games, they lost their mechanics and categories, not worth moving
DELETE FROM 'game';