ALTER TABLE 'challenge' ADD 'type' integer;
UPDATE 'challenge' SET 'type'=0;
-- Challenge Type id 1
CREATE TABLE 'alphabet_challenge_row' ('_id' integer primary key autoincrement, 'challenge_id' integer, 'letter' integer, 'game_id' integer);
-- Challenge Type id 2 (reusing challenge table)
