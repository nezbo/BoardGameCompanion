ALTER TABLE 'challenge' RENAME TO 'challenge_row';
ALTER TABLE 'challenge_row' ADD 'challenge_id' integer;
UPDATE 'challenge_row' SET 'challenge_id'=0;
CREATE TABLE 'challenge' ('_id' integer primary key autoincrement, 'name' text, 'games' integer, 'times' integer);
INSERT INTO 'challenge' ('_id', 'name', 'games', 'times') VALUES (0, 'new challenge', 10, 10);