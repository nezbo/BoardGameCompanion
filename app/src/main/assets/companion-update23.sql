-- Collection Challenge - Type id 3
CREATE TABLE 'collection_challenge_row' ('_id' integer primary key autoincrement, 'challenge_id' integer, 'game_id' integer);
-- Allow for individual start time setting on each Challenge
ALTER TABLE 'challenge' ADD 'start' boolean;
ALTER TABLE 'challenge' ADD 'manual' boolean;
UPDATE 'challenge' SET 'start'=0;
UPDATE 'challenge' SET 'manual'=0;