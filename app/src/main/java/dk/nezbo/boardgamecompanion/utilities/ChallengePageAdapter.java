package dk.nezbo.boardgamecompanion.utilities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.challenges.ChallengeFragment;
import dk.nezbo.boardgamecompanion.ui.ChallengeNewFragment;
import dk.nezbo.boardgamecompanion.ui.challenges.AlphabetFragment;
import dk.nezbo.boardgamecompanion.ui.challenges.CollectionChallengeFragment;
import dk.nezbo.boardgamecompanion.ui.challenges.UniqueChallengeFragment;

/**
 * The adapter responsible for instantiating the different types of Challenges.
 */
public class ChallengePageAdapter extends FragmentStatePagerAdapter implements ChallengeNewFragment.NewChallengeListener {

    private List<CompanionDAO.Challenge> challenges;
    private final BGGAndroidCache dao;
    private ChallengesChangedListener listener;

    /**
     * The constructor with required arguments to function.
     * @param fm The FragmentManager for the super class.
     * @param dao An open BGGAndroidCache to fetch metadata for the Challenges.
     */
    public ChallengePageAdapter(FragmentManager fm, BGGAndroidCache dao) {
        super(fm);
        this.challenges = dao.getChallenges();
        this.dao = dao;
    }

    /**
     * Gets the Challenge instances the Adapter is currently handling.
     * @return A list of Challenges fetched from the database.
     */
    public List<CompanionDAO.Challenge> getChallenges() {
        return challenges;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == this.challenges.size()) {
            return "+";
        }
        return challenges.get(position).getName().trim();
    }

    @Override
    public int getCount() {
        return this.challenges.size() + 1;
    }

    @Override
    public Fragment getItem(int position) {
        System.out.println("Instantiating pos = " + position);
        if (position == this.challenges.size()) {
            return ChallengeNewFragment.newInstance(this);
        } else {
            CompanionDAO.Challenge challenge = challenges.get(position);
            switch(challenge.getType()) {
                case 0: return ChallengeFragment.newInstance(challenge.getID());
                case 1: return AlphabetFragment.newInstance(challenge.getID());
                case 2: return UniqueChallengeFragment.newInstance(challenge.getID());
                case 3: return CollectionChallengeFragment.newInstance(challenge.getID());
            }
        }
        return null;
    }

    @Override
    public void onCreateTenByTenChallenge(String name, int games, int times) {
        dao.putNxNChallenge(name, games, times);
        updateChallenges();
        if(this.listener != null)
            listener.onChallengeCreated(challenges.get(challenges.size() - 1), this.challenges.size() - 1); // Because it is inserted last
    }

    @Override
    public void onCreateAlphabetChallenge(String name) {
        dao.putAlphabetChallenge(name);
        updateChallenges();
        if(this.listener != null)
            listener.onChallengeCreated(challenges.get(challenges.size() - 1), this.challenges.size() - 1); // Because it is inserted last
    }

    @Override
    public void onCreateUniqueChallenge(String name, int games) {
        dao.putUniqueChallenge(name, games);
        updateChallenges();
        if(this.listener != null)
            listener.onChallengeCreated(challenges.get(challenges.size() - 1), this.challenges.size() - 1); // Because it is inserted last
    }

    @Override
    public void onCreateCollectionChallenge(String name) {
        dao.putCollectionChallenge(name);
        updateChallenges();
        if(this.listener != null)
            listener.onChallengeCreated(challenges.get(challenges.size() - 1), this.challenges.size() - 1); // Because it is inserted last
    }

    /**
     * Forces the Adapter to update the Challenge data from the database and notify that it might
     * have changed.
     */
    public void updateChallenges() {
        this.challenges = dao.getChallenges();
        this.notifyDataSetChanged();

    }

    /**
     * Tells how many Challenges are currently handled by the Adapter.
     * @return The number of Challenges currently handled by the Adapter.
     */
    public int getChallengesCount() {
        return this.challenges.size();
    }

    /**
     * The method for anyone interested in knowing when the Challenges are changed. There may
     * currently only be on.
     * @param listener The new listener for the challenges changed event.
     */
    public void setChallengesChangedListener(ChallengesChangedListener listener) {
        this.listener = listener;
    }

    /**
     * The required interface for anyone interested in responding to the challenges being updated.
     */
    public interface ChallengesChangedListener {
        void onChallengeCreated(CompanionDAO.Challenge challenge, int indexOfNewChallenge);
    }
}
