package dk.nezbo.boardgamecompanion.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.achievements.Achievement;

/**
 * Created by Emil on 28-06-2015.
 */
public class AchievementAdapter extends BaseAdapter {

    private List<Achievement> achievements;
    private final Context context;

    public AchievementAdapter(List<Achievement> achievements, Context context){

        this.achievements = achievements;
        this.context = context;
    }

    @Override
    public int getCount() {
        return achievements.size();
    }

    @Override
    public Object getItem(int position) {
        return achievements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Achievement cur = achievements.get(position);
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.achievement_item, null);
        }

        TextView title = (TextView) v.findViewById(R.id.tvAchTitle);
        TextView desc = (TextView) v.findViewById(R.id.tvAchDescription);
        TextView level = (TextView) v.findViewById(R.id.tvAchLevel);
        ProgressBar progress = (ProgressBar) v.findViewById(R.id.pbAchProgress);
        LinearLayout containerMatches = (LinearLayout) v.findViewById(R.id.llAchMatches);

        title.setText(cur.getTitle());
        desc.setText(cur.getDescription());
        level.setText((cur.getCompletedLevel() + 1) + " / " + cur.getLevels().length);
        progress.setMax(cur.getNextTarget());
        progress.setProgress(cur.getProgress());

        return v;
    }
}
