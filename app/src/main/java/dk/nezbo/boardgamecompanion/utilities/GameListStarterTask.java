package dk.nezbo.boardgamecompanion.utilities;

import android.content.Context;
import android.content.Intent;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.ui.BoardGameListActivity;

/**
 * Created by Emil on 09-08-2015.
 */
public class GameListStarterTask extends GamesLoaderTask {

    private final AdvancedList.Filter<BoardGame> filter;
    private final Comparator<BoardGame> orderBy;
    private final String title;
    private final Context c;

    public GameListStarterTask(Context c, boolean showProgress, String title, AdvancedList.Filter<BoardGame> filter, Comparator<BoardGame> orderBy) {
        super(c, showProgress);
        this.c = c;
        this.filter = filter;
        this.orderBy = orderBy;
        this.title = title;
    }

    @Override
    public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
        return cache.getCollection(Utilities.getUsername(c)).keySet();
    }

    @Override
    public AdvancedList<BoardGame> postLoadHandle(AdvancedList<BoardGame> initialGames, BGGAndroidCache cache) {
        AdvancedList<BoardGame> result = initialGames;
        if(filter != null) result = result.filter(filter);
        result = orderBy == null ? result.sort(Utilities.alphabeticallyComparator) : result.sort(orderBy);

        return result;
    }

    @Override
    public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
        // go to list view
        Intent i = new Intent(c, BoardGameListActivity.class);
        i.putExtra(BoardGameListActivity.BOARD_GAME_IDS_EXTRA, Utilities.getIds(loadedGames));
        i.putExtra("title",title);
        c.startActivity(i);
    }
}
