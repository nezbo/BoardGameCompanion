package dk.nezbo.boardgamecompanion.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.UserNotFoundException;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;

/**
 * Created by Emil on 14-03-2015.
 */
public abstract class GamesLoaderTask extends AsyncTask<Void, String, AdvancedList<BoardGame>> {


    private static GamesLoaderTask currentlyGoing = null;
    private static final int SYSTEM_STEPS = 2;

    public static boolean isLoaderActive(){
        return currentlyGoing != null;
    }

    // non-static

    private final Context context;
    private final BGGAndroidCache dao;
    private final boolean showProgress;

    private ProgressDialog pdialog;
    private Thread imageDownloader;
    private UserNotFoundException userNotFoundException = null;

    public GamesLoaderTask(Context c, boolean showProgress){
        this.context = c;
        this.dao = new BGGAndroidCache(c);
        this.showProgress = showProgress;

        if(isLoaderActive()){
            currentlyGoing.cancel(true);
            currentlyGoing = null;
        }
        currentlyGoing = this;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(showProgress){
            // create progress dialog
            pdialog = new ProgressDialog(context);
            pdialog.setCancelable(true);
            pdialog.setMessage("Searching");
            pdialog.setTitle("Please Wait");
            pdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pdialog.setProgress(0);
            pdialog.setMax(1);
            pdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    cancel(true);
                }
            });
            pdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    cancel(true);
                }
            });
            pdialog.show();
        }
    }

    @Override
    protected AdvancedList<BoardGame> doInBackground(Void... params) {
        try {
            Collection<Integer> integers = this.boardGamesToDownload(dao);
            if (integers == null)
                return null;


            List<Integer> ids = new ArrayList<>();
            ids.addAll(integers);
            ArrayList<BoardGame> games = new ArrayList<>();

            // we have to do it in batches
            int batch_size = showProgress ? Utilities.clamp(ids.size() / 10, 5, 25) : 50;
            int batches = (ids.size() + batch_size - 1) / batch_size;
            int max = ids.size();

            // progress
            publishProgress(ids.size() + " games selected", String.valueOf(1), String.valueOf(SYSTEM_STEPS + max));

            for (int batch = 0; batch < batches; batch++) {
                // check if the task has been cancelled
                if (isCancelled()) {
                    return null;
                }

                int startIndex = batch * batch_size;
                int endIndex = Math.min((batch + 1) * batch_size, max);

                long before = System.currentTimeMillis();
                // fetch detailed games
                AdvancedList<BoardGame> chunk = dao.getBoardGames(ids.subList(startIndex, endIndex));
                games.addAll(chunk);

                System.out.println("Time spend: " + (System.currentTimeMillis() - before) / 1000.0 + " sec");

                updateForGames(chunk, endIndex, max);
                //downloadImages(chunk); /* WE HAVE TO SAVE TIME DAMNED IT! */
            }

            // progress
            publishProgress("Looking at back of boxes", String.valueOf(SYSTEM_STEPS + max), String.valueOf(SYSTEM_STEPS + max));

            return postLoadHandle(new AdvancedList<BoardGame>(games), dao);
        } catch (IOException e) {
            return null;
        } catch (UserNotFoundException e) {
            this.userNotFoundException = e;
            return null;
        }
    }

    private void updateForGames(AdvancedList<BoardGame> chunk, int current, int max) {
        if(showProgress){
            for(int i = 0; i < chunk.size(); i++){
                if(chunk.get(i) != null) {
                    publishProgress(chunk.get(i).getName()+" fetched.",String.valueOf(current+2+i),String.valueOf(SYSTEM_STEPS + max));
                }
            }
        }
    }

    @Override                       // text, progress, max
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        if(showProgress) {
            String message = values[0];
            int progress = Integer.parseInt(values[1]);
            int max = Integer.parseInt(values[2]);

            pdialog.setMessage(message);
            pdialog.setProgress(progress);
            pdialog.setMax(max);
        }
    }

    @Override
    protected void onPostExecute(AdvancedList<BoardGame> boardGames) {
        super.onPostExecute(boardGames);
        currentlyGoing = null; // I am done now :P

        if(showProgress){
            pdialog.dismiss();
            pdialog = null;
        }
        if(boardGames != null){
            this.handleGamesInUIThread(boardGames,dao);
        }else if(userNotFoundException != null){
            Utilities.handleUserException(context,userNotFoundException);
        }else {

            Utilities.handleIOException(context);
        }
        dao.close();
    }

    @Override
    protected void onCancelled(AdvancedList<BoardGame> boardGames) {
        super.onCancelled(boardGames);
        if(showProgress && pdialog != null && pdialog.isShowing()){
            pdialog.dismiss();
            pdialog = null;
        }
        if(currentlyGoing == this){
            currentlyGoing = null;
        }
        dao.close();
        System.out.println("Games download task CANCELLED");
    }

    // TO IMPLEMENT

    /**
     * Collect the IDs to download. This is done in the async task.
     * @param cache You can use this object to get data from the database or internet
     * @return A collection of GameIDs to download afterwards (with dialog) in the
     * same thread.
     * @throws IOException Can throw this if something goes wrong, will be handled by
     * the super class. You're welcome!
     */
    public abstract Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException;

    /**
     * If you want to handle the games after loading, but still in the async task.
     * @param initialGames The complete list of loaded games.
     * @param cache You can use this object to get data from the database or internet
     * @return Your processed list of games.
     */
    public AdvancedList<BoardGame> postLoadHandle(AdvancedList<BoardGame> initialGames, BGGAndroidCache cache){ return initialGames; }

    /**
     * After loading the games async you can now do something with them in the UI
     * Thread, that is load the content to Views etc.
     * DON'T USE THE CACHE FOR INTERNET!
     * @param loadedGames The final BoardGame Objects from the loading/downloading
     *                    process.
     * @param cache You can use this object to get data from the database or internet
     */
    public abstract void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache);
}
