package dk.nezbo.boardgamecompanion.utilities;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import bggapi.api.AdvancedList;
import bggapi.model.Play;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.challenges.ChallengeFragment;

/**
 * Created by Emil on 3/10/2015.
 */
public class ChallengeStarsPopulator extends AsyncTask<Integer,Void,List<String>> {

    private final BGGAndroidCache cache;
    private final ChallengeFragment.ChallengeRowViews views;
    private final CompanionDAO.Challenge challenge;
    private Context context;

    public ChallengeStarsPopulator(Context context, CompanionDAO.Challenge challenge, ChallengeFragment.ChallengeRowViews views){
        this.context = context;
        this.cache = new BGGAndroidCache(context);
        this.challenge = challenge;
        this.views = views;
    }

    @Override
    protected List<String> doInBackground(Integer... gameId) {
        try{
            AdvancedList<Play> plays = Utilities.getPlays(false,cache,context);
            System.out.println("PLAYS: "+plays);

            plays = Utilities.getValidChallengePlays(plays,gameId[0],challenge.getStart(),context);
            System.out.println("PLAYS: "+plays);
            plays = plays.sort(new Comparator<Play>() {
                @Override
                public int compare(Play lhs, Play rhs) {
                    return lhs.compareTo(rhs);
                }
            },true);
            // make it into date strings (duplicates for multiple games pr play)
            return Utilities.getPlaysStrings(plays);
        }catch(IOException e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(final List<String> dates) {
        if(dates == null){
            Utilities.handleIOException(context);
        } else {
            views.updateStars(dates);
        }
        cache.close();
    }
}
