package dk.nezbo.boardgamecompanion.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;

/**
 * Created by Emil on 25-03-2015.
 */
public abstract class BoardGameAdapter extends BaseAdapter {

    private Context context;
    private int itemLayoutId;
    private AdvancedList<BoardGame> games;
    private BGGAndroidCache cache;
    private OnGameClickListener listener = null;

    public BoardGameAdapter(Context context, int itemLayoutId, AdvancedList<BoardGame> games, BGGAndroidCache cache){
        this.context = context;
        this.itemLayoutId = itemLayoutId;
        this.games = games;
        this.cache = cache;
    }

    public void addGames(AdvancedList<BoardGame> newGames){
        games = games.union(newGames);
    }

    // SIMPLE OVERRIDES

    /*@Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return position < games.size();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {}

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {}*/

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return games.get(position).getId();
    }
/*
    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return games.size() == 0;
    }*/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(itemLayoutId, null);
        }
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onClick(games.get(position),position);
            }
        });

        populateView(v, games.get(position), position, cache);

        return v;
    }

    public void changeBoardGames(AdvancedList<BoardGame> newGames){
        games = newGames;
    }

    public abstract void populateView(View root, BoardGame currentGame, int position, BGGAndroidCache cache);

    // OnClickListener

    public void setOnClickListener(OnGameClickListener listener){
        this.listener = listener;
    }

    public interface OnGameClickListener {
        void onClick(BoardGame game, int position);
    }
}
