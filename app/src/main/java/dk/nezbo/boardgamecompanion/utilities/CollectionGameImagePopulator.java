package dk.nezbo.boardgamecompanion.utilities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.widget.ImageView;

import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;

/**
 * Created by Emil on 24-02-2016.
 */
public class CollectionGameImagePopulator extends GameImagePopulator {

    private final ImageView toChange;
    private final int gameId;
    private final boolean selected;
    private final BGGAndroidCache dao;
    private final boolean animate;

    public CollectionGameImagePopulator(ImageView toChange, int gameId, boolean selected, boolean animate, BGGAndroidCache dao) {
        super(toChange, gameId, dao);
        this.toChange = toChange;
        this.gameId = gameId;
        this.selected = selected;
        this.animate = animate;
        this.dao = dao;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        if(!selected) {
            toChange.setImageBitmap(Utilities.toGrayscale(bitmap));
        }
        
        if(animate) {
            ObjectAnimator animation;
            if(!selected) {
                animation = ObjectAnimator.ofInt(toChange, "alpha", toChange.getImageAlpha(), 128);
            } else {
                animation = ObjectAnimator.ofInt(toChange, "alpha", toChange.getImageAlpha(), 255);
            }
            animation.setDuration(250);
            animation.start();
        } else {
            if(!selected) {
                toChange.setImageAlpha(128);
            } else {
                toChange.setImageAlpha(255);
            }
        }

    }
}
