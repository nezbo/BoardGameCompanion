package dk.nezbo.boardgamecompanion.utilities;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.util.HashMap;

import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.DBHelper;

/**
 * Created by Emil on 3/10/2015.
 */
public class GameImagePopulator extends AsyncTask<String, Void, Bitmap> {

    // static cancellation
    private static HashMap<ImageView,GameImagePopulator> map = new HashMap<>();

    private static void cancelLoad(ImageView image){
        if(map.containsKey(image)){
            map.get(image).cancel(true);
            map.remove(image);
        }
    }

    // actual class

    private final int gameId;
    private final BGGAndroidCache dao;
    private final ImageView view;

    public GameImagePopulator(ImageView view, int gameId, BGGAndroidCache dao) {
        this.view = view;
        this.dao = dao;
        this.gameId = gameId;

        cancelLoad(view);
        map.put(view,this);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try{
            return dao.getBitmap(gameId, DBHelper.BoardGameAttribute.thumb_url);

        }catch(NullPointerException e) { /* Too bad */ }
        return null;
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        super.onCancelled(bitmap);

        // do nothing
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(view != null && bitmap != null)
            view.setImageBitmap(bitmap);
        map.remove(view);
    }
}
