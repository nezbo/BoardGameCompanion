package dk.nezbo.boardgamecompanion.utilities;

/**
 * Created by Emil on 26-03-2015.
 */
public class StopWatch {

    private static long start;
    private static long lastTick;

    private StopWatch(){}

    public static void start(){
        start = System.currentTimeMillis();
        lastTick = start;
    }

    public static void tick(){
        tick("");
    }

    public static void tick(String title){
        long current = System.currentTimeMillis();
        System.out.println("["+title+"] Since Last: "+(current-lastTick)/1000.0+" sec - Total Time: "+(current-start)/1000.0+" sec");
        lastTick = System.currentTimeMillis();
    }
}
