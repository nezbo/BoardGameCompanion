package dk.nezbo.boardgamecompanion.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

/**
 * Created by Emil on 18-04-2015.
 */
public abstract class AsyncTaskExecutor extends AsyncTask<Void,String,Void> {

    private ProgressDialog pdialog = null;
    private int counter;

    public AsyncTaskExecutor(ProgressDialog dialog){
        this.pdialog = dialog;
        counter = 0;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(pdialog.isShowing()){
            pdialog.dismiss();
        }

        // Start Progress
        pdialog.setCancelable(true);
        pdialog.setMessage("Starting");
        pdialog.setTitle("Please Wait");
        pdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pdialog.setProgress(0);
        pdialog.setMax(1);
        pdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
            }
        });
        pdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                cancel(true);
            }
        });
        pdialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        pdialog.dismiss();
        pdialog = null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        String message = values[0];
        int progress = Integer.parseInt(values[1]);
        int max = Integer.parseInt(values[2]);

        pdialog.setMessage(message);
        pdialog.setProgress(progress);
        pdialog.setMax(max);
    }

    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);

        pdialog.dismiss();
        pdialog = null;
    }

    // USE THESE

    protected void updateProgress(String message, int newTotal){
        publishProgress(message,String.valueOf(counter),String.valueOf(newTotal));
        counter++;
    }
}
