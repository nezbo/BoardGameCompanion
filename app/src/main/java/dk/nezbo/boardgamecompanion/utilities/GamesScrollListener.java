package dk.nezbo.boardgamecompanion.utilities;

import android.widget.AbsListView;

import java.io.IOException;
import java.util.Collection;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;

/**
 * Created by Emil on 09-05-2015.
 */
public class GamesScrollListener implements AbsListView.OnScrollListener {

    private AdvancedList<Integer> gameIds;
    private int pageSize;
    private boolean loading = false;

    public GamesScrollListener(AdvancedList<Integer> gameIds, int pageSize){
        this.gameIds = gameIds;
        this.pageSize = pageSize;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}

    @Override
    public void onScroll(final AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
        if(firstVisibleItem + visibleItemCount >= totalItemCount){
            if(!loading){
                if(totalItemCount == gameIds.size()){
                    return; // nothing to load
                }
                if(!(view.getAdapter() instanceof BoardGameAdapter)){
                    // not the right adapter, so can't do anything anyway
                    System.err.println("GamesScrollListener COULD NOT INSERT GAMES (you need a BoardGameAdapter attached).");
                    return;
                }
                final BoardGameAdapter adapter = (BoardGameAdapter)view.getAdapter();

                loading = true;

                final int startIndex = adapter.getCount();
                final int endIndex = Math.min(totalItemCount + pageSize, gameIds.size());

                System.out.println("Scrolling for more: Current ("+totalItemCount+"/"+gameIds.size()+") getting "+(endIndex-startIndex));


                // All visible
                new GamesLoaderTask(view.getContext(),true){
                    @Override
                    public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                        return gameIds.subList(startIndex,endIndex);
                    }
                    @Override
                    public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                        adapter.addGames(loadedGames);
                        adapter.notifyDataSetChanged();
                        Utilities.preloadBitmaps(view.getContext(),loadedGames);
                        loading = false;
                    }
                }.execute();
            }
        }
    }
}
