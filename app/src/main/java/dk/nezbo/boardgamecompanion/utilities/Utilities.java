package dk.nezbo.boardgamecompanion.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.BoardGameWeight;
import bggapi.model.Experience;
import bggapi.model.Play;
import bggapi.model.Player;
import bggapi.model.UserNotFoundException;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.database.DBHelper;
import dk.nezbo.boardgamecompanion.ui.BoardGameActivity;
import dk.nezbo.boardgamecompanion.ui.BoardGameListActivity;
import dk.nezbo.boardgamecompanion.ui.InfiniteBoardGameListActivity;
import dk.nezbo.boardgamecompanion.ui.views.SquareImageButton;

/**
 * Created by Emil on 2/18/2015.
 */
public class Utilities {

    // HELPER CLASSES

    public static class IsThisYearFilter implements AdvancedList.Filter<Play> {

        private CompanionDAO.Challenge.ChallengeStart start;
        private boolean inclIncomplete;

        public IsThisYearFilter(CompanionDAO.Challenge.ChallengeStart start, boolean inclIncomplete){
            this.start = start;
            this.inclIncomplete = inclIncomplete;
        }

        @Override
        public boolean filter(Play play) {
            Calendar now = Calendar.getInstance();
            boolean thisYear = now.get(Calendar.YEAR) == play.getDate().get(Calendar.YEAR);

            return (thisYear || start == CompanionDAO.Challenge.ChallengeStart.All_Time) && (inclIncomplete || !play.isIncomplete());
        }
    }

    public static class IsGameFilter extends IsThisYearFilter {
        private final int gameId;

        public IsGameFilter(int gameId, CompanionDAO.Challenge.ChallengeStart start, boolean inclIncomplete){
            super(start,inclIncomplete);
            this.gameId = gameId;
        }

        @Override
        public boolean filter(Play play) {
            return super.filter(play) && play.getGameID() == gameId;
        }
    }

    // OBJECTS

    public static Comparator<BoardGame> alphabeticallyComparator = new Comparator<BoardGame>() {
        @Override
        public int compare(BoardGame lhs, BoardGame rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    };

    public static Comparator<BoardGame> rankComparator = new Comparator<BoardGame>(){
        @Override
        public int compare(BoardGame lhs, BoardGame rhs) {
            return (lhs.getRank() - rhs.getRank()) > 0 ? 1 : -1;
        }
    };

    public static Comparator<Play> timeComparator = new Comparator<Play>(){
        @Override
        public int compare(Play lhs, Play rhs) {
            return lhs.getDate().compareTo(rhs.getDate());
        }
    };

    public static DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static DateFormat longdateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    // HELPER METHODS

    public static String formatTime(int in_minutes) {
        if(in_minutes == 0) {
            return "0 minutes";
        }
        int hours = in_minutes / 60;
        int minutes = in_minutes % 60;
        return (hours > 0 ? hours + " hours " : " ") + (minutes > 0 ? minutes + " minutes" : "");
    }

    public static AdvancedList<Integer> arrayToAdvanced(int[] intArray) {
        ArrayList<Integer> list = new ArrayList<>(intArray.length);
        for(int i = 0; i < intArray.length; i++) {
            list.add(intArray[i]);
        }

        return new AdvancedList<>(list);
    }

    public static void populateBoardGameView(final Activity caller, View root, final BoardGame currentGame, int position, BGGAndroidCache cache) {
        ImageView image = (ImageView) root.findViewById(R.id.imageView);
        TextView title = (TextView) root.findViewById(R.id.titleView);
        RelativeLayout container = (RelativeLayout) root.findViewById(R.id.rlGameItemContainer);

        TextView infoPlayers = (TextView) root.findViewById(R.id.tvInfoPlayers);
        TextView infoTime = (TextView) root.findViewById(R.id.tvInfoTime);
        TextView infoWeight = (TextView) root.findViewById(R.id.tvInfoWeight);

        // put data
        new GameImagePopulator(image, currentGame.getId(), cache).execute();
        title.setText(currentGame.getName());
        infoPlayers.setText(currentGame.getPlayersString());
        infoTime.setText(currentGame.getPlayingTime()+" min");
        infoWeight.setText(currentGame.getWeightEnum().toString());

        // click for details
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.startGameActivity(caller, currentGame.getId());
            }
        });

        if (position == 0) { // first element special
            container.setBackgroundColor(caller.getResources().getColor(R.color.golden));
            container.setPadding(0, 15, 0, 15);
        } else {
            container.setBackgroundColor(caller.getResources().getColor(android.R.color.white));
            container.setPadding(0, 0, 0, 0);
        }
    }

    public static HashMap<Integer,BoardGame> getGamesForPlays(AdvancedList<Play> plays, BGGAndroidCache cache) {
        try {
            return cache.getBoardGames(new ArrayList<>(plays.mapDistinct(new AdvancedList.Map<Play, Integer>() {
                @Override
                public Integer map(Play item) {
                    return item.getGameID();
                }
            }))).toHashMap(new AdvancedList.Map<BoardGame, Integer>() {
                @Override
                public Integer map(BoardGame game) {
                    return game.getId();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> boolean containsAny(Set<T> set, Collection<T> toTest) {
        for(T t : toTest){
            if(set.contains(t))
                return true;
        }
        return false;
    }

    public static String removeArticles(String name) {
        String[] articles = new String[]{"A ","An ","The "};
        for(int i = 0; i < articles.length; i++){
            if(name.startsWith(articles[i])){
                return name.substring(articles[i].length(),name.length());
            }
        }
        return name;
    }

    public static double magicScore(BoardGame game, int players, boolean userRating, int time){

        double avgRating = (userRating && game.hasUserRating()) ? game.getUserRating() : game.getAvgRating();
        double playerBest = 0.0;
        double playerFrac = 0.0;

        // calculating fitness
        HashMap<String, HashMap<Experience, Integer>> suggestions = game.getSuggestedNumPlayers();
        int sum = 0;
        if(suggestions.containsKey(""+players)){
            for(String key : suggestions.keySet()){
                sum += suggestions.get(key).get(Experience.Best);
            }
            int votesForPlayer = 0;
            for(Experience e : Experience.values()){
                votesForPlayer += suggestions.get(""+players).get(e);
            }

            if(votesForPlayer != 0 && sum != 0){
                double curPlayers = suggestions.get(""+players).get(Experience.Best);
                playerBest = 10.0*(curPlayers / sum);
                playerFrac = 10.0*(curPlayers / votesForPlayer);
            }
        }

        // Calculating other parts (than avgRating, playerBest and playerFrac)
        double timePart = Math.max(0.0, 1.0 - Math.abs((double)time - game.getPlayingTime()) / time) * 10.0;

        // Adding up factors
        int numFactors = 6;
        double factorWeight = 1.0/numFactors;

        // calculating vote weight
        double voteWeight = factorWeight*(Math.min(50.0,sum)/50.0);
        double ratingWeight = 1 + factorWeight * (Math.min(50.0, game.getUsersRated()) / 50.0); // Always used as one factor, other if enough votes

        double result = 0.0;
        result += ratingWeight * avgRating;         // The average rating of game (double weight)
        result += voteWeight * playerBest;          // Fraction of Best votes (across player options)
        result += voteWeight * playerFrac;          // The fraction of Best votes (for this player count)
        if(time > 0) {                              // Being close to target playing time is better
            result += 2 * factorWeight * timePart;
        }

        System.out.printf("%s: rating=%.2f playerBest=%.2f playerFrac=%.2f time=%.2f == %.2f\n",game.getName(), avgRating, playerBest, playerFrac, timePart, result);

        return result;
    }

    public static AdvancedList<Play> getValidChallengePlays(AdvancedList<Play> plays, Integer gameId, CompanionDAO.Challenge.ChallengeStart start, Context context) {
        return plays.filter(new Utilities.IsGameFilter(gameId,start,Utilities.prefInclIncomplete(context)));
    }

    public static AdvancedList<Play> getValidAlphabetChallengePlays(AdvancedList<Play> plays, CompanionDAO.Challenge challenge, Context context) {
        return plays.filter(new Utilities.IsThisYearFilter(challenge.getStart(), Utilities.prefInclIncomplete(context)));
    }

    public static int getValidChallengePlaysCount(AdvancedList<Play> plays, Integer gameId, CompanionDAO.Challenge challenge, Context context) {
        return plays.count(new Utilities.IsGameFilter(gameId, challenge.getStart(), Utilities.prefInclIncomplete(context)));
    }

    public static GamesLoaderTask performSearchForGame(final Context context, final BGGAndroidCache externalDAO, final BoardGameAdapter.OnGameClickListener listener) {
        final GamesLoaderTask[] loaderTask = {null};

        // get search term
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Search for game");

        final EditText input = new EditText(context);
        alert.setView(input);

        alert.setPositiveButton("Search",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        final String query = input.getText().toString()
                                .trim();

                        // handle the search
                        loaderTask[0] = new GamesLoaderTask(context, true) {
                            public AdvancedList<Integer> allGames = null;

                            @Override
                            public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                                this.allGames = new AdvancedList<Integer>(cache.searchBoardGames(query));
                                return allGames.take(10);
                            }

                            @Override
                            public void handleGamesInUIThread(final AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Select Game");

                                // Have to make my own ListView
                                ListView subView = new ListView(context);
                                // ADAPTER TO LAYOUT GAMES
                                BoardGameAdapter adapter = new BoardGameAdapter(context, R.layout.board_game_list_item_simple, loadedGames, cache) {

                                    @Override
                                    public void populateView(View root, final BoardGame currentGame, int position, BGGAndroidCache cache) {
                                        ImageView image = (ImageView) root.findViewById(R.id.ivGameImage);
                                        TextView title = (TextView) root.findViewById(R.id.tvGameTitle);

                                        if (currentGame.getThumbnailUrl() != null) {
                                            new GameImagePopulator(image, currentGame.getId(), externalDAO).execute();
                                        }
                                        title.setText(currentGame.getName());
                                    }
                                };

                                subView.setAdapter(adapter);
                                builder.setView(subView);
                                final AlertDialog alertDialog = builder.create();

                                // ON SCROLL, LOAD MORE
                                subView.setOnScrollListener(new GamesScrollListener(allGames, 10));


                                // ON ITEM CLICKED
                                adapter.setOnClickListener(new BoardGameAdapter.OnGameClickListener() {
                                    @Override
                                    public void onClick(BoardGame game, int position) {
                                        // Let the caller handle this
                                        listener.onClick(game, position);

                                        alertDialog.dismiss();
                                    }
                                });

                                alertDialog.show();
                            }
                        };
                        loaderTask[0].execute();
                    }
                });

        alert.show();
        Utilities.showKeyboard(input, context);

        return loaderTask[0];
    }

    public static ImageButton makeGameIcon(Context c, boolean showPlaceholder, View.OnClickListener listener) {
        ImageButton button = new SquareImageButton(c);

        LinearLayout.LayoutParams weightParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
        button.setPadding(5, 5, 5, 5);
        button.setLayoutParams(weightParams);
        button.setScaleType(ImageView.ScaleType.FIT_CENTER);
        button.setBackgroundColor(Color.TRANSPARENT);
        if(showPlaceholder) button.setImageResource(R.drawable.unselected);
        button.setOnClickListener(listener);

        return button;
    }

    public static int clamp(int value, int min, int max) {
        return Math.max(min, Math.min(value, max));
    }

    public static <T> String[] toStrings(T[] values) {
        String[] result = new String[values.length];
        for(int i = 0; i < values.length; i++){
            result[i] = values[i].toString();
        }
        return result;
    }

    public static void startGameActivity(Context context, int id) {
        Intent i = new Intent(context, BoardGameActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("gameId", id);
        i.putExtras(bundle);
        context.startActivity(i);
    }

    public static void startGameListActivity(Context context, String title, int[] gameIds) {
        Intent i = new Intent(context, BoardGameListActivity.class);
        i.putExtra(BoardGameListActivity.BOARD_GAME_IDS_EXTRA, gameIds);
        i.putExtra("title", title);
        context.startActivity(i);
    }

    public static void startInfiniteGameListActivity(Context context, String title, int[] gameIds) {
        Intent i = new Intent(context, InfiniteBoardGameListActivity.class);
        i.putExtra(BoardGameListActivity.BOARD_GAME_IDS_EXTRA, gameIds);
        i.putExtra("title",title);
        context.startActivity(i);
    }

    public static boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void handleUserException(Context c, UserNotFoundException e) {
        Toast.makeText(c, "The BGG username '"+e.getUsername()+"' didn't exist", Toast.LENGTH_LONG).show();
        System.err.println("UserNotFoundException :(");
    }

    public static void handleIOException(Context c){
        Toast.makeText(c, "Network problems - have you tried turning it off and on again?", Toast.LENGTH_LONG).show();
        System.err.println("IOException :(");
    }

    public static String[] prefExtraUsers(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        String full = prefs.getString("pref_extrausers", "");
        if(full.length() > 0){
            String[] result = full.split(",");
            for(int i = 0; i < result.length; i++)
                result[i] = result[i].trim();
            return result;
        }
        return new String[]{};
    }

    public static String prefPlayLogger(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString("pref_playlogger", "");
    }

    public static boolean prefAlphabetExclude(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_alphabetical", true);
    }

    public static boolean prefAlphabetDigits(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_alphabetdigits", false);
    }

    // PREFERENCES

    public static int prefDefaultTool(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        String prefValue = prefs.getString("pref_deftool", "");

        String[] values = c.getResources().getStringArray(R.array.tool_titles);
        for(int i = 0; i < values.length; i++) {
            if(values[i].equals(prefValue))
                return i;
        }
        return 0;
    }

    public static boolean prefInclExpansions(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_chooseexps", false);
    }

    public static boolean prefInclLendOut(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_chooselendout", true);
    }

    public static boolean prefInclIncomplete(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_chooseincomplete", true);
    }

    public static boolean prefUserRating(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString("pref_chooserating", "1").equals("2");
    }

    public static boolean prefCollInclExpansions(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_collexps", true);
    }

    public static String getUsername(Context c){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString("pref_username", "").trim();
    }

    public static int[] getIds(AdvancedList<BoardGame> list){
        int[] result = new int[list.size()];
        for(int i = 0; i < list.size(); i++){
            result[i] = list.get(i).getId();
        }
        return result;
    }

    public static AdvancedList<Play> getPlays(boolean forceDownload, BGGAndroidCache cache, Context c) throws IOException {
        final String username = getUsername(c).toLowerCase();

        AdvancedList<Play> result;
        // and marked by another
        String logger = prefPlayLogger(c);
        if(logger != null && logger.length() > 0) {
            result = cache.getPlays(logger,forceDownload).filter(new AdvancedList.Filter<Play>() {
                @Override
                public boolean filter(Play item) {
                    return new AdvancedList<Player>(item.getPlayers()).any(new AdvancedList.Filter<Player>() {
                        @Override
                        public boolean filter(Player item) {
                            return item.getUsername().toLowerCase().equals(username);
                        }
                    });
                }
            });
        } else {
            // get own plays
            result = cache.getPlays(username,forceDownload);
        }
        return result;
    }

    public static String createTitle(int players, int maxTime,
                               Set<BoardGameWeight> weights, String bgClass, boolean isRequiredCats,
                               List<String> selectedCats, boolean isRequiredMechs, List<String> selectedMechs) {
        StringBuilder builder = new StringBuilder();
        builder.append("Games");
        int infoCount = 0;
        if(players != -1){
            builder.append(" for ").append(players).append(" players");
            infoCount++;
        }
        if(maxTime != -1){
            builder.append(" about ").append(maxTime).append(" min");
            infoCount++;
        }
        if(infoCount < 3 && weights != null && weights.size() == 1){
            builder.insert(0," ");
            builder.insert(0,weights.iterator().next().toString());
            infoCount++;
        }
        if(infoCount < 3 && bgClass != null && !bgClass.equals("ANY")){
            builder.insert(0," ");
            builder.insert(0,bgClass.replace(" Games","").replace(" Game",""));
            infoCount++;
        }
        if(infoCount < 3 && selectedCats != null && selectedCats.size() == 1){
            builder.append(isRequiredCats ? " with '" : " without '");
            builder.append(selectedCats.get(0));
            builder.append("'");
            infoCount++;
        }
        if(infoCount < 3 && selectedMechs != null && selectedMechs.size() == 1){
            if(selectedCats != null && selectedCats.size() == 1){
                if(isRequiredMechs != isRequiredCats){
                    builder.append(isRequiredMechs ? " but with '" : " and without '");
                }else{
                    builder.append(" and '");
                }
            }else{
                builder.append(isRequiredMechs ? " with '" : " without '");
            }
            builder.append(selectedMechs.get(0));
            builder.append("'");
            infoCount++;
        }
        if(infoCount > 0){
            return builder.toString();
        }else{
            return "Board Games";
        }
    }

    public static List<String> getPlaysStrings(AdvancedList<Play> plays){
        ArrayList<String> result = new ArrayList<>();
        for(Play p : plays){
            String playString = p.toString();
            for(int i = 0; i < p.getQuantity(); i++)
                result.add(playString);
        }
        return result;
    }

    public static int safeParseInt(String str){
        try{
            return Integer.parseInt(str);
        }catch(NumberFormatException e){}
        return -1;
    }

    public static void showKeyboard(final EditText editText, final Context c){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager= (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        editText.requestFocus();
    }

    public static Bitmap loadBitmap(String url) {
        if(url == null)
            return null;

        if (!url.startsWith("http:"))
            url = "http:" + url;
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream());

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            // options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
                    options);
        } catch (IOException e) {
            System.err.println("Could not load Bitmap from: " + url);
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                // System.err.println("Failed to load Bitmap from internet.");
            }
        }

        return bitmap;
    }

    public static Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public static void preloadBitmaps(final Context context, final AdvancedList<BoardGame> games){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                BGGAndroidCache cache = new BGGAndroidCache(context);
                for(BoardGame game : games){
                    cache.getBitmap(game.getId(), DBHelper.BoardGameAttribute.thumb_url);
                }
                return null;
            }
        }.execute();
    }

    // PRIVATE HELPER METHODS

    private static int prefChallengeSize(Context c){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getInt("pref_challsize",10);
    }

    private static boolean prefChallengeHardcore(Context c){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getBoolean("pref_challhardcore",false);
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
