package dk.nezbo.boardgamecompanion.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Emil on 11-03-2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 23;

    public static final String DB_NAME = "bgg";

    public static final String DB_TABLE_CHALLENGE_ROW = "challenge_row";
    public static final String DB_COL_ID = "_id";           // present in all tables
    public static final String DB_COL_ROW = "row_num";      // the index of this challenge row
    public static final String DB_COL_GAME_ID = "game_id";  // the game id for the row
    public static final String DB_COL_STARS = "stars";      // only used for manual entry
    public static final String DB_COL_CHALLENGE_ID = "challenge_id"; // reference to the challenge

    public static final String DB_TABLE_ALPHABET_ROW = "alphabet_challenge_row";
    public static final String DB_COL_LETTER = "letter";      // the letter of the row
    public static final String DB_COL_PLAYED = "man_played";  // for manual logging, if it has been played

    public static final String DB_TABLE_CHALLENGE = "challenge";
    public static final String DB_COL_NAME = "name"; // The customizable name of the challenge
    public static final String DB_COL_GAMES = "games"; // The number of games in the challenge
    public static final String DB_COL_TIMES = "times"; // The wanted number of plays for each game
    public static final String DB_COL_TYPE = "type"; // The type of challenge. 0=10x10, 1 = alphabet, 2 = unique
    public static final String DB_COL_ALLTIME = "start"; // 0 if the challenge should only be for the current year
    public static final String DB_COL_MANUAL = "manual"; // 0 if the challenge should use BGG plays as logging

    public static final String DB_TABLE_COLLECTION = "collection";
    public static final String DB_COL_USERNAME = "username";// the username of the owner
    //public static final String DB_COL_GAME_ID = "game_id";// the game id for in the collection
    public static final String DB_COL_RATING = "rating";    // the user's rating of that game
    public static final String DB_COL_TIMESTAMP = "timestamp";   // the timestamp of when fetched

    public static final String DB_TABLE_BOARDGAME = "game";
    public static final String DB_TABLE_COLLECTION_CHALLENGE_ROW = "collection_challenge_row";

    public enum BoardGameAttribute {
        id,
        name,
        thumb_url,
        image_url,
        game_type,
        year,
        playing_time,
        min_players,
        max_players,
        min_time,
        max_time,
        min_age,
        rank,
        avg_rating,
        users_rated,
        board_game_class_id,
        class_rank,
        weight,
        timestamp
    }

    /*
    public static final String DB_COL_THUMB_URL = "thumb_url";
    public static final String DB_COL_IMAGE_URL = "image_url";
    public static final String DB_COL_GAME_TYPE = "type";
    public static final String DB_COL_YEAR = "year";
    public static final String DB_COL_PLAYING_TIME = "play_time";
    public static final String DB_COL_MIN_PLAYERS = "min_players";
    public static final String DB_COL_MAX_PLAYERS = "max_players";
    public static final String DB_COL_MIN_TIME = "min_time";
    public static final String DB_COL_MAX_TIME = "max_time";
    public static final String DB_COL_MIN_AGE = "min_age";
    public static final String DB_COL_RANK = "rank";
    public static final String DB_COL_AVG_RATING = "rating";
    //public static final String DB_COL_TIMESTAMP = "date";   // the timestamp of when fetched*/

    public static final String DB_TABLE_SUGG_PLAYERS = "sugg_players";
    //public static final String DB_COL_GAME_ID = "game_id";  // the game id for the row
    public static final String DB_COL_NUM_PLAYERS = "players";
    public static final String DB_COL_BEST = "best";
    public static final String DB_COL_RECOMM = "recomm";
    public static final String DB_COL_NOT_RECOMM = "not_recomm";

    public static final String DB_TABLE_LINK = "link";
    //public static final String DB_COL_GAME_ID = "game_id";  // the game id for the row
    //public static final String DB_COL_NAME = "name";
    //public static final String DB_COL_TYPE = "type";
    public static final String DB_COL_VALUE = "value";

    public static final String DB_TABLE_PLAY = "play";
    //public static final String DB_COL_USERNAME = "username";
    //public static final String DB_COL_GAME_ID = "game_id";
    public static final String DB_COL_QUANTITY = "quantity";
    public static final String DB_COL_DURATION = "duration";
    public static final String DB_COL_LOCATION = "location";
    public static final String DB_COL_COMMENT = "comment";
    public static final String DB_COL_DATE = "date";
    //public static final String DB_COL_TIMESTAMP = "timestamp";

    public static final String DB_TABLE_PARTICIPANTS = "participants";
    public static final String DB_COL_PLAY_ID = "play_id";
    //public static final String DB_COL_NAME = "name";
    //public static final String DB_COL_USERNAME = "username";
    public static final String DB_COL_COLOR = "color";
    public static final String DB_COL_SCORE = "score";
    public static final String DB_COL_NEW_PLAYER = "new_player";
    //public static final String DB_COL_RATING = "rating";
    public static final String DB_COL_WIN = "win";

    public static final String DB_TABLE_BOARD_GAME_CLASS = "bg_class";
    //public static final String DB_COL_NAME = "name";

    public static final String DB_TABLE_LEND_OUT = "lend_out";
    //public static final String DB_COL_USERNAME = "username";
    //public static final String DB_COL_GAME_ID = "game_id";
    //public static final String DB_COL_NAME = "name";

    // PRIVATE FIELDS

    private final Context context;
    private final String db_name;

    public DBHelper(Context c, String db_name){
        super(c, DB_NAME, null, DB_VERSION);
        this.context = c;
        this.db_name = db_name;
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        executeSQLFile(db, db_name + "-source.sql");
        System.out.println("Database created.");
        this.onUpgrade(db, 0, DB_VERSION);
    }

    private void executeSQLFile(SQLiteDatabase db, String sql_filename) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.context.getAssets().open(sql_filename)));
            while (reader.ready()) {
                try {
                    String stmt = reader.readLine();
                    if(stmt != null && !stmt.startsWith("--")) {
                        System.out.println(stmt);
                        db.execSQL(stmt);
                    }
                }catch(SQLException e) {
                    if(!e.getMessage().contains("not an error")) {
                        throw e;
                    }
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            // File not found, do nothing
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // UPDATE

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion < 19) {
            // Perform hard reset, sorry
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_CHALLENGE+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_COLLECTION+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_BOARDGAME+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_SUGG_PLAYERS+";");
            db.execSQL("DROP TABLE IF EXISTS "+"category"+";");
            db.execSQL("DROP TABLE IF EXISTS "+"mechanic"+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_PLAY+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_PARTICIPANTS+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_BOARD_GAME_CLASS+";");
            db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE_LEND_OUT+";");
            this.onCreate(db);
        } else {
            // Try to perform iterative upgrades for all versions
            for(int i = oldVersion + 1; i <= newVersion; i++) {
                executeSQLFile(db, db_name + "-update" + i + ".sql");
                System.out.println("Database upgraded to version "+i);
            }
        }
    }
}
