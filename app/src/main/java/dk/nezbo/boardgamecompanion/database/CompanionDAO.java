package dk.nezbo.boardgamecompanion.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Experience;
import bggapi.model.Play;
import bggapi.model.Player;
import dk.nezbo.boardgamecompanion.database.DBHelper.BoardGameAttribute;

/**
 * Created by Emil on 11-03-2015.
 */
public class CompanionDAO {

    private final DBHelper helper;
    private SQLiteDatabase db;

    // locking
    private static final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private static final Lock r = rwl.readLock();
    private static final Lock w = rwl.writeLock();

    public CompanionDAO(Context c){
        this.helper = new DBHelper(c, "companion");
    }

    public void open(){
        try{
            w.lock();
            this.db = helper.getWritableDatabase();
        }finally {
            w.unlock();
        }

    }

    public void close(){
        try{
            w.lock();
            this.db.close();
            db = null;
        }finally {
            w.unlock();
        }
    }

    // TIMESTAMPS
/*
    try{
        w.lock();

    }finally {
        w.unlock();
    }
*/
    public Calendar getGameTimestamp(int gameId){
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT " + BoardGameAttribute.timestamp
                    + " FROM " + DBHelper.DB_TABLE_BOARDGAME
                    + " WHERE " + BoardGameAttribute.id + " = ?"
                    , new String[]{gameId + ""});
            if(cursor.moveToFirst()){
                Calendar result = longToCal(cursor.getLong(cursor.getColumnIndexOrThrow(BoardGameAttribute.timestamp.name())));
                cursor.close();
                return result;
            }
            cursor.close();
            return longToCal(0);
        }finally {
            r.unlock();
        }
    }

    public Calendar getPlayTimestamp(String username){
        try{
            r.lock();
            // we just need to check one
            Cursor cursor = db.query(DBHelper.DB_TABLE_PLAY
                    , new String[]{DBHelper.DB_COL_TIMESTAMP}
                    , DBHelper.DB_COL_USERNAME + " = ?"
                    , new String[]{username}
                    , null, null, null, "1");
            if(cursor.moveToFirst()){
                Calendar result = longToCal(cursor.getLong(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TIMESTAMP)));
                cursor.close();
                return result;
            }
            cursor.close();
            return longToCal(0);
        }finally {
            r.unlock();
        }
    }

    // GETTERS

    public Challenge getChallenge(int id) {
        Challenge result = null;
        try{
            r.lock();

            Cursor cursor = db.rawQuery("SELECT *"
                            + " FROM " + DBHelper.DB_TABLE_CHALLENGE
                            + " WHERE " + DBHelper.DB_COL_ID + " = ?",
                    new String[]{String.valueOf(id)}
            );
            if(cursor.moveToFirst()) {
                String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                int games = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAMES));
                int times = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TIMES));
                int type = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TYPE));
                boolean time = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ALLTIME)).equals("1");
                boolean manual = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_MANUAL)).equals("1");
                result = new Challenge(id, name, games, times, type, time, manual);
            }
        }finally {
            r.unlock();
        }
        return result;
    }

    public List<Challenge> getChallenges() {
        try{
            r.lock();

            Cursor cursor = db.rawQuery("SELECT *"
                    + " FROM " + DBHelper.DB_TABLE_CHALLENGE
                    + " ORDER BY " + DBHelper.DB_COL_ID + " ASC"
                    , new String[]{});
            ArrayList<Challenge> result = new ArrayList<>();
            while(cursor.moveToNext()){
                int id = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ID));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                int games = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAMES));
                int times = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TIMES));
                int type = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TYPE));
                boolean time = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ALLTIME)).equals("1");
                boolean manual = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_MANUAL)).equals("1");
                result.add(new Challenge(id, name, games, times, type, time, manual));
            }
            cursor.close();
            return result;
        }finally {
            r.unlock();
        }
    }

    public List<Integer> getChallengeGames(int challenge_id) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT "+DBHelper.DB_COL_GAME_ID
                    +" FROM "+DBHelper.DB_TABLE_CHALLENGE_ROW
                    +" WHERE "+DBHelper.DB_COL_CHALLENGE_ID+" = ?"
                    ,new String[]{String.valueOf(challenge_id)});
            LinkedList<Integer> ids = new LinkedList<>();
            while(cursor.moveToNext()){
                ids.add(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID)));
            }
            cursor.close();

            return ids;
        }finally {
            r.unlock();
        }
    }

    public List<Integer> getAlphabetGames(int challenge_id, char letter) {
        try{
            int number = Character.toUpperCase(letter);

            r.lock();
            Cursor cursor = db.rawQuery("SELECT "+DBHelper.DB_COL_GAME_ID
                    +" FROM "+DBHelper.DB_TABLE_ALPHABET_ROW
                    +" WHERE "+DBHelper.DB_COL_CHALLENGE_ID+" = ?"
                    +" AND "+DBHelper.DB_COL_LETTER+" = ?"
                    ,new String[]{String.valueOf(challenge_id), String.valueOf(number)});
            LinkedList<Integer> ids = new LinkedList<>();
            while(cursor.moveToNext()){
                ids.add(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID)));
            }
            cursor.close();

            return ids;
        }finally {
            r.unlock();
        }
    }

    public Collection<Integer> getCollectionChallengeGames(int challenge_id) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT "+DBHelper.DB_COL_GAME_ID
                    +" FROM "+DBHelper.DB_TABLE_COLLECTION_CHALLENGE_ROW
                    +" WHERE "+DBHelper.DB_COL_CHALLENGE_ID+" = ?"
                    ,new String[]{String.valueOf(challenge_id)});
            LinkedList<Integer> ids = new LinkedList<>();
            while(cursor.moveToNext()){
                ids.add(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID)));
            }
            cursor.close();

            return ids;
        }finally {
            r.unlock();
        }
    }

    public HashMap<Integer,ChallengeRow> getChallengeRows(int challenge_id){
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT * FROM " + DBHelper.DB_TABLE_CHALLENGE_ROW
                    + " WHERE " + DBHelper.DB_COL_CHALLENGE_ID + " = ?"
                    , new String[]{String.valueOf(challenge_id)});

            HashMap<Integer,ChallengeRow> rows = new HashMap<>();
            while(cursor.moveToNext()){
                int row = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ROW));
                rows.put(row, new ChallengeRow(
                        cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID)),
                        cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_STARS))));
            }
            cursor.close();
            return rows;
        }finally {
            r.unlock();
        }
    }

    public UserCollection getCollection(String username){
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT * FROM " + DBHelper.DB_TABLE_COLLECTION
                    + " WHERE " + DBHelper.DB_COL_USERNAME + " = ?", new String[]{username});

            HashMap<Integer,Double> games = new HashMap<>();
            long timestamp = -1;
            while(cursor.moveToNext()){
                int gameId = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID));
                double rating = cursor.getDouble(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_RATING));
                games.put(gameId, rating);
                if(timestamp < 0) timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TIMESTAMP));
            }
            cursor.close();
            return new UserCollection(username,games,longToCal(timestamp));
        }finally {
            r.unlock();
        }
    }

    public Set<Integer> getBoardGameIds(){
        try{
            r.lock();

            Set<Integer> ids = new HashSet<>();
            Cursor cursor = db.query(DBHelper.DB_TABLE_BOARDGAME, new String[]{BoardGameAttribute.id.name()}
                    , null, null, null, null, null
            );
            while(cursor.moveToNext()){
                ids.add(cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.id.name())));
            }
            cursor.close();

            return ids;
        }finally {
            r.unlock();
        }
    }

    public BoardGame getBoardGame(int gameId){
        try{
            r.lock();

            // get the nested stuff
            HashMap<String,HashMap<Experience,Integer>> sugg_num_players = getSuggestedNumPlayers(gameId);
            EnumMap<BoardGame.LinkTypes,Set<Pair<Integer,String>>> links = this.getLinks(gameId);

            Cursor cursor = db.rawQuery("SELECT * FROM "+DBHelper.DB_TABLE_BOARDGAME
                    +" WHERE "+ BoardGameAttribute.id+" = ?",new String[]{gameId+""});

            if(cursor.moveToFirst()){
                String bgClass = getBoardGameClass(cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.board_game_class_id.name())));
                BoardGame result = new BoardGame(gameId,
                        cursor.getString(cursor.getColumnIndexOrThrow(BoardGameAttribute.name.name())),
                        cursor.getString(cursor.getColumnIndexOrThrow(BoardGameAttribute.thumb_url.name())),
                        cursor.getString(cursor.getColumnIndexOrThrow(BoardGameAttribute.image_url.name())),
                        cursor.getString(cursor.getColumnIndexOrThrow(BoardGameAttribute.game_type.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.year.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.playing_time.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.min_players.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.max_players.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.min_time.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.max_time.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.min_age.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.rank.name())),
                        cursor.getDouble(cursor.getColumnIndexOrThrow(BoardGameAttribute.avg_rating.name())),
                        sugg_num_players,
                        links,
                        bgClass,
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.class_rank.name())),
                        cursor.getDouble(cursor.getColumnIndexOrThrow(BoardGameAttribute.weight.name())),
                        cursor.getInt(cursor.getColumnIndexOrThrow(BoardGameAttribute.users_rated.name()))
                );
                cursor.close();
                return result;
            }else{
                cursor.close();
                return null;
            }
        }finally {
            r.unlock();
        }

    }

    public AdvancedList<Pair<Integer,String>> getLinks(BoardGame.LinkTypes selectedType){
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT DISTINCT " + DBHelper.DB_COL_NAME
                    + " FROM " + DBHelper.DB_TABLE_LINK
                    + " ORDER BY " + DBHelper.DB_COL_NAME
                    + " WHERE " + DBHelper.DB_COL_TYPE + " = ? "
                    , new String[]{selectedType.name()});
            ArrayList<Pair<Integer,String>> result = new ArrayList<>();
            while(cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                int value = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_VALUE));
                result.add(new Pair<Integer,String>(value,name));
            }
            cursor.close();
            return new AdvancedList<>(result);
        }finally {
            r.unlock();
        }
    }

    public AdvancedList<Pair<Integer,String>> getLinks(int gameId, BoardGame.LinkTypes selectedType) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT DISTINCT " + DBHelper.DB_COL_NAME
                    + " FROM " + DBHelper.DB_TABLE_LINK
                    + " ORDER BY " + DBHelper.DB_COL_NAME
                    + " WHERE " + DBHelper.DB_COL_TYPE + " = ?"
                    + " AND " + DBHelper.DB_COL_GAME_ID + " = ? "
                    , new String[]{selectedType.name(), String.valueOf(gameId)});
            ArrayList<Pair<Integer,String>> result = new ArrayList<>();
            while(cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                int value = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_VALUE));
                result.add(new Pair<Integer,String>(value,name));
            }
            cursor.close();
            return new AdvancedList<>(result);
        }finally {
            r.unlock();
        }
    }

    public AdvancedList<Pair<Integer,String>> getLinks(String username, BoardGame.LinkTypes selectedType) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT DISTINCT " + DBHelper.DB_TABLE_LINK+"."+DBHelper.DB_COL_NAME+", "+DBHelper.DB_TABLE_LINK+"."+DBHelper.DB_COL_VALUE
                    + " FROM " + DBHelper.DB_TABLE_LINK + ", " + DBHelper.DB_TABLE_COLLECTION
                    + " WHERE " + DBHelper.DB_TABLE_LINK+"."+DBHelper.DB_COL_TYPE + " = ?"
                    + " AND " + DBHelper.DB_TABLE_COLLECTION+"."+DBHelper.DB_COL_USERNAME + " = ?"
                    + " AND " + DBHelper.DB_TABLE_LINK+"."+DBHelper.DB_COL_GAME_ID + " = " + DBHelper.DB_TABLE_COLLECTION+"."+DBHelper.DB_COL_GAME_ID
                    + " ORDER BY " + DBHelper.DB_TABLE_LINK+"."+DBHelper.DB_COL_NAME
                    , new String[]{selectedType.name(), username});
            ArrayList<Pair<Integer,String>> result = new ArrayList<>();
            while(cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                int value = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_VALUE));
                result.add(new Pair<Integer,String>(value,name));
            }
            cursor.close();
            return new AdvancedList<>(result);
        }finally {
            r.unlock();
        }
    }

    public AdvancedList<String> getBoardGameClasses() {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT "+DBHelper.DB_COL_NAME
                    +" FROM "+DBHelper.DB_TABLE_BOARD_GAME_CLASS+" ORDER BY "+ DBHelper.DB_COL_NAME
                    ,new String[]{});
            ArrayList<String> result = new ArrayList<>();
            while(cursor.moveToNext()){
                result.add(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME)));
            }
            cursor.close();
            return new AdvancedList<>(result);
        }finally {
            r.unlock();
        }
    }

    public String getBoardGameAttribute(int gameId, BoardGameAttribute attribute) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT "+attribute
                    +" FROM "+DBHelper.DB_TABLE_BOARDGAME
                    +" WHERE "+BoardGameAttribute.id+" = ?", new String[]{String.valueOf(gameId)});
            if(cursor.moveToFirst()){
                String result = cursor.getString(cursor.getColumnIndexOrThrow(attribute.name()));
                cursor.close();
                return result;
            }
            cursor.close();
            // no result
            return null;
        }finally {
            r.unlock();
        }
    }

    public AdvancedList<Play> getPlays(String username){
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT * FROM "+DBHelper.DB_TABLE_PLAY
                    + " WHERE "+DBHelper.DB_COL_USERNAME+" = ?",new String[]{username});
            ArrayList<Play> plays = new ArrayList<>();
            while(cursor.moveToNext()){
                Play.Builder builder = new Play.Builder();
                int playId = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ID));
                builder.setGameID(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_GAME_ID)));
                builder.setQuantity(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_QUANTITY)));
                builder.setDuration(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_DURATION)));
                builder.setLocation(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_LOCATION)));
                builder.setComment(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_COMMENT)));
                builder.setDate(longToCal(cursor.getLong(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_DATE))));
                for(Player p : getParticipants(playId))
                    builder.addPlayer(p);
                plays.add(builder.build());
            }
            cursor.close();
            return new AdvancedList<>(plays);
        }finally {
            r.unlock();
        }
    }

    public String getLendOut(String username, int gameId) {
        try{
            r.lock();
            Cursor cursor = db.rawQuery("SELECT * FROM " + DBHelper.DB_TABLE_LEND_OUT
                            + " WHERE " + DBHelper.DB_COL_USERNAME + " = ? AND " + DBHelper.DB_COL_GAME_ID + " = ?"
                    , new String[]{username, String.valueOf(gameId)}
            );
            if(cursor.moveToFirst()){
                String result = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
                cursor.close();
                return result;
            }
            cursor.close();
            return null;
        }finally {
            r.unlock();
        }
    }

    // SETTERS

    public void setLendOut(String username, int gameId, String personName) {
        try{
            w.lock();
            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_USERNAME,username);
            values.put(DBHelper.DB_COL_GAME_ID,gameId);
            values.put(DBHelper.DB_COL_NAME, personName);

            int rows = db.update(DBHelper.DB_TABLE_LEND_OUT, values, DBHelper.DB_COL_USERNAME + " = ? AND " + DBHelper.DB_COL_GAME_ID + " = ?",
                    new String[]{username, String.valueOf(gameId)});
            System.out.println(rows + " rows affected");
            if(rows < 1){
                long newId = db.insert(DBHelper.DB_TABLE_LEND_OUT, null, values);
                System.out.println("New id = "+newId);
            }
        }finally {
            w.unlock();
        }
    }

    public void removeLendOut(String username, int gameId) {
        try{
            w.lock();
            int rows = db.delete(DBHelper.DB_TABLE_LEND_OUT, DBHelper.DB_COL_USERNAME + " = ? AND " + DBHelper.DB_COL_GAME_ID + " = ?",
                    new String[]{username, String.valueOf(gameId)});
            System.out.println(rows+" rows affected");
        }finally {
            w.unlock();
        }
    }

    public void putPlays(String username, AdvancedList<Play> plays){
        try{
            w.lock();
            // delete if existing
            for(Integer playId : getPlayIds(username)){
                db.delete(DBHelper.DB_TABLE_PARTICIPANTS,
                        DBHelper.DB_COL_PLAY_ID+" = ?", new String[]{String.valueOf(playId)});
            }
            db.delete(DBHelper.DB_TABLE_PLAY,DBHelper.DB_COL_USERNAME+" = ?",new String[]{username});

            // insert plays
            for(Play p : plays){
                ContentValues values = new ContentValues();
                values.put(DBHelper.DB_COL_USERNAME,username);
                values.put(DBHelper.DB_COL_GAME_ID,p.getGameID());
                values.put(DBHelper.DB_COL_QUANTITY,p.getQuantity());
                values.put(DBHelper.DB_COL_DURATION,p.getDuration());
                values.put(DBHelper.DB_COL_LOCATION, p.getLocation());
                values.put(DBHelper.DB_COL_COMMENT,p.getComment());
                values.put(DBHelper.DB_COL_DATE,calToLong(p.getDate()));
                values.put(DBHelper.DB_COL_TIMESTAMP,calToLong(Calendar.getInstance()));
                long playId = db.insert(DBHelper.DB_TABLE_PLAY,null,values);

                // insert participants
                for(Player participant : p.getPlayers()){
                    putParticipant(playId,participant);
                }
            }
        }finally {
            w.unlock();
        }
    }

    public void putBoardGame(BoardGame game){
        try{
            w.lock();
            // delete if existing
            db.delete(DBHelper.DB_TABLE_LINK,
                    DBHelper.DB_COL_GAME_ID+" = ?",new String[]{String.valueOf(game.getId())});
            db.delete(DBHelper.DB_TABLE_SUGG_PLAYERS,
                    DBHelper.DB_COL_GAME_ID+" = ?",new String[]{String.valueOf(game.getId())});
            db.delete(DBHelper.DB_TABLE_BOARDGAME,
                    BoardGameAttribute.id+" = ?",new String[]{String.valueOf(game.getId())});

            int boardGameClass = this.putBoardGameClass(game.getBoardGameClass());
            // put game in
            ContentValues values = new ContentValues();
            values.put(BoardGameAttribute.id.name(),game.getId());
            values.put(BoardGameAttribute.name.name(),game.getName());
            values.put(BoardGameAttribute.thumb_url.name(),game.getThumbnailUrl());
            values.put(BoardGameAttribute.image_url.name(),game.getImageUrl());
            values.put(BoardGameAttribute.game_type.name(),game.getType());
            values.put(BoardGameAttribute.year.name(),game.getYear());
            values.put(BoardGameAttribute.playing_time.name(),game.getPlayingTime());
            values.put(BoardGameAttribute.min_players.name(),game.getMinPlayers());
            values.put(BoardGameAttribute.max_players.name(),game.getMaxPlayers());
            values.put(BoardGameAttribute.min_time.name(),game.getMinPlayingTime());
            values.put(BoardGameAttribute.max_time.name(),game.getMaxPlayingTime());
            values.put(BoardGameAttribute.min_age.name(),game.getMinAge());
            values.put(BoardGameAttribute.rank.name(),game.getRank());
            values.put(BoardGameAttribute.avg_rating.name(),game.getAvgRating());
            values.put(BoardGameAttribute.board_game_class_id.name(),boardGameClass);
            values.put(BoardGameAttribute.class_rank.name(), game.getClassRank());
            values.put(BoardGameAttribute.weight.name(), game.getWeight());
            values.put(BoardGameAttribute.timestamp.name(), calToLong(Calendar.getInstance()));
            values.put(BoardGameAttribute.users_rated.name(), game.getUsersRated());

            db.insert(DBHelper.DB_TABLE_BOARDGAME,null,values);

            // put game references in
            for(Map.Entry<String,HashMap<Experience,Integer>> entry : game.getSuggestedNumPlayers().entrySet()){
                values = new ContentValues();
                values.put(DBHelper.DB_COL_GAME_ID,game.getId());
                values.put(DBHelper.DB_COL_NUM_PLAYERS,entry.getKey());
                values.put(DBHelper.DB_COL_BEST,entry.getValue().get(Experience.Best));
                values.put(DBHelper.DB_COL_RECOMM,entry.getValue().get(Experience.Recommended));
                values.put(DBHelper.DB_COL_NOT_RECOMM,entry.getValue().get(Experience.NotRecommended));

                db.insert(DBHelper.DB_TABLE_SUGG_PLAYERS,null,values);
            }

            // links
            for(BoardGame.LinkTypes type : BoardGame.LinkTypes.values()) {
                Set<Pair<Integer,String>> matches = game.getLinks(type);
                if(matches == null)
                    continue;

                for(Pair<Integer,String> row : matches) {
                    values = new ContentValues();
                    values.put(DBHelper.DB_COL_TYPE, type.name());
                    values.put(DBHelper.DB_COL_GAME_ID,game.getId());
                    values.put(DBHelper.DB_COL_NAME, row.second);
                    values.put(DBHelper.DB_COL_VALUE, row.first);

                    db.insert(DBHelper.DB_TABLE_LINK,null,values);
                }
            }
        }finally {
            w.unlock();
        }
    }

    public void setCollection(UserCollection collection){
        try{
            w.lock();
            // delete all existing
            db.delete(DBHelper.DB_TABLE_COLLECTION, DBHelper.DB_COL_USERNAME + " = ?",
                    new String[]{collection.getUsername()});

            // insert
            long timestamp = calToLong(Calendar.getInstance());
            for(HashMap.Entry<Integer,Double> game : collection.getGames().entrySet()){
                ContentValues values = new ContentValues();
                values.put(DBHelper.DB_COL_USERNAME,collection.getUsername());
                values.put(DBHelper.DB_COL_GAME_ID,game.getKey());
                values.put(DBHelper.DB_COL_RATING,game.getValue());
                values.put(DBHelper.DB_COL_TIMESTAMP,timestamp);

                db.insert(DBHelper.DB_TABLE_COLLECTION,null,values);
            }
        }finally {
            w.unlock();
        }
    }

    public int putChallenge(String name, int num_games, int num_times, int type) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_GAMES, num_games);
            values.put(DBHelper.DB_COL_TIMES, num_times);
            values.put(DBHelper.DB_COL_NAME, name);
            values.put(DBHelper.DB_COL_TYPE, type);
            values.put(DBHelper.DB_COL_ALLTIME, 0);
            values.put(DBHelper.DB_COL_MANUAL, 0);

            int assigned_id = (int) db.insert(DBHelper.DB_TABLE_CHALLENGE, null, values);
            return assigned_id;
        }finally {
            w.unlock();
        }
    }

    public boolean setChallengeTime(int challenge_id, boolean allTime) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_ALLTIME, allTime);

            int rows = db.update(DBHelper.DB_TABLE_CHALLENGE, values, DBHelper.DB_COL_ID + " = ?", new String[]{String.valueOf(challenge_id)});
            return rows == 1;
        }finally {
            w.unlock();
        }
    }

    public boolean setChallengeManual(int challenge_id, boolean manual) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_MANUAL, manual);

            int rows = db.update(DBHelper.DB_TABLE_CHALLENGE, values, DBHelper.DB_COL_ID + " = ?", new String[]{String.valueOf(challenge_id)});
            return rows == 1;
        }finally {
            w.unlock();
        }
    }

    public int putAlphabetChallenge(String name) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_NAME, name);
            values.put(DBHelper.DB_COL_TYPE, 1);
            values.put(DBHelper.DB_COL_ALLTIME, 0);
            values.put(DBHelper.DB_COL_MANUAL, 0);

            int assigned_id = (int) db.insert(DBHelper.DB_TABLE_CHALLENGE, null, values);
            return assigned_id;
        }finally {
            w.unlock();
        }
    }

    public void putCollectionChallengeGame(int challenge_id, int gameId) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_CHALLENGE_ID, challenge_id);
            values.put(DBHelper.DB_COL_GAME_ID, gameId);

            int assigned_id = (int) db.insert(DBHelper.DB_TABLE_COLLECTION_CHALLENGE_ROW, null, values);
        }finally {
            w.unlock();
        }
    }

    public void removeCollectionChallengeGame(int challenge_id, int gameId) {
        try{
            w.lock();

            db.delete(DBHelper.DB_TABLE_COLLECTION_CHALLENGE_ROW,
                    DBHelper.DB_COL_CHALLENGE_ID + " = ? AND " + DBHelper.DB_COL_GAME_ID + " = ?",
                    new String[]{String.valueOf(challenge_id), String.valueOf(gameId)});
        }finally {
            w.unlock();
        }
    }

    public boolean setChallenge(int id, String name, int num_games, int num_times) {
        try{
            w.lock();

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_GAMES, num_games);
            values.put(DBHelper.DB_COL_TIMES, num_times);
            values.put(DBHelper.DB_COL_NAME, name);

            int rows = db.update(DBHelper.DB_TABLE_CHALLENGE, values, DBHelper.DB_COL_ID + " = ?", new String[]{String.valueOf(id)});
            return rows == 1;
        }finally {
            w.unlock();
        }
    }

    public boolean removeChallenge(int challenge_id) {
        try{
            w.lock();

            db.delete(DBHelper.DB_TABLE_CHALLENGE_ROW, DBHelper.DB_COL_CHALLENGE_ID + " = ?", new String[]{String.valueOf(challenge_id)});
            long affected = db.delete(DBHelper.DB_TABLE_CHALLENGE, DBHelper.DB_COL_ID + " = ?", new String[]{String.valueOf(challenge_id)});

            return affected > 0;
        }finally {
            w.unlock();
        }
    }

    public void setChallengeRow(int challenge_id, int row, int gameId, int stars){
        try{
            w.lock();
            // check if already exists (overwrite if so)
            Cursor cursor = db.rawQuery("SELECT *"
                    + " FROM " + DBHelper.DB_TABLE_CHALLENGE_ROW
                    + " WHERE " + DBHelper.DB_COL_ROW + " = ?"
                    + " AND " + DBHelper.DB_COL_CHALLENGE_ID + " = ?;"
                    , new String[]{String.valueOf(row), String.valueOf(challenge_id)});

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_GAME_ID,gameId);
            values.put(DBHelper.DB_COL_STARS,stars);
            values.put(DBHelper.DB_COL_ROW,row);
            values.put(DBHelper.DB_COL_CHALLENGE_ID, challenge_id);

            if(cursor.getCount() > 0){ // exists
                db.update(DBHelper.DB_TABLE_CHALLENGE_ROW
                        ,values
                        ,DBHelper.DB_COL_ROW+" = ?" +
                                " AND " + DBHelper.DB_COL_CHALLENGE_ID + " = ?"
                        ,new String[]{row+"", challenge_id+""});
            }else{ // insert from new
                db.insert(DBHelper.DB_TABLE_CHALLENGE_ROW,null,values);
            }
            cursor.close();
        }finally {
            w.unlock();
        }
    }

    public void removeChallengeRow(int challenge_id, int row){
        try{
            w.lock();

            db.delete(DBHelper.DB_TABLE_CHALLENGE_ROW, DBHelper.DB_COL_CHALLENGE_ID + " = ? AND " + DBHelper.DB_COL_ROW + " = ?"
                    , new String[]{String.valueOf(challenge_id), String.valueOf(row)});
        }finally {
            w.unlock();
        }
    }

    public void setAlphabetGame(int challenge_id, char letter, int gameId) {
        try{
            w.lock();

            int number = Character.toUpperCase(letter);

            ContentValues values = new ContentValues();
            values.put(DBHelper.DB_COL_GAME_ID,gameId);
            values.put(DBHelper.DB_COL_LETTER, String.valueOf(number));
            values.put(DBHelper.DB_COL_CHALLENGE_ID, challenge_id);

            db.insert(DBHelper.DB_TABLE_ALPHABET_ROW, null, values);
        }finally {
            w.unlock();
        }
    }

    public void removeAlphabetGame(int challenge_id, char letter, int gameid) {
        try{
            w.lock();

            int number = Character.toUpperCase(letter);

            db.delete(DBHelper.DB_TABLE_ALPHABET_ROW,
                            DBHelper.DB_COL_CHALLENGE_ID+" = ?"+
                            " AND "+DBHelper.DB_COL_LETTER+" = ?"+
                            " AND "+DBHelper.DB_COL_GAME_ID+" = ?",
                    new String[]{String.valueOf(challenge_id), String.valueOf(number), String.valueOf(gameid)});
        }finally {
            w.unlock();
        }
    }

    // PRIVATE HELPER METHODS

    private String getBoardGameClass(int classId) {
        if(classId < 0)
            return "";

        Cursor cursor = db.query(DBHelper.DB_TABLE_BOARD_GAME_CLASS
                ,new String[]{DBHelper.DB_COL_NAME}
                ,DBHelper.DB_COL_ID+" = ?"
                ,new String[]{String.valueOf(classId)}
                ,null,null,null);
        if(cursor.moveToFirst()){
            String result = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
            cursor.close();
            return result;
        }
        cursor.close();
        return null;
    }

    private int putBoardGameClass(String name){
        if(name == null || name.isEmpty())
            return -1;

        // check if exists (get id)
        Cursor cursor = db.query(DBHelper.DB_TABLE_BOARD_GAME_CLASS,
                new String[]{DBHelper.DB_COL_ID},
                DBHelper.DB_COL_NAME+" = ?",
                new String[]{name},null,null,null);
        if(cursor.moveToFirst()){
            int result = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ID));
            cursor.close();
            return result;
        }
        cursor.close();

        // add and get id
        ContentValues values = new ContentValues();
        values.put(DBHelper.DB_COL_NAME, name);
        return (int)db.insert(DBHelper.DB_TABLE_BOARD_GAME_CLASS, null, values); // i hope this cast doesn't kill things
    }

    private ArrayList<Player> getParticipants(int playId) {
        Cursor cursor = db.rawQuery("SELECT * FROM "+ DBHelper.DB_TABLE_PARTICIPANTS
                + " WHERE "+DBHelper.DB_COL_PLAY_ID+" = ?"
                ,new String[]{String.valueOf(playId)});
        ArrayList<Player> result = new ArrayList<>();
        while(cursor.moveToNext()){
            Player.Builder builder = new Player.Builder();
            builder.setUsername(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_USERNAME)));
            builder.setName(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME)));
            builder.setColor(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_COLOR)));
            builder.setScore(cursor.getDouble(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_SCORE)));
            builder.setNewPlayer(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NEW_PLAYER)).equals("1"));
            builder.setRating(cursor.getDouble(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_RATING)));
            builder.setWin(cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_WIN)).equals("1"));
            result.add(builder.build());
        }
        cursor.close();
        return result;
    }

    private void putParticipant(long playId, Player participant) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.DB_COL_PLAY_ID,playId);
        values.put(DBHelper.DB_COL_NAME,participant.getName());
        values.put(DBHelper.DB_COL_USERNAME,participant.getUsername());
        values.put(DBHelper.DB_COL_COLOR,participant.getColor());
        values.put(DBHelper.DB_COL_SCORE,participant.getScore());
        values.put(DBHelper.DB_COL_NEW_PLAYER,participant.isNewPlayer());
        values.put(DBHelper.DB_COL_RATING,participant.getRating());
        values.put(DBHelper.DB_COL_WIN,participant.isWin());

        db.insert(DBHelper.DB_TABLE_PARTICIPANTS, null, values);
    }

    private ArrayList<Integer> getPlayIds(String username) {
        Cursor cursor = db.query(DBHelper.DB_TABLE_PLAY
                , new String[]{DBHelper.DB_COL_ID}
                , DBHelper.DB_COL_USERNAME + " = ?"
                , new String[]{username}
                , null, null, null);
        ArrayList<Integer> result = new ArrayList<>();
        while(cursor.moveToNext()){
            result.add(cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_ID)));
        }
        cursor.close();
        return result;
    }

    private EnumMap<BoardGame.LinkTypes,Set<Pair<Integer,String>>> getLinks(int gameId) {
        Cursor cursor = db.rawQuery("SELECT * "
                + " FROM " + DBHelper.DB_TABLE_LINK
                + " WHERE " + DBHelper.DB_COL_GAME_ID + " = ?"
                , new String[]{String.valueOf(gameId)});
        EnumMap<BoardGame.LinkTypes,Set<Pair<Integer,String>>> result =
                new EnumMap<BoardGame.LinkTypes,Set<Pair<Integer,String>>>(BoardGame.LinkTypes.class);

        while(cursor.moveToNext()){
            String type = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_TYPE));
            BoardGame.LinkTypes enumType = BoardGame.LinkTypes.valueOf(type);
            if(!result.containsKey(enumType)) {
                result.put(enumType, new HashSet<Pair<Integer,String>>());
            }

            String name = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NAME));
            int value = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_VALUE));

            result.get(enumType).add(new Pair<Integer, String>(value, name));
        }
        cursor.close();
        return result;
    }

    private long calToLong(Calendar time) {
        return time.getTimeInMillis();
    }

    private Calendar longToCal(long time){
        Calendar result = Calendar.getInstance();
        result.setTimeInMillis(time);
        return result;
    }

    private HashMap<String,HashMap<Experience,Integer>> getSuggestedNumPlayers(int gameId) {
        Cursor cursor = db.rawQuery("SELECT * FROM "+DBHelper.DB_TABLE_SUGG_PLAYERS
                +" WHERE "+DBHelper.DB_COL_GAME_ID+" = ?",new String[]{String.valueOf(gameId)});

        HashMap<String,HashMap<Experience,Integer>> result = new HashMap<>();
        while(cursor.moveToNext()){
            String numPlayers = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NUM_PLAYERS));
            HashMap<Experience,Integer> subHash = new HashMap<>();

            subHash.put(Experience.Best,cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_BEST)));
            subHash.put(Experience.Recommended,cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_RECOMM)));
            subHash.put(Experience.NotRecommended,cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.DB_COL_NOT_RECOMM)));

            result.put(numPlayers,subHash);
        }
        cursor.close();
        return result;
    }

    // HELPER CLASSES

    public static class UserCollection{

        private final String username;
        private final HashMap<Integer,Double> games;
        private final Calendar timestamp;

        public UserCollection(String username, HashMap<Integer, Double> games, Calendar timestamp) {
            this.username = username;
            this.games = games;
            this.timestamp = timestamp;
        }

        public String getUsername() {
            return username;
        }

        public HashMap<Integer, Double> getGames() {
            return games;
        }

        public Calendar getTimestamp() {
            return timestamp;
        }
    }

    public static class ChallengeRow{

        private final int gameId;
        private final int stars;

        protected ChallengeRow(int gameId, int stars){

            this.gameId = gameId;
            this.stars = stars;
        }

        public int getGameId() {
            return gameId;
        }

        public int getStars() {
            return stars;
        }

        @Override
        public String toString() {
            return "ChallengeRow{" +
                    ", gameId=" + gameId +
                    ", stars=" + stars +
                    '}';
        }
    }

    public static class Challenge {

        public static enum ChallengeStart { All_Time, This_Year }

        private int id;
        private String name;
        private int num_games;
        private int num_times;
        private final int type;
        private boolean allTime, manual;

        public Challenge(int id, String name, int num_games, int num_times, int type, boolean allTime, boolean manual) {
            this.id = id;
            this.name = name;
            this.num_games = num_games;
            this.num_times = num_times;
            this.type = type;
            this.allTime = allTime;
            this.manual = manual;
        }

        public int getID() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getGamesCount() {
            return num_games;
        }

        public int getTimes() {
            return num_times;
        }

        public int getType() {
            return type;
        }

        public ChallengeStart getStart() { return allTime ? ChallengeStart.All_Time : ChallengeStart.This_Year; }

        public boolean isManual() { return manual; }

        @Override
        public int hashCode() {
            return id;
        }
    }
}
