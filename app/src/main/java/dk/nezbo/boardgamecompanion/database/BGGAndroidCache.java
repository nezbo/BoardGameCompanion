package dk.nezbo.boardgamecompanion.database;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.api.BoardGameGeek;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import bggapi.model.UserNotFoundException;
import dk.nezbo.boardgamecompanion.utilities.StopWatch;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * Created by Emil on 12-03-2015.
 *
 * This data abstraction handles two caches and retrieves data
 * from BGG when necessary. The priority goes as follows:
 * 1. Check if the wanted data is already instantiated as objects in the 'live cache'
 * 2. Check of the wanted data is stored in the database and has not expired (put in live cache)
 * 3. Download the wanted data from the BGG REST API (and cache it)
 *
 * For ease of use the app specific data is also accessed from this point.
 */
public class BGGAndroidCache {

    private static final int EXP_MIN_BOARDGAME = 60*24*30;
    private static final int EXP_MIN_PLAYS = 60;
    private static final int EXP_MIN_COLLECTION = 60*24;

    private final Context c;
    private final CompanionDAO dao;

    // LIVE CACHE
    public static final HashMap<Integer,BoardGame> liveBoardGameCache = new HashMap<>();
    public static final HashMap<String,AdvancedList<Play>> livePlayCache = new HashMap<>();
    public static final HashMap<String,SoftReference<Bitmap>> liveBitmapCache = new HashMap<>();

    /**
     * The only constructor for data access. Automatically opens a connection to the database.
     * @param c The context of the Android component requesting access. Is required for
     *          the database.
     */
    public BGGAndroidCache(Context c){
        this.c = c;
        this.dao = new CompanionDAO(c);
        dao.open();
    }

    /**
     * It is important to close the database (through this method),
     * when the object is not needed anymore.
     */
    public void close(){
        dao.close();
    }

    // Search

    /**
     * Searches the BGG for Games with the given query.
     * @param query The query to search for. Does not have to be sanitized in any way.
     * @return A list of integers which are the ids of games matching the query.
     * These can in turn be resolved to actual board games using @see dk.nezbo.boardgamecompanion.BGGAndroidCache#getBoardGames(List<Integer> ids)
     * .
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public List<Integer> searchBoardGames(String query) throws IOException{
        List<Integer> result = BoardGameGeek.searchBoardGameIds(query);
        return result;
    }

    // Collections

    /**
     * Gets the board game Collection of a specific BGG user.
     * @param userName The BGG username to fetch games for.
     * @return A map of the board game ids and the user's rating of each.
     * @throws UserNotFoundException The given username does not match a BGG user.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public HashMap<Integer,Double> getCollection(String userName) throws UserNotFoundException,IOException {
        return getCollection(userName, false);
    }

    /**
     * Gets the board game Collection of a specific BGG user.
     * @param userName The BGG username to fetch games for.
     * @param forceDownload True if the cache should force an update of the data, else false.
     * @return A map of the board game ids and the user's rating of each.
     * @throws UserNotFoundException The given username does not match a BGG user.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public HashMap<Integer,Double> getCollection(String userName, boolean forceDownload) throws UserNotFoundException,IOException {
        // 1) check cache
        if(!forceDownload){

            CompanionDAO.UserCollection dbCollection = dao.getCollection(userName);                                  // 3) Use if can't download
            if(dbCollection.getGames().size() > 0 && !hasExpired(dbCollection.getTimestamp(), EXP_MIN_COLLECTION) || !Utilities.isOnline(c)){
                System.out.println("Getting Collection from Cache");
                return dbCollection.getGames();
            }
        }

        // 2) get from bgg
        HashMap<Integer,Double> collection = BoardGameGeek.getCollection(userName);

        System.out.println("Getting Collection from BGG");

        // cache it
        dao.setCollection(new CompanionDAO.UserCollection(userName, collection, Calendar.getInstance()));

        // hand it over
        return collection;
    }

    // BoardGames

    /**
     * Gets a number of board games from their ids and allows for pairing with a user's rating of them.
     * @param order The ids and order for the board games.
     * @param collection The Collection of a BGG user whose ratings will be put in the result.
     * @param forceDownload True if the cache should force an update of the data, else false.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(List<Integer> order, HashMap<Integer, Double> collection, boolean forceDownload) throws IOException {
        System.out.println("Fetching "+collection.size());

        HashMap<Integer,BoardGame> hashResults = new HashMap<>();
        List<Integer> missing = new ArrayList<>();
        if(forceDownload){
            missing = order;
        }else{
            // check cache
            for(Integer gameId : order){
                BoardGame cached = null;
                // first the live cache
                if(liveBoardGameCache.containsKey(gameId)){
                    // live cache doesn't expire
                    //System.out.println("LIVE CACHED!");
                    cached = liveBoardGameCache.get(gameId);
                }else{
                    if(hasExpired(dao.getGameTimestamp(gameId),EXP_MIN_BOARDGAME) && Utilities.isOnline(c)){
                        missing.add(gameId);
                    }else{
                        cached = dao.getBoardGame(gameId);
                    }
                }

                // use the cached
                if(cached != null){
                    cached.setUserRating(collection.get(gameId));
                    liveBoardGameCache.put(gameId,cached);
                    hashResults.put(gameId, cached);
                }
            }
        }

        System.out.println((collection.size()-missing.size())+" were in cache");
        StopWatch.tick("Checked Cache");

        boolean missingGames = false;

        if(missing.size() > 0) {
            if(Utilities.isOnline(c)){
                // get from bgg

                System.out.println("Downloading remaining "+missing.size());
                Collection<BoardGame> fresh = BoardGameGeek.getBoardGames(missing);
                StopWatch.tick("Raw Downloaded");
                for(BoardGame game : fresh){
                    game.setUserRating(collection.get(game.getId()));

                    // cache it
                    liveBoardGameCache.put(game.getId(),game);
                    dao.putBoardGame(game);
                    hashResults.put(game.getId(),game);
                    //StopWatch.tick("Cached game");
                }
            }else{
                // take old stuff from database
                System.out.println("No Internet - Using any expired cache");
                for(Integer id : missing){
                    BoardGame rotten = dao.getBoardGame(id);
                    if(rotten != null){
                        hashResults.put(id,rotten);
                    }else{
                        System.err.println("Unable to fetch game (id="+id+")");
                        missingGames = true;
                    }
                }
            }
        }

        System.out.println("Done fetching games");
        //System.out.println(liveBoardGameCache.size()+" in live cache");

        StopWatch.tick("Done Fetching");

        // conserve order
        ArrayList<BoardGame> result = new ArrayList<>();
        for(Integer id : order){
            result.add(hashResults.get(id));
        }

        StopWatch.tick("Ordered");

        // print if game(s) not fetched
        if(missingGames)
            Utilities.handleIOException(c);
            //Toast.makeText(c,"Unable to fetch some games, check internet connection",Toast.LENGTH_LONG).show();

        // hand it over
        return new AdvancedList<BoardGame>(result);
    }

    /**
     * Gets a number of board games from their ids.
     * @param ids The ids and order for the board games.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(Integer... ids) throws IOException{
        return getBoardGames(false, ids);
    }

    /**
     * Gets a number of board games from their ids.
     * @param forceDownload True if the cache should force an update of the data, else false.
     * @param ids The ids and order for the board games.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(boolean forceDownload, Integer... ids) throws IOException{
        StopWatch.start();
        HashMap<Integer, Double> collection = dao.getCollection(Utilities.getUsername(c)).getGames();
        StopWatch.tick("Collection");
        HashMap<Integer, Double> query = new HashMap<>();
        for(Integer id : ids){
            if(collection.containsKey(id)){
                query.put(id,collection.get(id));
            }else{
                query.put(id,0.0);
            }
        }
        StopWatch.tick("User Ratings");
        return this.getBoardGames(Arrays.asList(ids), query, forceDownload);
    }

    /**
     * Gets a number of board games from their ids.
     * @param ids The ids and order for the board games.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(List<Integer> ids) throws IOException {
        return getBoardGames(ids, false);
    }

    /**
     * Gets a number of board games from their ids.
     * @param ids The ids and order for the board games.
     * @param forceDownload True if the cache should force an update of the data, else false.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(List<Integer> ids, boolean forceDownload) throws IOException {
        return getBoardGames(forceDownload, ids.toArray(new Integer[ids.size()]));
    }

    /**
     * Gets a number of board games from their ids.
     * @param ids The ids and order for the board games.
     * @return A list of BoardGames for the given ids in the same order as they were initially given.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<BoardGame> getBoardGames(int[] ids) throws IOException {
        ArrayList<Integer> list = new ArrayList<>(ids.length);
        for(int i = 0; i < ids.length; i++) {
            list.add(ids[i]);
        }
        return getBoardGames(list);
    }

    /**
     * Gets only the value of a specific attribute for the given board game.
     * @param gameId The id of the board game.
     * @param attribute The attribute in the database entry to fetch.
     * @return The String representation of the requested attribute.
     */
    public String getBoardGameAttribute(int gameId, DBHelper.BoardGameAttribute attribute){
        if(hasExpired(dao.getGameTimestamp(gameId),EXP_MIN_BOARDGAME)){
            // we have to download
            dao.getBoardGame(gameId);
        }
        return dao.getBoardGameAttribute(gameId, attribute);
    }

    /**
     * Gets the ids of all board games currently in the database in no specific order.
     * @return
     */
    public Set<Integer> getBoardGameIds(){
        return dao.getBoardGameIds();
    }

    // Plays

    /**
     * Gets all logged plays for a specific BGG user.
     * @param username The BGG username to fetch plays for.
     * @return A list of all Plays for the given user.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<Play> getPlays(String username) throws IOException{
        return getPlays(username, false);
    }

    /**
     * Gets all logged plays for a specific BGG user.
     * @param username The BGG username to fetch plays for.
     * @param forceDownload True if the cache should force an update of the data, else false.
     * @return A list of all Plays for the given user.
     * @throws IOException An error occurred when trying to fetch the result online.
     */
    public AdvancedList<Play> getPlays(String username, boolean forceDownload) throws IOException{
        // 1) check cache
        if(!forceDownload){
            if(livePlayCache.containsKey(username)){
                // take from live cache
                System.out.println("Getting plays from LIVE cache");
                return livePlayCache.get(username);                             // 3) take anything from cache anyway
            }else if(!hasExpired(dao.getPlayTimestamp(username),EXP_MIN_PLAYS) || !Utilities.isOnline(c)){
                // just take from cache
                System.out.println("Getting plays from cache");
                AdvancedList<Play> cached = dao.getPlays(username);
                livePlayCache.put(username,cached);
                return cached;
            }
        }

        // 2) get from bgg
        System.out.println("Getting plays from BGG");
        AdvancedList<Play> newPlays = BoardGameGeek.getPlays(username);

        // cache it
        livePlayCache.put(username, newPlays);
        dao.putPlays(username, newPlays);

        // hand it over
        return newPlays;
    }

    // BGG Related

    /**
     * Gets all 'links' currently stored in the database of a specific type
     * (e.g. Category, Mechanic, Designer etc.)
     * @param type The link type to load.
     * @return A list of pairs with the BGG id and String representation of each link.
     */
    public AdvancedList<Pair<Integer,String>> getBoardGameLinks(BoardGame.LinkTypes type){
        return dao.getLinks(type);
    }

    /**
     * Gets all 'links' relevant for a given BGG user, by them being associated with
     * a board game in that user's Collection.
     * @param username The BGG username to fetch links for.
     * @param type The link type to load.
     * @return A list of pairs with the BGG id and String representation of each link.
     */
    public AdvancedList<Pair<Integer,String>> getBoardGameLinks(String username, BoardGame.LinkTypes type) {
        return dao.getLinks(username, type);
    }

    /**
     * Gets all 'links' relevant for a given BGG user, by them being associated with
     * a board game in that user's Collection. BUT only their String representation.
     * @param username The BGG username to fetch links for.
     * @param type The link type to load.
     * @return A list of String representation of each link.
     */
    public AdvancedList<String> getBoardGameLinkNames(String username, BoardGame.LinkTypes type) {
        ArrayList<String> result = new ArrayList<>();
        for(Pair<Integer,String> match : getBoardGameLinks(username, type)) {
            result.add(match.second);
        }
        return new AdvancedList<>(result);
    }

    /**
     * Gets all stored board game classes (e.g. Abstract Game, Family Game, Thematic Game etc.)
     * @return The name of all stored board game classes.
     */
    public AdvancedList<String> getBoardGameClasses() {
        return dao.getBoardGameClasses();
    }

    // Images

    /**
     * Gets a Bitmap of an attribute of a specific game id.
     * @param gameId The game id to get the image for.
     * @param attr The attribute, must be either "thumb_url" or "image_url".
     * @return An instantiated Bitmap of the wanted image.
     */
    public Bitmap getBitmap(int gameId, DBHelper.BoardGameAttribute attr){
        if(gameId < 1)
            return null;

        String filename = gameId+"-"+attr.name()+".jpg";

        // load from cache?
        if(liveBitmapCache.containsKey(filename) && liveBitmapCache.get(filename).get() != null){
            return liveBitmapCache.get(filename).get();
        }else if(fileExists(filename)){
            Bitmap cached = loadImage(filename);
            liveBitmapCache.put(filename,new SoftReference<Bitmap>(cached));
            return cached;
        }
        // download image
        Bitmap image = Utilities.loadBitmap(this.getBoardGameAttribute(gameId,attr));
        saveImage(image,filename);
        liveBitmapCache.put(filename,new SoftReference<Bitmap>(image));
        return image;
    }

    // Non-BGG stuff (From the Database)

    /**
     * Loads a specific Challenge from the Database.
     * @param id The id of the Challenge to load.
     * @return An instance of the wanted Challenge, or null if it does not exist.
     */
    public CompanionDAO.Challenge getChallenge(int id) { return dao.getChallenge(id); }

    /**
     * Loads all Challenges regardless of type with their basic information.
     * @return A list of instances of all stored challenges.
     */
    public List<CompanionDAO.Challenge> getChallenges() { return dao.getChallenges(); }

    // Challenge General

    /**
     * Deletes a Challenge from the database.
     * @param challenge_id The id of the Challenge to delete.
     * @return True if the Challenge was successfully deleted, else false.
     */
    public boolean removeChallenge(int challenge_id) { return dao.removeChallenge(challenge_id); }

    /**
     * Each Challenge contains a time attribute of if it should count
     * from just the current year, or all time. This method sets that attribute.
     * @param challenge_id The id of the Challenge to change.
     * @param allTime True if the given Challenge should be set to last from all time, else false.
     */
    public void setChallengeTime(int challenge_id, boolean allTime) { dao.setChallengeTime(challenge_id, allTime); }

    /**
     * Sets the attribute on a Challenge if it should be logged manually or based
     * on plays logged on BGG.
     * @param challenge_id The id of the Challenge to change.
     * @param manual True if the given Challenge should be logged manually, else false.
     */
    public void setChallengeManual(int challenge_id, boolean manual) { dao.setChallengeManual(challenge_id, manual); }

    // 10x10 Challenge

    /**
     * Creates a new NxN Challenge in the database.
     * @param name The name of the new Challenge.
     * @param games The number of different games in the Challenge.
     * @param times The number of times each game should be played.
     * @return The assigned id of the newly created challenge, or -1 if an error occurred.
     */
    public int putNxNChallenge(String name, int games, int times) { return dao.putChallenge(name, games, times, 0); }

    /**
     * Edits the values of an existing NxN Challenge in the database.
     * @param id The id of the existing Challenge.
     * @param name The new name of the Challenge.
     * @param games The new number of games in the Challenge.
     * @param times The new number of times each game in the Challenge should be played.
     * @return True if the Challenge was updated successfully, else false.
     */
    public boolean setNxNChallenge(int id, String name, int games, int times) { return dao.setChallenge(id, name, games, times); }

    /**
     * Gets the instantiated version of all games stored for the given Challenge.
     * @param id The id of the Challenge to get games for.
     * @return An advanced list (for convenience) with the games in the Challenge.
     * @throws IOException An error occurred when trying to fetch the games online.
     */
    public AdvancedList<BoardGame> getChallengeGames(int id) throws IOException {
        List<Integer> ids = dao.getChallengeGames(id);
        return getBoardGames(ids);
    }

    /**
     * Gets the rows of the given NxN Challenge with its associated game and stars.
     * @param id The id of the Challenge.
     * @return A map of the row numbers and their ChallengeRows.
     */
    public HashMap<Integer,CompanionDAO.ChallengeRow> getChallengeRows(int id) {
        return dao.getChallengeRows(id);
    }

    /**
     * Updates the ChallengeRow of a specific NxN Challenge.
     * @param challenge_id The id of the Challenge.
     * @param rowIndex The index the row is at.
     * @param gameId The game that has been selected for that row.
     * @param stars The number of stars (plays) selected for that row.
     */
    public void setChallengeRow(int challenge_id, int rowIndex, int gameId, int stars) {
        dao.setChallengeRow(challenge_id, rowIndex, gameId, stars);
    }

    /**
     * Clears a row in the NxN Challenge.
     * @param challenge_id The id of the Challenge.
     * @param rowIndex The row index to clear, zero-based.
     */
    public void removeChallengeRow(int challenge_id, int rowIndex) { dao.removeChallengeRow(challenge_id, rowIndex); }

    // Alphabet Challenge

    /**
     * Creates a new Alphabet Challenge.
     * @param name The name of the Alphabet Challenge to create.
     * @return The assigned id of the newly created challenge, or -1 if an error occurred.
     */
    public int putAlphabetChallenge(String name) { return dao.putAlphabetChallenge(name); }

    /**
     * Sets a game for a specific letter in the Alphabet Challenge.
     * @param challenge_id The Challenge id to save at.
     * @param letter The letter in the Challenge to save.
     * @param gameId The game id to put at that letter in the given Alphabet Challenge.
     */
    public void setAlphabetGame(int challenge_id, char letter, int gameId) { dao.setAlphabetGame(challenge_id, letter, gameId); }

    /**
     * Gets the instantiated board games for the selected letter in the Alphabet Challenge.
     * @param challenge_id The id of the Alphabet Challenge.
     * @param letter The letter to look up games for.
     * @return An advanced list with instantiated board games.
     */
    public AdvancedList<BoardGame> getAlphabetGames(int challenge_id, char letter) throws IOException
    {
        List<Integer> ids = dao.getAlphabetGames(challenge_id, letter);
        return getBoardGames(ids);
    }

    /**
     * Removes a specific game from a letter in the given Alphabet Challenge.
     * @param challenge_id The id of the Alphabet Challenge.
     * @param c The character to remove the game from.
     * @param gameid The id of the game to remove from the Challenge.
     */
    public void removeAlphabetGame(int challenge_id, char c, int gameid) { dao.removeAlphabetGame(challenge_id, c, gameid); }

    // Unique Challenge

    /**
     * Creates a new Unique Games Challenge in the database.
     * @param name The name of the new Unique Games Challenge.
     * @param games The number of unique games to play.
     */
    public void putUniqueChallenge(String name, int games) { dao.putChallenge(name, games, 1, 2); }

    // Collection Challenge

    /**
     * Creates a new Collection Challenge in the database.
     * @param name The name of the new Collection Challenge.
     */
    public void putCollectionChallenge(String name) { dao.putChallenge(name, 0, 1, 3); }

    /**
     * Sets a game to have been played for the given Collection Challenge.
     * @param challenge_id The id of the Collection Challenge.
     * @param gameId The id of the game.
     */
    public void putCollectionChallengeGame(int challenge_id, int gameId) {dao.putCollectionChallengeGame(challenge_id, gameId); }

    /**
     * Sets a game to unplayed for the given Collection Challenge.
     * @param challenge_id The id of the Collection Challenge.
     * @param gameId The id of the game to mark unplayed.
     */
    public void removeCollectionChallengeGame(int challenge_id, int gameId) {dao.removeCollectionChallengeGame(challenge_id, gameId); }

    /**
     * Gets all game ids for all games marked as played in a Collection Challenge.
     * @param challenge_id The id of the Collection Challenge.
     * @return A collection of ids for the games in the Challenge which have been marked as played.
     */
    public Collection<Integer> getCollectionChallengeGames(int challenge_id) { return dao.getCollectionChallengeGames(challenge_id); }

    // Lend out

    /**
     * Checks if a specific game has been marked as lend out by the given BGG username.
     * @param username The BGG user owning the game.
     * @param gameId The game id of the game to check.
     * @return True if the game is marked as lend out, else false.
     */
    public boolean isLendOut(String username, int gameId) {
        return getLendOut(username,gameId) != null;
    }

    /**
     * Sets a specific game as lend out in the database.
     * @param username The BGG user owning the game.
     * @param gameId The game id of the game that is lend out.
     * @param personName The free-text name of the person who has lend the game.
     */
    public void setLendOut(String username, int gameId, String personName) {
        dao.setLendOut(username,gameId,personName != null ? personName : "");
    }

    /**
     * Gets the person who has lend out a specific game.
     * @param username The BGG user owning the game.
     * @param gameId The game id of the game that might be lend out.
     * @return The free-text name of the person who has lend the game.
     * The empty string if no name were given, but it is lend out, or
     * null of the game is not lend out.
     */
    public String getLendOut(String username, int gameId){
        return dao.getLendOut(username,gameId);
    }

    /**
     * Marks the game as not lend out.
     * @param username The BGG user owning the game.
     * @param gameId The game id of the game that is no longer lend out.
     */
    public void removeLendOut(String username, int gameId) {
        dao.removeLendOut(username, gameId);
    }

    // PRIVATE HELPER METHODS

    /**
     * A utility method for checking if a given timestamp has been expired.
     * @param timestamp The timestamp to check.
     * @param freshPeriod The number of minutes the timestamp was originally
     *                    fresh for.
     * @return True if the timestamp is still fresh, else false.
     */
    private boolean hasExpired(Calendar timestamp, int freshPeriod) {
        Calendar expTime = Calendar.getInstance();
        expTime.add(Calendar.MINUTE,-1*freshPeriod);
        return timestamp.before(expTime);
    }

    /**
     * Stores a Bitmap under the given filename in the app's private
     * directory on the device.
     * @param bitmap The Bitmap to save.
     * @param filename The filename the Bitmap should be saved under.
     */
    private void saveImage(Bitmap bitmap, String filename) {
        if(bitmap == null) return;

        try {
            FileOutputStream fos = c.openFileOutput(filename,
                    Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads a Bitmap with filename for a previously stored file
     * in the app's private directory on the device.
     * @param filename The filename to load as Bitmap.
     * @return The instantiated Bitmap from the file, or null
     * if the file did not exist or was not a Bitmap.
     */
    private Bitmap loadImage(String filename) {
        if (!fileExists(filename))
            return null;

        try {
            FileInputStream fis = c.openFileInput(filename);
            return BitmapFactory.decodeStream(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Checks if a file exists with the given filename
     * in the app's private directory on the device.
     * @param filename The filename to look up.
     * @return True if a file exists with the given filename,
     * else false.
     */
    private boolean fileExists(String filename) {
        return c.getFileStreamPath(filename).exists();
    }
}
