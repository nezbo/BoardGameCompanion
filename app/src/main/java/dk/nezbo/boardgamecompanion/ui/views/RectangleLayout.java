package dk.nezbo.boardgamecompanion.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import dk.nezbo.boardgamecompanion.R;

/**
 * Created by Rasmus on 25-02-2015.
 */
public class RectangleLayout extends LinearLayout {

    private int relativeWidth = 4, relativeHeight = 3;

    public RectangleLayout(Context context) {
        super(context);
    }

    public RectangleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public RectangleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RectangleLayout);

        relativeWidth = a.getInt(R.styleable.RectangleLayout_relativeWidth, 4);
        relativeHeight = a.getInt(R.styleable.RectangleLayout_relativeHeight, 3);

        a.recycle();
    }

    public void setRelativeWidth(int w) {
        relativeWidth = w;
    }

    public void setRelativeHeight(int h){
        relativeHeight = h;
    }

    public int getRelativeWidth() { return relativeWidth; }
    public int getRelativeHeight() { return relativeHeight; }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec)
    {
        int originalWidth = MeasureSpec.getSize(widthMeasureSpec);
        int originalHeight = MeasureSpec.getSize(heightMeasureSpec);

        int finalWidth = originalWidth;
        int finalHeight = (int)(originalWidth*((1.0*relativeHeight)/relativeWidth));

//        System.out.println("RectangleLayout: Before: W="+originalWidth+" H="+originalHeight+" - After: W="+finalWidth+" H="+finalHeight);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
    }
}
