package dk.nezbo.boardgamecompanion.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by Emil on 05-12-2015.
 */
public class SquareImageButton extends ImageButton
{

    public SquareImageButton(final Context context)
    {
        super(context);
    }

    public SquareImageButton(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareImageButton(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
    {
        final int width = getDefaultSize(getSuggestedMinimumWidth(),widthMeasureSpec);
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
    {
        super.onSizeChanged(w, w, oldw, oldh);
    }
}