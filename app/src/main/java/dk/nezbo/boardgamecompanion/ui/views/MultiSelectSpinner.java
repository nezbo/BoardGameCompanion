package dk.nezbo.boardgamecompanion.ui.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Emil on 4/8/2015.
 */
public class MultiSelectSpinner extends Spinner implements DialogInterface.OnMultiChoiceClickListener{

    private static final String ALL_ELEMENT = "ALL";

    String[] items = null;
    boolean[] selections = null;
    ArrayAdapter<String> simple_adapter;
    private boolean selectAllElement;
    private AlertDialog dialog;

    public MultiSelectSpinner(Context context){
        super(context);

        simple_adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item);
        super.setAdapter(simple_adapter);
    }

    public MultiSelectSpinner(Context context, AttributeSet attrs){
        super(context,attrs);

        simple_adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item);
        super.setAdapter(simple_adapter);
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter){
        throw new UnsupportedOperationException("setAdapter is not supported by MultiSelectSpinner");
    }

    @Override
    public boolean performClick(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(items,selections,this);

        dialog = builder.show();
        return true;
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        System.out.println(which);
        if(selections != null && which < selections.length){
            if(selectAllElement && which == 0){
                Arrays.fill(selections,isChecked);
                dialog.dismiss();
            }

            simple_adapter.clear();
            simple_adapter.add(buildSelectedItemString());
        }else{
            throw new IllegalArgumentException("Argument 'which' is out of bounds.");
        }
    }

    // CUSTOM METHODS
    
    public void toggleSelectAllElement(boolean on){
        if(selectAllElement && !on){
            // turn off
            items = Arrays.copyOfRange(items,1,items.length);
            selections = Arrays.copyOfRange(selections,1,selections.length);

        }else if(!selectAllElement && on){
            // turn on
            items = concatenate(new String[]{ALL_ELEMENT},items);
            selections = concat(new boolean[]{allSelected()},selections);
        }
        this.selectAllElement = on;


    }

    public void setAll(boolean checked){
        Arrays.fill(selections,checked);
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setItems(String[] items){
        this.items = items;
        selections = new boolean[items.length];
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
        Arrays.fill(selections, false);
    }

    public void setItems(List<String> items){
        setItems(items.toArray(new String[items.size()]));
    }

    public List<Integer> getSelectedIndices(){
        List<Integer> selection = new LinkedList<>();

        if(items == null)
            return selection;

        for(int i = 0; i < items.length; i++){
            if(selections[i])
                selection.add(i);
        }
        return selection;
    }

    public List<String> getSelectedItems(){
        List<String> result = new LinkedList<>();

        if(items == null)
            return result;

        for(int i = 0; i < items.length; i++){
            if(selections[i])
                result.add(items[i]);
        }
        return result;
    }

    // PRIVATE METHODS

    private boolean allSelected() {
        for(boolean b : selections)
            if(!b)
                return false;
        return true;
    }

    public boolean[] concat(boolean[] a, boolean[] b) {
        int aLen = a.length;
        int bLen = b.length;
        boolean[] c= new boolean[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    private <T> T[] concatenate (T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }

    private String buildSelectedItemString() {
        LinkedList<String> selected = new LinkedList<>();
        LinkedList<String> notSelected = new LinkedList<>();

        for (int i = selectAllElement ? 1 : 0; i < items.length; i++) {
            if (selections[i]) {
                selected.add(items[i]);
            } else {
                notSelected.add(items[i]);
            }
        }
        if (selected.isEmpty()){
            return "None";
        } else if (notSelected.isEmpty()) {
            return "All";
        } else  if (selected.size() < 5) {
            return listToString(selected);
        } else if (notSelected.size() < 5) {
            return "Not: " + listToString(notSelected);
        } else {
            return selected.size()+" items";
        }
    }

    private String listToString(List<String> list){
        StringBuilder builder = new StringBuilder();
        boolean foundOne = false;

        for(String str : list){
            if (foundOne) {
                builder.append(", ");
            }
            foundOne = true;
            builder.append(str);
        }
        return builder.toString();
    }
}
