package dk.nezbo.boardgamecompanion.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import dk.nezbo.boardgamecompanion.R;


public class UsernameActivity extends ActionBarActivity {

    private EditText textfield;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_username);
        init();
    }

    private void init() {
        final Button b = (Button) findViewById(R.id.usButton);
        textfield = (EditText) findViewById(R.id.etUsername);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUsernameAndExit();
            }
        });
        
        textfield.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                saveUsernameAndExit();
                return true;
            }
        });
    }

    private void saveUsernameAndExit() {
        String text = textfield.getText().toString();

        // save in settings
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(UsernameActivity.this);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("pref_username",text);
        edit.commit();

        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_username, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
