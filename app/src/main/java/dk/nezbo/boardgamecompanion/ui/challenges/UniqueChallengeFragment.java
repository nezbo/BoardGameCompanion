package dk.nezbo.boardgamecompanion.ui.challenges;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.views.RectangleLayout;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class UniqueChallengeFragment extends Fragment {

    private static final String CHALLENGE_ID = "challenge_id";
    private static final int GAMES_PER_ROW = 5;

    private int challenge_id;
    private BGGAndroidCache dao;
    public GamesLoaderTask loaderTask = null;
    public AsyncTask<Void, Void, AdvancedList<Play>> asynctask = null;

    private CompanionDAO.Challenge challenge;
    private TextView tvFraction, tvTimeLeft, tvTimeTotal;
    private ArrayList<ImageView> gameViews;

    public static UniqueChallengeFragment newInstance(int challenge_id) {
        UniqueChallengeFragment fragment = new UniqueChallengeFragment();
        Bundle args = new Bundle();
        args.putInt(CHALLENGE_ID, challenge_id);
        fragment.setArguments(args);
        return fragment;
    }

    public UniqueChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            challenge_id = getArguments().getInt(CHALLENGE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challenge_unique, container, false);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dao == null) {
            dao = new BGGAndroidCache(getActivity());
        }
        this.challenge = dao.getChallenge(challenge_id);

        int games = this.challenge.getGamesCount();
        int inLastRow = games % UniqueChallengeFragment.GAMES_PER_ROW;
        int rows = (games / UniqueChallengeFragment.GAMES_PER_ROW);
        if(inLastRow > 0) {
            rows++;
        } else {
            inLastRow = 5;
        }

        // Stats
        this.tvFraction = (TextView) getView().findViewById(R.id.tvGamesFrac);
        this.tvTimeTotal = (TextView) getView().findViewById(R.id.tvTimeTotal);
        this.tvTimeLeft = (TextView) getView().findViewById(R.id.tvTimeLeft);

        // Layout
        RectangleLayout cont = (RectangleLayout) getView().findViewById(R.id.rctlChallengeContainer);
        cont.removeAllViews();
        cont.setRelativeHeight(rows);
        cont.setRelativeWidth(UniqueChallengeFragment.GAMES_PER_ROW);
        cont.setWeightSum(rows);

        this.gameViews = new ArrayList<ImageView>(games);

        for (int row = 0; row < rows; row++) {
            LinearLayout rowLL = new LinearLayout(getActivity());
            rowLL.setOrientation(LinearLayout.HORIZONTAL);
            rowLL.setWeightSum(cont.getRelativeWidth());
            rowLL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

            // The games in the row
            int gamesInRow = (row < rows-1) ? UniqueChallengeFragment.GAMES_PER_ROW : inLastRow;
            for (int i = 0; i < gamesInRow ; i++) {
                ImageView b = Utilities.makeGameIcon(getActivity(), true, null);
                rowLL.addView(b);
                gameViews.add(b);
            }

            cont.addView(rowLL);
        }

        this.refreshChallengeGames();
    }

    public void refreshChallengeGames(final int... indices) {

        if (challenge.isManual()) {
            if(loaderTask != null) {
                loaderTask.cancel(true);
                loaderTask = null;
            }
            loaderTask = new GamesLoaderTask(getActivity(), false) {
                @Override
                public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                    return cache.getChallengeGames(challenge_id).map(new AdvancedList.Map<BoardGame, Integer>() {
                        @Override
                        public Integer map(BoardGame item) {
                            return item.getId();
                        }
                    });
                }

                @Override
                public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                    // updating "rows" separately

                    HashMap<Integer, CompanionDAO.ChallengeRow> challengeRows = dao.getChallengeRows(challenge_id);
                    for(int i = 0; i < ((indices.length > 0) ? indices.length : gameViews.size()); i++) {
                        final int index = indices.length > 0 ? indices[i] : i;
                        if(challengeRows.containsKey(index)) {
                            new GameImagePopulator(gameViews.get(index), challengeRows.get(index).getGameId(), dao).execute();
                            gameViews.get(index).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dao.removeChallengeRow(challenge_id, index);
                                    refreshChallengeGames(index);
                                }
                            });
                        } else {
                            gameViews.get(index).setImageResource(R.drawable.unselected);
                            gameViews.get(index).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Utilities.performSearchForGame(getActivity(), dao, new BoardGameAdapter.OnGameClickListener() {
                                        @Override
                                        public void onClick(BoardGame game, int position) {
                                            dao.setChallengeRow(challenge_id, index, game.getId(), -1);
                                            refreshChallengeGames(index);
                                        }
                                    });
                                }
                            });
                        }
                    }

                    // Calculate progress
                    updateStats(loadedGames);
                }
            };
            loaderTask.execute();


        } else {
            this.asynctask = new AsyncTask<Void, Void, AdvancedList<Play>>() {
                @Override
                protected AdvancedList<Play> doInBackground(Void... params) {
                    try {
                        final boolean inclIncomplete = Utilities.prefInclIncomplete(getActivity());

                        final AdvancedList.Filter thisYearFilter = new Utilities.IsThisYearFilter(challenge.getStart(),Utilities.prefInclIncomplete(getActivity()));

                        if(this.isCancelled())
                            return null;

                        AdvancedList<Play> plays = dao.getPlays(Utilities.getUsername(getActivity()));

                        boolean allTime = challenge.getStart() == CompanionDAO.Challenge.ChallengeStart.All_Time;
                        AdvancedList<Play> finalPlays = null;
                        if(allTime) {
                            finalPlays = plays;
                        } else {
                            HashMap<Boolean, AdvancedList<Play>> splitted = plays.groupBy(new AdvancedList.Map<Play, Boolean>() {
                                @Override
                                public Boolean map(Play item) {
                                    return thisYearFilter.filter(item);
                                }
                            });
                            // Map it to game id -> play in both
                            final AdvancedList<Integer> beforeGameIds = splitted.get(false).map(new AdvancedList.Map<Play, Integer>() {
                                @Override
                                public Integer map(Play item) {
                                    return item.getGameID();
                                }
                            });

                            finalPlays = splitted.get(true).filter(new AdvancedList.Filter<Play>() {
                                @Override
                                public boolean filter(Play item) {
                                    return !beforeGameIds.contains(item.getGameID());
                                }
                            });
                        }

                        finalPlays = finalPlays.filter(new AdvancedList.Filter<Play>() {
                            @Override
                            public boolean filter(Play item) {
                                return (inclIncomplete || !item.isIncomplete());
                            }
                        }).sort(Utilities.timeComparator).distinctBy(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        });

                        if(this.isCancelled())
                            return null;

                        // Preload the Games
                        dao.getBoardGames(plays.map(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        }));

                        return finalPlays;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(final AdvancedList<Play> plays) {
                    super.onPostExecute(plays);

                    for(int i = 0; i < gameViews.size(); i++) {
                        if(i < plays.size()) {
                            new GameImagePopulator(gameViews.get(i), plays.get(i).getGameID(), dao).execute();
                            final int finalI = i;
                            gameViews.get(i).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(UniqueChallengeFragment.this.getActivity(), plays.get(finalI).toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else { // Make sure resource is "unknown"
                            gameViews.get(i).setImageResource(R.drawable.unselected);
                        }
                    }

                    // Calculate progress
                    try {
                        updateStats(dao.getBoardGames(plays.map(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        })));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            asynctask.execute();
        }
    }

    private void updateStats(AdvancedList<BoardGame> games) {
        int totalTime = 0;
        for(int i = 0; i < games.size(); i++) {
            totalTime += games.get(i).getPlayingTime();
        }
        double averageTime = games.size() > 0 ? (1.0*totalTime) / games.size() : 0;

        this.tvTimeTotal.setText(Utilities.formatTime((int) (averageTime * gameViews.size())));
        this.tvTimeLeft.setText(Utilities.formatTime((int) (averageTime * (gameViews.size() - games.size()))));

        this.tvFraction.setText(games.size()+" / "+gameViews.size());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }
        if (asynctask != null) {
            asynctask.cancel(true);
            asynctask = null;
        }

        dao.close();
        dao = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    // CLASS

    private class BoardGameView {

        private ImageView view;
        private BoardGame game;

        public BoardGameView(ImageView view, BoardGame game) {
            this.game = game;
            this.view = view;
        }

        public ImageView getView() {
            return view;
        }

        public void setView(ImageView view) {
            this.view = view;
        }

        public BoardGame getGame() {
            return game;
        }

        public void setGame(BoardGame game) {
            this.game = game;
        }
    }
}
