package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.UserNotFoundException;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.database.DBHelper;
import dk.nezbo.boardgamecompanion.utilities.AsyncTaskExecutor;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * Created by Emil on 12-03-2015.
 */
public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if(savedInstanceState == null){
            Fragment newFragment = new PlaceholderFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.flFragmentContainer,newFragment).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_settings, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends PreferenceFragment {


        public PlaceholderFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);

            init();
        }

        private void init() {
            // Update Data
            Preference pref_updatedata = findPreference("pref_updatedata");
            pref_updatedata.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    // Start Async Task with progress
                    final BGGAndroidCache cache = new BGGAndroidCache(getActivity());

                    final Activity act = getActivity();
                    if(act == null){
                        // because someone apparently crashed where it was null
                        return false;
                    }

                    ProgressDialog pdialog = new ProgressDialog(getActivity());

                    new AsyncTaskExecutor(pdialog){

                        @Override
                        protected Void doInBackground(Void... params) {
                            int total = 4;
                            try {
                                updateProgress("Downloading Collection",total); //TODO: Challenge id hardcoded to 0
                                Set<Integer> existingGames = new HashSet<>(new AdvancedList<>(cache.getChallengeRows(0).values()).map(new AdvancedList.Map<CompanionDAO.ChallengeRow, Integer>() {
                                    @Override
                                    public Integer map(CompanionDAO.ChallengeRow game) {
                                        return game.getGameId();
                                    }
                                }));
                                existingGames.addAll(cache.getCollection(Utilities.getUsername(getActivity()),true).keySet());

                                // Extra Users
                                String[] extraUsernames = Utilities.prefExtraUsers(getActivity());
                                for(String user : extraUsernames){
                                    existingGames.addAll(cache.getCollection(user,true).keySet());
                                }

                                updateProgress("Downloading Games", total);
                                cache.getBoardGames(new AdvancedList<>(existingGames),true);

                                updateProgress("Downloading Plays",total);
                                Utilities.getPlays(true,cache,getActivity());

                                updateProgress("Downloading Images",total);
                                for(Integer id : existingGames){
                                    cache.getBitmap(id, DBHelper.BoardGameAttribute.thumb_url);
                                }

                                updateProgress("Everything Completed",total);

                            } catch (IOException|UserNotFoundException e) {
                                // Complains about leaked window whenever I do anything, so whatever
                            }

                            return null;
                        }
                    }.execute();

                    return true;
                }
            });
        }
    }
}
