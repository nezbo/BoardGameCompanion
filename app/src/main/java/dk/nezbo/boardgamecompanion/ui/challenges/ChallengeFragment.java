package dk.nezbo.boardgamecompanion.ui.challenges;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.views.RectangleLayout;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.ChallengeStarsPopulator;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class ChallengeFragment extends Fragment {

    private static final String CHALLENGE_ID = "challenge_id";

    private int challenge_id;

    private ChallengeRowViews[] starViews;

    private BGGAndroidCache dao;

    public GamesLoaderTask loaderTask = null;
    private TextView tvTotal, tvLeft;

    private AsyncTask<Void, Void, int[]> backgroundTotalTask = null;
    private CompanionDAO.Challenge challenge;


    public static ChallengeFragment newInstance(int challenge_id) {
        ChallengeFragment fragment = new ChallengeFragment();
        Bundle args = new Bundle();
        args.putInt(CHALLENGE_ID, challenge_id);
        fragment.setArguments(args);
        return fragment;
    }

    public ChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            challenge_id = getArguments().getInt(CHALLENGE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challenge_10x10, container, false);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dao == null) {
            dao = new BGGAndroidCache(getActivity());
        }
        this.challenge = dao.getChallenge(challenge_id);

        // time
        tvTotal = (TextView) getView().findViewById(R.id.tvTimeTotal);
        tvLeft = (TextView) getView().findViewById(R.id.tvTimeLeft);

        // Create stars
        RectangleLayout cont = (RectangleLayout) getView().findViewById(R.id.rctlChallengeContainer);
        cont.removeAllViews();
        cont.setRelativeHeight(challenge.getGamesCount());
        cont.setRelativeWidth(challenge.getTimes() + 1);
        cont.setWeightSum(challenge.getGamesCount());

        // preparations
        starViews = new ChallengeRowViews[challenge.getGamesCount()];

        // rows of games with stars
        for (int i = 0; i < challenge.getGamesCount(); i++) {
            LinearLayout row = new LinearLayout(getActivity());
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setWeightSum(challenge.getTimes() + 1.0f);
            row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

            // the game image
            final int finalI = i;
            ImageButton button = Utilities.makeGameIcon(getActivity(), true, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSearch(finalI, (ImageView) v);
                }
            });
            row.addView(button);

            // save stars for async populator
            ImageView[] stars = new ImageView[challenge.getTimes()];

            // actually create the stars
            LinearLayout.LayoutParams weightParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            for (int j = 0; j < challenge.getTimes(); j++) {
                ImageView star = new ImageView(getActivity());
                star.setLayoutParams(weightParams);
                star.setScaleType(ImageView.ScaleType.FIT_CENTER);
                star.setImageResource(R.drawable.star_none);

                stars[j] = star;
                row.addView(star);
            }

            cont.addView(row);
            starViews[i] = new ChallengeRowViews(button, stars);

            // if manual, put listeners on stars
            if (challenge.isManual()) {
                ImageView[] starsForListener = starViews[i].getStars();
                for (int j = 0; j < challenge.getTimes(); j++) {
                    final int finalJ = j;
                    starsForListener[j].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CompanionDAO.ChallengeRow currentRow = dao.getChallengeRows(ChallengeFragment.this.challenge_id).get(finalI);

                            // check if it is an empty row
                            if (currentRow == null || currentRow.getGameId() < 0)
                                return;

                            int toSet = 0;
                            if (currentRow.getStars() == finalJ + 1) { // clicked on last star (remove this)
                                toSet = finalJ;
                            } else { // clicked somewhere else, set stars to that amount
                                toSet = finalJ + 1;
                            }
                            starViews[finalI].updateStars(toSet);
                            dao.setChallengeRow(challenge_id, finalI, currentRow.getGameId(), toSet);
                            calculateTotals();
                        }
                    });
                }
            }
        }

        this.refreshChallengeRows();
    }

    public void refreshChallengeRows(Integer... ids) {
        HashMap<Integer, CompanionDAO.ChallengeRow> rows = dao.getChallengeRows(ChallengeFragment.this.challenge_id);

        if (ids.length > 0) {
            for (Integer id : ids) {
                if (rows.containsKey(id))
                    updateRow(id, rows.get(id));
            }
        } else { // update everything
            int size = challenge.getGamesCount();
            for (int id = 0; id < size; id++) {
                if (rows.containsKey(id))
                    updateRow(id, rows.get(id));
            }
        }

        calculateTotals();
    }

    private void updateRow(int rowIndex, CompanionDAO.ChallengeRow rowData) {
        if (challenge.isManual()) { // from database
            starViews[rowIndex].updateStars(rowData.getStars());
        } else { // from plays
            new ChallengeStarsPopulator(getActivity(), challenge, starViews[rowIndex]).execute(rowData.getGameId());
        }
        new GameImagePopulator(starViews[rowIndex].thumb, rowData.getGameId(), dao).execute();
    }

    private void calculateTotals() {
        // calculate time total/left async
        backgroundTotalTask = new AsyncTask<Void, Void, int[]>() {

            @Override
            protected int[] doInBackground(Void... params) {
                try {
                    int totalTime = 0;
                    int time = 0;
                    if (challenge.isManual()) {
                        // manual
                        AdvancedList<CompanionDAO.ChallengeRow> challengeRows = new AdvancedList<>(dao.getChallengeRows(ChallengeFragment.this.challenge_id).values()).filter(new AdvancedList.Filter<CompanionDAO.ChallengeRow>() {
                            @Override
                            public boolean filter(CompanionDAO.ChallengeRow game) {
                                return game.getGameId() > 0;
                            }
                        });
                        System.out.println(challengeRows.size());
                        AdvancedList<BoardGame> games = dao.getChallengeGames(ChallengeFragment.this.challenge_id);

                        if (isCancelled())
                            return null;

                        for (CompanionDAO.ChallengeRow row : challengeRows) {
                            BoardGame game = null;
                            for (BoardGame bg : games) {
                                if (bg.getId() == row.getGameId()) {
                                    game = bg;
                                    break;
                                }
                            }
                            time += game.getPlayingTime() * row.getStars();
                            totalTime += game.getPlayingTime() * challenge.getTimes();
                        }
                    } else {
                        // from plays
                        AdvancedList<Play> plays = Utilities.getPlays(false, dao, getActivity());

                        if (isCancelled()) return null;

                        AdvancedList<BoardGame> games = dao.getChallengeGames(ChallengeFragment.this.challenge_id);

                        if (isCancelled()) return null;

                        for (BoardGame bg : games) {
                            if(bg != null) {
                                time += Math.min(challenge.getTimes(), Utilities.getValidChallengePlaysCount(plays, bg.getId(), challenge, getActivity())) * bg.getPlayingTime();
                                totalTime += challenge.getTimes() * bg.getPlayingTime();
                            }
                        }
                    }

                    return new int[]{totalTime - time, totalTime};
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(int[] time) {
                if (time == null) {
                    Utilities.handleIOException(getActivity());
                    tvLeft.setText("Unknown");
                    tvTotal.setText("Unknown");
                } else {
                    for (int i = 0; i < 2; i++) {
                        TextView tv = i == 0 ? tvLeft : tvTotal;
                        tv.setText(Utilities.formatTime(time[i]));
                    }
                }
            }
        };
        backgroundTotalTask.execute();

    }

    private void showSearch(final int rowIndex, final ImageView image) {
        if(loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }
        loaderTask = Utilities.performSearchForGame(getActivity(), dao, new BoardGameAdapter.OnGameClickListener() {
            @Override
            public void onClick(BoardGame game, int position) {
                // finally select the game
                BGGAndroidCache cache = new BGGAndroidCache(getActivity());
                cache.setChallengeRow(ChallengeFragment.this.challenge_id, rowIndex, game.getId(), 0);
                refreshChallengeRows(rowIndex);
                cache.close();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }
        if (backgroundTotalTask != null) {
            backgroundTotalTask.cancel(true);
            backgroundTotalTask = null;
        }

        dao.close();
        dao = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public class ChallengeRowViews {

        private final ImageButton thumb;
        private final ImageView[] stars;

        public ChallengeRowViews(ImageButton thumb, ImageView[] stars) {
            this.thumb = thumb;
            this.stars = stars;
        }

        public ImageButton getThumb() {
            return thumb;
        }

        public ImageView[] getStars() {
            return stars;
        }

        public void updateImage(Bitmap bitmap) {
            thumb.setImageBitmap(bitmap);
        }

        public void updateStars(final List<String> plays) {
            if (plays == null)
                return;

            // Modify the X first stars corresponding to plays
            for (int i = 0; i < stars.length; i++) {
                if (plays.size() > i) {
                    stars[i].setImageResource(R.drawable.star_full);
                    final int finalI = i;
                    stars[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), plays.get(finalI), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    stars[i].setImageResource(R.drawable.star_none);
                    stars[i].setOnClickListener(new View.OnClickListener() { // do nothing
                        @Override
                        public void onClick(View v) {
                        }
                    });
                }

            }
        }

        public void updateStars(int amount) {
            for (int i = 0; i < stars.length; i++) {
                if (amount > i) {
                    stars[i].setImageResource(R.drawable.star_full);
                } else {
                    stars[i].setImageResource(R.drawable.star_none);
                }
            }
        }
    }
}
