package dk.nezbo.boardgamecompanion.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dk.nezbo.boardgamecompanion.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChallengeNewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChallengeNewFragment extends Fragment implements View.OnClickListener{

    private NewChallengeListener onNewChallengeListener;
    private EditText tbtgames, tbttimes, uniqueGames;
    private Button tbtconfirm, alphabetConfirm, uniqueConfirm, collectionConfirm;
    private View tenbytenroot, alphabetroot, uniqueroot, collroot;

    public static ChallengeNewFragment newInstance(NewChallengeListener listener) {
        ChallengeNewFragment fragment = new ChallengeNewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setOnNewChallengeListener(listener);
        return fragment;
    }

    public ChallengeNewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challenge_new, container, false);

        this.tenbytenroot = root.findViewById(R.id.inclTenByTen);
        this.alphabetroot = root.findViewById(R.id.inclAlphabet);
        this.uniqueroot = root.findViewById(R.id.inclUnique);
        this.collroot = root.findViewById(R.id.inclCollection);

        this.tbtgames = (EditText) tenbytenroot.findViewById(R.id.etChallNewGames);
        this.tbttimes = (EditText) tenbytenroot.findViewById(R.id.etChallNewTimes);
        this.tbtconfirm = (Button) tenbytenroot.findViewById(R.id.bChallNewConfirm);
        this.tbtconfirm.setOnClickListener(this);

        this.alphabetConfirm = (Button) alphabetroot.findViewById(R.id.bChallNewConfirm);
        this.alphabetConfirm.setOnClickListener(this);

        this.uniqueGames = (EditText) uniqueroot.findViewById(R.id.etChallNewGames);
        this.uniqueConfirm = (Button) uniqueroot.findViewById(R.id.bChallNewConfirm);
        this.uniqueConfirm.setOnClickListener(this);

        this.collectionConfirm = (Button) collroot.findViewById(R.id.bChallCollConfirm);
        this.collectionConfirm.setOnClickListener(this);

        return root;
    }

    public void setOnNewChallengeListener(NewChallengeListener onNewChallengeListener) {
        this.onNewChallengeListener = onNewChallengeListener;
    }

    @Override
    public void onClick(View v) {
        if(this.onNewChallengeListener != null) {
            if(v == this.tbtconfirm) {
                try {
                    int games = Integer.parseInt(this.tbtgames.getText().toString());
                    int times = Integer.parseInt(this.tbttimes.getText().toString());
                    if(games > 0 && games <= 100 && times > 0 && times <= 25) {
                        this.onNewChallengeListener.onCreateTenByTenChallenge(games+"x"+times+" Challenge", games, times);
                    }
                }catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), "Number of games and amount not chosen", Toast.LENGTH_SHORT).show();
                }
            } else if(v == this.alphabetConfirm) {
                this.onNewChallengeListener.onCreateAlphabetChallenge("Alphabet Challenge");
            } else if(v == this.uniqueConfirm) {
                try {
                    int games = Integer.parseInt(this.uniqueGames.getText().toString());
                    if(games > 0 && games < 1000) {
                        this.onNewChallengeListener.onCreateUniqueChallenge(games+" New Games Challenge", games);
                    }
                }catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), "Number of games not chosen", Toast.LENGTH_SHORT).show();
                }
            } else if(v == this.collectionConfirm) {
                this.onNewChallengeListener.onCreateCollectionChallenge("Collection Challenge");
            }
        }
    }

    public interface NewChallengeListener {
        void onCreateTenByTenChallenge(String name, int games, int times);
        void onCreateAlphabetChallenge(String name);
        void onCreateUniqueChallenge(String name, int games);
        void onCreateCollectionChallenge(String name);
    }
}
