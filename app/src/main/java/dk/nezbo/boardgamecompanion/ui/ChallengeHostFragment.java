package dk.nezbo.boardgamecompanion.ui;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.android.common.view.SlidingTabLayout;

import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.utilities.ChallengePageAdapter;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * This Fragment holds a ViewPager with all of the user's Challenges and a final tab to add new
 * Challenges. There are also controls displayed to change the settings of the currently displayed
 * Challenge.
 */
public class ChallengeHostFragment extends Fragment implements ChallengePageAdapter.ChallengesChangedListener, View.OnClickListener, ViewPager.OnPageChangeListener {

    private ViewPager pager;
    private SlidingTabLayout slidingTabs;
    private ChallengePageAdapter adapter;
    private LinearLayout editLinearLayout;
    private ImageButton editRenameButton, editDeleteButton, editTimeButton, editManualButton;
    private BGGAndroidCache dao;

    /**
     * Factory method for instantiating the ChallengeHostFragment. This is the preferred way of
     * instantiating this.
     * @return The initialized instance of a ChallengeHostFragment.
     */
    public static ChallengeHostFragment newInstance() {
        // Nothing to do here yet
        return new ChallengeHostFragment();
    }

    /**
     * Obligatory non-args constructor for Fragments.
     */
    public ChallengeHostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dao.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.dao = new BGGAndroidCache(getActivity());
        
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challengehost, container, false);

        this.adapter = new ChallengePageAdapter(getActivity().getSupportFragmentManager(), dao);
        adapter.setChallengesChangedListener(this);

        this.pager = (ViewPager) root.findViewById(R.id.vpHostPager);
        this.pager.setAdapter(adapter);

        this.slidingTabs = (SlidingTabLayout) root.findViewById(R.id.slHostTab);
        this.slidingTabs.setViewPager(pager);
        this.slidingTabs.setOnPageChangeListener(this);

        // Edit bar
        this.editLinearLayout = (LinearLayout) root.findViewById(R.id.llChallEdit);
        this.editRenameButton = (ImageButton) root.findViewById(R.id.ibChallEdit);
        this.editDeleteButton = (ImageButton) root.findViewById(R.id.ibChallDelete);
        this.editTimeButton = (ImageButton) root.findViewById(R.id.ibChallTime);
        this.editManualButton = (ImageButton) root.findViewById(R.id.ibChallManual);
        this.editRenameButton.setOnClickListener(this);
        this.editDeleteButton.setOnClickListener(this);
        this.editTimeButton.setOnClickListener(this);
        this.editManualButton.setOnClickListener(this);

        this.update();

        return root;
    }

    /**
     * Updates all Challenges by loading them from the database and simply sets the adapter and
     * ViewPager from scratch due to a bug in the SlidingTabLayout.
     */
    private void update() {
        System.out.println("Challenges Updated");
        int current = this.pager.getCurrentItem();
        adapter.updateChallenges();
        this.pager.setAdapter(adapter);
        this.slidingTabs.setViewPager(pager);
        this.pager.setCurrentItem(current);
        onPageSelected(current);
    }

    @Override
    public void onClick(View v) {
        if(v == editRenameButton) {
            showEditTitleDialog();

        } else if (v == editDeleteButton) {
            final int oldIndexOfChallenge = this.pager.getCurrentItem();
            final CompanionDAO.Challenge challenge = adapter.getChallenges().get(oldIndexOfChallenge);

            // Lets make sure they want to delete it
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Are you sure?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dao.removeChallenge(challenge.getID());
                    System.out.println("Challenge Deleted");
                    update();
                    pager.setCurrentItem(oldIndexOfChallenge == 0 ? 0 : oldIndexOfChallenge - 1);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.create().show();
        } else if (v == editTimeButton) {
            showEditTimeDialog();
        } else if (v == editManualButton) {
            showEditManualDialog();
        }
    }

    @Override
    public void onChallengeCreated(CompanionDAO.Challenge challenge, int indexOfNewChallenge) {
        System.out.println("Challenge Created");
        this.update();
        this.pager.setCurrentItem(indexOfNewChallenge);
    }

    // PRIVATE HELPER METHODS

    /**
     * Shows a dialog to select between the two possible timespams for the Challenge.
     */
    private void showEditTimeDialog() {
        final CompanionDAO.Challenge challenge = adapter.getChallenges().get(pager.getCurrentItem());
        int current = challenge.getStart() == CompanionDAO.Challenge.ChallengeStart.All_Time ? 0 : 1;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.challenge_time).setSingleChoiceItems(R.array.pref_start_options, current, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.setChallengeTime(challenge.getID(), which == 0);
                ChallengeHostFragment.this.update();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Shows a dialog to select between the two possible ways of logging plays for the Challenge.
     */
    private void showEditManualDialog() {
        final CompanionDAO.Challenge challenge = adapter.getChallenges().get(pager.getCurrentItem());
        int current = challenge.isManual() ? 1 : 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.challenge_manual).setSingleChoiceItems(R.array.pref_manual_options, current, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.setChallengeManual(challenge.getID(), which == 1);
                ChallengeHostFragment.this.update();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Shows a dialog to select the name of the current Challenge.
     */
    private void showEditTitleDialog() {
        final CompanionDAO.Challenge challenge = adapter.getChallenges().get(pager.getCurrentItem());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        builder.setPositiveButton("Done",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = editText.getText().toString().trim();

                if(name.length() > 0) {

                    dao.setNxNChallenge(challenge.getID(), name, challenge.getGamesCount(), challenge.getTimes());
                    ChallengeHostFragment.this.update();
                }
            }
        });
        builder.setView(editText);
        editText.setText(challenge.getName());
        builder.setTitle("Rename Challenge");
        builder.show();
        Utilities.showKeyboard(editText, getActivity());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        if(position >= adapter.getChallengesCount()) {
            this.editLinearLayout.setVisibility(View.GONE);
        }else {
            this.editLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}
