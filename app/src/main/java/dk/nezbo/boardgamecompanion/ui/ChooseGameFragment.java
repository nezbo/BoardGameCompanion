package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.BoardGameWeight;
import bggapi.model.UserNotFoundException;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.ui.views.MultiSelectSpinner;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseGameFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseGameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseGameFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private BGGAndroidCache cache;
    private GamesLoaderTask loaderTask = null;

    private MultiSelectSpinner spMechanics, spCategories;
    private Spinner spBGClass;

    private AsyncTask backgroundLoader = null;
    private String lastUsername = null;

    private boolean isRequiredCats;
    private boolean isRequiredMechs;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChooseGameFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChooseGameFragment newInstance(String param1, String param2) {
        ChooseGameFragment fragment = new ChooseGameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ChooseGameFragment() {
        // required
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_choose_game, container, false);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(cache == null)
            cache = new BGGAndroidCache(getActivity());

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        if(loaderTask != null){
            loaderTask.cancel(true);
            loaderTask = null;
        }
        if(backgroundLoader != null){
            backgroundLoader.cancel(true);
            backgroundLoader = null;
        }

        cache.close();
        cache = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        cache = new BGGAndroidCache(getActivity());

        final EditText etPlayers = (EditText)getView().findViewById(R.id.etPlayers);
        final EditText etMaxTime = (EditText)getView().findViewById(R.id.etPlayingTime);
        final RadioGroup rgOrdering = (RadioGroup)getView().findViewById(R.id.rgOrdering);
        spBGClass = (Spinner) getView().findViewById(R.id.spBGClass);
        final MultiSelectSpinner spWeight = (MultiSelectSpinner)getView().findViewById(R.id.spWeight);
        spCategories = (MultiSelectSpinner)getView().findViewById(R.id.spCategories);
        spMechanics = (MultiSelectSpinner)getView().findViewById(R.id.spMechanics);

        final TextView tvCategories = (TextView) getView().findViewById(R.id.tvCategories);
        final TextView tvMechanics = (TextView) getView().findViewById(R.id.tvMechanics);
        ImageButton ibSwapCats = (ImageButton) getView().findViewById(R.id.ibSwapCategories);
        ImageButton ibSwapMechs = (ImageButton) getView().findViewById(R.id.ibSwapMechanics);
        isRequiredCats = true;
        isRequiredMechs = true;

        Button findGames = (Button)getView().findViewById(R.id.bBestMatch);

        // Swappers
        ibSwapCats.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isRequiredCats = !isRequiredCats;
                if(isRequiredCats){
                    tvCategories.setText("Required Categories");
                }else{
                    tvCategories.setText("Forbidden Categories");
                }
            }
        });
        ibSwapMechs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRequiredMechs = !isRequiredMechs;
                if(isRequiredMechs){
                    tvMechanics.setText("Required Mechanics");
                }else{
                    tvMechanics.setText("Forbidden Mechanics");
                }
            }
        });

        // MultiSelectSpinner data
        spWeight.setItems(Utilities.toStrings(BoardGameWeight.values()));
        spWeight.setAll(true);
        updateSpinners(new ArrayList<String>(),new ArrayList<String>(),new AdvancedList<String>("ANY"));

        findGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Gather attributes
                final int players = Utilities.safeParseInt(etPlayers.getText().toString());
                final int maxTime = Utilities.safeParseInt(etMaxTime.getText().toString());
                final Set<BoardGameWeight> weights = BoardGameWeight.fromIndices(spWeight.getSelectedIndices());
                final List<String> selectedCats = spCategories.getSelectedItems();
                final List<String> selectedMechs = spMechanics.getSelectedItems();

                int radioButtonID = rgOrdering.getCheckedRadioButtonId();
                View radioButton = rgOrdering.findViewById(radioButtonID);
                final int sortMethod = rgOrdering.indexOfChild(radioButton);
                final boolean userRating = Utilities.prefUserRating(getActivity());
                final boolean inclExpansions = Utilities.prefInclExpansions(getActivity());
                final boolean inclLendOut = Utilities.prefInclLendOut(getActivity());

                final String[] extraUsernames = Utilities.prefExtraUsers(getActivity());

                loaderTask = new GamesLoaderTask(getActivity(),true) {
                    @Override
                    public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException{
                        ArrayList<Integer> games = new ArrayList<Integer>(cache.getCollection(Utilities.getUsername(getActivity())).keySet());

                        // search
                        for (String username : extraUsernames) {
                            try {
                                Set<Integer> collection = cache.getCollection(username).keySet();
                                games.addAll(collection);
                            } catch (UserNotFoundException e) {
                                /* too freaking bad */
                                System.err.println("Username '" + username + "' not found on BGG");
                            }
                        }

                        return games;
                    }

                    @Override
                    public AdvancedList<BoardGame> postLoadHandle(AdvancedList<BoardGame> initialGames, final BGGAndroidCache cache) {
                        final String username = Utilities.getUsername(getActivity());

                        // filter
                        AdvancedList<BoardGame> games = initialGames.filter(new AdvancedList.Filter<BoardGame>() {
                            @Override
                            public boolean filter(BoardGame game) {
                                boolean bCategories = isRequiredCats ? game.getLinkNames(BoardGame.LinkTypes.CATEGORY).containsAll(selectedCats) : !Utilities.containsAny(game.getLinkNames(BoardGame.LinkTypes.CATEGORY),selectedCats);
                                boolean bMechanics = isRequiredMechs ? game.getLinkNames(BoardGame.LinkTypes.MECHANIC).containsAll(selectedMechs) : !Utilities.containsAny(game.getLinkNames(BoardGame.LinkTypes.MECHANIC),selectedMechs);

                                return (players == -1 || game.allowsPlayers(players)) &&
//                                        (maxTime == -1 || game.getMaxPlayingTime() <= maxTime) &&
                                        (weights.contains(game.getWeightEnum())) &&
                                        (spBGClass.getSelectedItem().equals("ANY") ||
                                                game.getBoardGameClass().equals(spBGClass.getSelectedItem())) &&
                                        (inclExpansions || !game.isExpansion() &&
                                        (inclLendOut || !cache.isLendOut(username, game.getId())) &&
                                        bCategories && bMechanics
                                        );
                            }
                        });

                        // order
                        switch (sortMethod) {
                            case 0: {
                                games = games.sort(new Comparator<BoardGame>() {

                                    private HashMap<BoardGame, Double> cache = new HashMap<BoardGame, Double>();

                                    @Override
                                    public int compare(BoardGame lhs, BoardGame rhs) {
                                        if(!cache.containsKey(lhs)) {
                                            cache.put(lhs,Utilities.magicScore(lhs,players, userRating,maxTime));
                                        }
                                        if(!cache.containsKey(rhs)) {
                                            cache.put(rhs,Utilities.magicScore(rhs,players, userRating,maxTime));
                                        }

                                        double left = cache.get(lhs);
                                        double right = cache.get(rhs);
                                        return (left - right) > 0.0 ? 1 : -1;
                                    }
                                }, false);
                                break;
                            }
                            case 1: {
                                games = games.sort(Utilities.rankComparator, true);
                                break;
                            }
                            case 2: {
                                games = games.shuffle();
                                break;
                            }
                        }

                        //System.out.println("TOP GAME: " + games.get(0));

                        return games;
                    }

                    @Override
                    public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                        // create title
                        String sTitle = Utilities.createTitle(players,maxTime,weights,(String)spBGClass.getSelectedItem(),isRequiredCats,selectedCats,isRequiredMechs,selectedMechs);

                        // go to list view
                        Utilities.startGameListActivity(getActivity(), sTitle, Utilities.getIds(loadedGames));
                    }
                };
                loaderTask.execute();
            }
        });
    }

    public void updateSpinners() {
        final String username = Utilities.getUsername(getActivity());

        if(username == null || username.isEmpty() || (lastUsername != null && lastUsername.equals(username))){
            System.out.println("NOT UPDATING SPINNERS");
            return;
        }
        System.out.println("UPDATING SPINNERS!");

        backgroundLoader = new AsyncTask<Object,Void,Boolean>(){

            @Override
            protected Boolean doInBackground(Object... params) {
                try {
                    cache.getCollection(username).keySet();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UserNotFoundException e){
                    return false;
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);

                if(success == false){
                    Toast.makeText(getActivity(), getString(R.string.error_username), Toast.LENGTH_LONG).show();
                }else{
                    // data
                    final AdvancedList<String> categories = cache.getBoardGameLinkNames(username, BoardGame.LinkTypes.CATEGORY);
                    final AdvancedList<String> mechanics = cache.getBoardGameLinkNames(username, BoardGame.LinkTypes.MECHANIC);

                    updateSpinners(categories,mechanics,new AdvancedList<String>("ANY").union(cache.getBoardGameClasses()));
                    lastUsername = username; // so we don't update unnecessarily
                }
            }
        };
        backgroundLoader.execute();
    }

    private void updateSpinners(List<String> cats, List<String> mechs, List<String> types){
        spCategories.setItems(cats);
        spMechanics.setItems(mechs);

        // categories
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, types);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBGClass.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateSpinners();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lastUsername = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
