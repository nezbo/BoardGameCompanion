package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.GamesScrollListener;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class InfiniteBoardGameListActivity extends Activity {

    private BoardGameAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boardgame_list);

        // Get list of game ids to show
        String title = getIntent().getExtras().getString("title","Board Games");
        AdvancedList<Integer> gameIds = Utilities.arrayToAdvanced(getIntent().getIntArrayExtra(BoardGameListActivity.BOARD_GAME_IDS_EXTRA));

        TextView tvTitle = (TextView) findViewById(R.id.tvListTitle);
        ListView lvGames = (ListView) findViewById(R.id.lvListGames);

        tvTitle.setText(title);

        this.adapter = new BoardGameAdapter(this, R.layout.board_game_list_item, new AdvancedList<BoardGame>(), new BGGAndroidCache(this)) {
            @Override
            public void populateView(View root, BoardGame currentGame, int position, BGGAndroidCache cache) {
                Utilities.populateBoardGameView(InfiniteBoardGameListActivity.this, root, currentGame, position, cache);
            }
        };
        lvGames.setOnScrollListener(new GamesScrollListener(gameIds, 10));
        lvGames.setAdapter(adapter);
    }

}
