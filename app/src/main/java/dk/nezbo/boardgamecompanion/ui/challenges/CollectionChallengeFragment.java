package dk.nezbo.boardgamecompanion.ui.challenges;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.views.RectangleLayout;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.CollectionGameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * This Fragment displays a Collection Challenge with the full collection of the user displayed.
 * The games which are not marked as played at least once will be grayed out and faded out, the
 * others are in full color.
 *
 * If manual logging the user can tap a game to toggle it played/unplayed and the stats in the
 * top will be updated accordingly. If automatic logging it will poll the user's plays from BGG and
 * toggle the games which have been played at least once in the time period on.
 */
public class CollectionChallengeFragment extends Fragment {

    private static final String CHALLENGE_ID = "challenge_id";
    private static final int GAMES_PER_ROW = 5;

    private int challenge_id;
    private BGGAndroidCache dao;
    public GamesLoaderTask loaderTask = null;
    public GamesLoaderTask populateTask = null;
    public AsyncTask<Void, Void, AdvancedList<Play>> asynctask = null;

    private CompanionDAO.Challenge challenge;
    private AdvancedList<BoardGame> collection;

    private TextView tvFraction, tvTimeLeft, tvTimeTotal;
    private ArrayList<ImageView> gameViews;

    /**
     * The factory method creates a new instance of this fragment with the given Challenge id.
     * @param challenge_id The id of the Collection Challenge, must be of the correct type.
     * @return An instantiated CollectionChallengeFragment which has received the Challenge id.
     */
    public static CollectionChallengeFragment newInstance(int challenge_id) {
        CollectionChallengeFragment fragment = new CollectionChallengeFragment();
        Bundle args = new Bundle();
        args.putInt(CHALLENGE_ID, challenge_id);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * The obligatory empty constructor for Fragments.
     */
    public CollectionChallengeFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            challenge_id = getArguments().getInt(CHALLENGE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challenge_unique, container, false);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dao == null) {
            dao = new BGGAndroidCache(getActivity());
        }

        this.populateTask = new GamesLoaderTask(getActivity(), true) {

            @Override
            public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                return cache.getCollection(Utilities.getUsername(getActivity())).keySet();
            }

            @Override
            public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                challenge = cache.getChallenge(challenge_id);
                collection = loadedGames.filter(new AdvancedList.Filter<BoardGame>() {
                    @Override
                    public boolean filter(BoardGame item) {
                        return !item.isExpansion();
                    }
                });

                int games = collection.size();
                int inLastRow = games % CollectionChallengeFragment.GAMES_PER_ROW;
                int rows = (games / CollectionChallengeFragment.GAMES_PER_ROW);
                if(inLastRow > 0) {
                    rows++;
                } else {
                    inLastRow = 5;
                }

                // Stats
                tvFraction = (TextView) getView().findViewById(R.id.tvGamesFrac);
                tvTimeTotal = (TextView) getView().findViewById(R.id.tvTimeTotal);
                tvTimeLeft = (TextView) getView().findViewById(R.id.tvTimeLeft);

                // Layout
                RectangleLayout cont = (RectangleLayout) getView().findViewById(R.id.rctlChallengeContainer);
                cont.removeAllViews();
                cont.setRelativeHeight(rows);
                cont.setRelativeWidth(CollectionChallengeFragment.GAMES_PER_ROW);
                cont.setWeightSum(rows);

                gameViews = new ArrayList<ImageView>(games);

                for (int row = 0; row < rows; row++) {
                    LinearLayout rowLL = new LinearLayout(getActivity());
                    rowLL.setOrientation(LinearLayout.HORIZONTAL);
                    rowLL.setWeightSum(cont.getRelativeWidth());
                    rowLL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

                    // The games in the row
                    int gamesInRow = (row < rows-1) ? CollectionChallengeFragment.GAMES_PER_ROW : inLastRow;
                    for (int i = 0; i < gamesInRow ; i++) {
                        ImageView b = Utilities.makeGameIcon(getActivity(), false, null);
                        rowLL.addView(b);
                        gameViews.add(b);
                    }

                    cont.addView(rowLL);
                }

                refreshChallengeGames();
            }
        };
        populateTask.execute();
    }

    /**
     * Refreshes the games given in the variable arguments.
     * @param indices The indices (position in games list) to update. If none given all games will
     *                be updated.
     */
    public void refreshChallengeGames(final int... indices) {
        // Updated differently depending on manual or automatic logged plays.
        if (challenge.isManual()) {
            if(loaderTask != null) {
                loaderTask.cancel(true);
                loaderTask = null;
            }
            loaderTask = new GamesLoaderTask(getActivity(), false) {
                @Override
                public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                    return cache.getCollectionChallengeGames(challenge_id);
                }

                @Override
                public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                    Collection<Integer> collectionChallengeGames = cache.getCollectionChallengeGames(challenge_id);
                    for(int i = 0; i < ((indices.length > 0) ? indices.length : gameViews.size()); i++) {
                        final int index = indices.length > 0 ? indices[i] : i;
                        final int gameId = collection.get(index).getId();

                        final boolean selected = collectionChallengeGames.contains(gameId);
                        new CollectionGameImagePopulator(gameViews.get(index), gameId, selected, indices.length > 0, dao).execute();

                        gameViews.get(index).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(selected) {
                                    dao.removeCollectionChallengeGame(challenge_id, gameId);
                                } else {
                                    dao.putCollectionChallengeGame(challenge_id, gameId);
                                }
                                refreshChallengeGames(index);
                            }
                        });
                    }

                    // Calculate progress
                    updateStats(loadedGames);
                }
            };
            loaderTask.execute();

        } else {
            this.asynctask = new AsyncTask<Void, Void, AdvancedList<Play>>() {
                @Override
                protected AdvancedList<Play> doInBackground(Void... params) {
                    try {
                        final boolean inclIncomplete = Utilities.prefInclIncomplete(getActivity());
                        String username = Utilities.getUsername(getActivity());

                        final AdvancedList.Filter thisYearFilter = new Utilities.IsThisYearFilter(challenge.getStart(),Utilities.prefInclIncomplete(getActivity()));
                        AdvancedList<Play> plays = dao.getPlays(username);
                        final Set<Integer> collection = dao.getCollection(username).keySet();

                        boolean allTime = challenge.getStart() == CompanionDAO.Challenge.ChallengeStart.All_Time;
                        AdvancedList<Play> finalPlays = null;
                        if(allTime) {
                            finalPlays = plays;
                        } else {
                            finalPlays = plays.filter(thisYearFilter);
                        }

                        finalPlays = finalPlays.filter(new AdvancedList.Filter<Play>() {
                            @Override
                            public boolean filter(Play item) {
                                return (inclIncomplete || !item.isIncomplete()) && collection.contains(item.getGameID());
                            }
                        }).sort(Utilities.timeComparator).distinctBy(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        });

                        // Preload the Games
                        final HashMap<Integer, BoardGame> gamesForPlays = Utilities.getGamesForPlays(finalPlays, dao);

                        return finalPlays;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(final AdvancedList<Play> plays) {
                    super.onPostExecute(plays);

                    for(int i = 0; i < gameViews.size(); i++) {
                        if(i < collection.size()) { // Should be
                            final int finalI = i;
                            AdvancedList<Play> matches = plays.filter(new AdvancedList.Filter<Play>() {
                                @Override
                                public boolean filter(Play item) {
                                    return collection.get(finalI).getId() == item.getGameID();
                                }
                            });
                            final Play match = matches.isEmpty() ? null : matches.get(0);
                            new CollectionGameImagePopulator(gameViews.get(i), collection.get(i).getId(), match != null, indices.length > 0, dao).execute();

                            if(match != null) {
                                gameViews.get(i).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Toast.makeText(CollectionChallengeFragment.this.getActivity(), match.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } else { // Make sure resource is "unknown"
                            gameViews.get(i).setImageResource(R.drawable.unselected);
                        }
                    }

                    // Calculate progress
                    try {
                        updateStats(dao.getBoardGames(plays.map(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        })));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            asynctask.execute();
        }
    }

    /**
     * Updates the tree statistics displayed at the top of the Challenge.
     * @param playedGames A list of the games in the collection which have been played.
     */
    private void updateStats(AdvancedList<BoardGame> playedGames) {
        int totalTime = 0;
        int playedTime = 0;
        for(BoardGame g : collection) {
            totalTime += g.getPlayingTime();
        }
        for(BoardGame g : playedGames) {
            playedTime += g.getPlayingTime();
        }

        this.tvTimeTotal.setText(Utilities.formatTime(totalTime));
        this.tvTimeLeft.setText(Utilities.formatTime(totalTime - playedTime));

        this.tvFraction.setText(playedGames.size()+" / "+collection.size());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }
        if (asynctask != null) {
            asynctask.cancel(true);
            asynctask = null;
        }
        if (populateTask != null) {
            populateTask.cancel(true);
            populateTask = null;
        }

        dao.close();
        dao = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
