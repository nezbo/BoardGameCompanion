package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Pair;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Experience;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GameListStarterTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class BoardGameActivity extends Activity {

    private BGGAndroidCache dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_detailed);
        init();
    }

    private void init() {
        // fetch game id from intent
        int id = getIntent().getExtras().getInt("gameId");
        this.dao = new BGGAndroidCache(this);

        // objects
        final TextView title = (TextView)findViewById(R.id.tvGameDetTitle);
        TextView year = (TextView)findViewById(R.id.tvGameDetYear);
        TextView rank = (TextView)findViewById(R.id.tvGameDetRank);
        TextView rating = (TextView)findViewById(R.id.tvGameDetRating);
        TextView userRating = (TextView)findViewById(R.id.tvGameDetUserRating);
        TextView time = (TextView)findViewById(R.id.tvGameDetPlayingTime);
        TextView age = (TextView)findViewById(R.id.tvGameDetSuggAge);
        TextView bgType = (TextView)findViewById(R.id.tvGameDetType);
        TextView weight = (TextView)findViewById(R.id.tvGameDetWeight);
        ImageView image = (ImageView)findViewById(R.id.ivGameDetImage);

        LinearLayout playerOptions = (LinearLayout) findViewById(R.id.llPlayerOptions);
        LinearLayout categoryList = (LinearLayout) findViewById(R.id.llGameDetCategories);
        LinearLayout mechanicList = (LinearLayout) findViewById(R.id.llGameDetMechanics);
        LinearLayout designerList = (LinearLayout) findViewById(R.id.llGameDetDesigners);
        LinearLayout artistList = (LinearLayout) findViewById(R.id.llGameDetArtists);

        ProgressBar legendBest = (ProgressBar) findViewById(R.id.pbLegendBest);
        ProgressBar legendRecomm = (ProgressBar) findViewById(R.id.pbLegendRecomm);
        ProgressBar legendNotRecomm = (ProgressBar) findViewById(R.id.pbLegendNotRecomm);

        // set legends
        legendBest.setProgressDrawable(getResources().getDrawable(R.drawable.greenprogress));
        legendRecomm.setProgressDrawable(getResources().getDrawable(R.drawable.greenprogress));
        legendNotRecomm.setProgressDrawable(getResources().getDrawable(R.drawable.greenprogress));

        try {
            new GameImagePopulator(image,id,dao).execute();

            final BoardGame game = dao.getBoardGames(id).get(0);

            title.setText(game.getName());
            year.setText(game.hasYear() ? String.valueOf(game.getYear()) : "-");
            rank.setText(game.hasRank() ? String.valueOf(game.getRank()) : "-");
            rating.setText(String.valueOf(game.getAvgRating()));
            if(game.hasUserRating())
                userRating.setText("(own "+game.getUserRating()+")");
            time.setText(game.getPlayingTime()+" min");
            age.setText(String.valueOf(game.getMinAge())+" years");
            bgType.setText(game.getBoardGameClass());
            weight.setText(game.getWeightEnum().toString());

            // onClickListeners
            bgType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String sTitle = Utilities.createTitle(-1,-1,null,game.getBoardGameClass(),false,null,false,null);
                    AdvancedList.Filter<BoardGame> filter = new AdvancedList.Filter<BoardGame>() {
                        @Override
                        public boolean filter(BoardGame item) {
                            return item.getBoardGameClass().equals(game.getBoardGameClass());
                        }
                    };

                    new GameListStarterTask(BoardGameActivity.this, true, sTitle, filter, null).execute();
                }
            });

            // num player bars
            playerOptions.removeAllViews();
            HashMap<String, HashMap<Experience, Integer>> options = game.getSuggestedNumPlayers();
            AdvancedList<String> orderedKeys = new AdvancedList<>(options.keySet()).sort(new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return lhs.compareTo(rhs);
                }
            });

            for(String key : orderedKeys){
                HashMap<Experience, Integer> values = options.get(key);
                View v = getLayoutInflater().inflate(R.layout.numplayers_item, playerOptions,false);
                TextView optionTitle = (TextView) v.findViewById(R.id.tvPlayersNumber);
                ProgressBar progress = (ProgressBar) v.findViewById(R.id.pbPlayersProgress);
                progress.setProgressDrawable(getResources().getDrawable(R.drawable.greenprogress));

                optionTitle.setText(key);

                int best = values.get(Experience.Best);
                int rec = values.get(Experience.Recommended);
                int notRec = values.get(Experience.NotRecommended);
                int sum = best + rec + notRec;

                System.out.println(key + ": " + best + "/" + sum);

                progress.setMax(sum);
                progress.setProgress(best);
                progress.setSecondaryProgress(best + rec);


                System.out.println("Actual: " + progress.getProgress() + "/" + progress.getMax());

                playerOptions.addView(v);
            }

            // categories and mechanics
            populateListLinks(categoryList, game, BoardGame.LinkTypes.CATEGORY, new LinkClickedListener() {
                @Override
                public void clicked(TextView view, BoardGame.LinkTypes type, int linkValue, final String linkName) {
                    String title = Utilities.createTitle(-1,-1,null,null,true,new AdvancedList<>(linkName),false,null);
                    AdvancedList.Filter<BoardGame> filter = new AdvancedList.Filter<BoardGame>() {
                        @Override
                        public boolean filter(BoardGame item) {
                            return item.getLinkNames(BoardGame.LinkTypes.CATEGORY).contains(linkName);
                        }
                    };

                    new GameListStarterTask(BoardGameActivity.this,true,title,filter,null).execute();
                }
            });

            populateListLinks(mechanicList, game, BoardGame.LinkTypes.MECHANIC, new LinkClickedListener() {
                @Override
                public void clicked(TextView view, BoardGame.LinkTypes type, int linkValue, final String linkName) {
                    String title = Utilities.createTitle(-1, -1, null, null, false, null, true, new AdvancedList<String>(linkName));
                    AdvancedList.Filter<BoardGame> filter = new AdvancedList.Filter<BoardGame>() {
                        @Override
                        public boolean filter(BoardGame item) {
                            return item.getLinkNames(BoardGame.LinkTypes.MECHANIC).contains(linkName);
                        }
                    };

                    new GameListStarterTask(BoardGameActivity.this, true, title, filter, null).execute();
                }
            });

            // Designers and Artists
            populateListLinks(designerList, game, BoardGame.LinkTypes.DESIGNER, new LinkClickedListener() {
                @Override
                public void clicked(TextView view, final BoardGame.LinkTypes type, int linkValue, final String linkName) {
                    String title = Utilities.createTitle(-1, -1, null, null, false, null, false, null) + " by " + linkName;
                    AdvancedList.Filter<BoardGame> filter = new AdvancedList.Filter<BoardGame>() {
                        @Override
                        public boolean filter(BoardGame item) {
                            return item.getLinkNames(type).contains(linkName);
                        }
                    };

                    new GameListStarterTask(BoardGameActivity.this, true, title, filter, null).execute();
                }
            });

            populateListLinks(artistList, game, BoardGame.LinkTypes.ARTIST, new LinkClickedListener() {
                @Override
                public void clicked(TextView view, final BoardGame.LinkTypes type, int linkValue, final String linkName) {
                    String title = Utilities.createTitle(-1, -1, null, null, false, null, false, null) + " by " + linkName;
                    AdvancedList.Filter<BoardGame> filter = new AdvancedList.Filter<BoardGame>() {
                        @Override
                        public boolean filter(BoardGame item) {
                            return item.getLinkNames(type).contains(linkName);
                        }
                    };

                    new GameListStarterTask(BoardGameActivity.this, true, title, filter, null).execute();
                }
            });

        } catch (IOException e) {
            Utilities.handleIOException(this);
        }
    }

    private void populateListLinks(LinearLayout root, BoardGame game, final BoardGame.LinkTypes type, final LinkClickedListener listener) {
        root.removeAllViews();
        Set<Pair<Integer, String>> links = game.getLinks(type);
        for(final Pair<Integer, String> link : links){
            TextView text = new TextView(this);
            text.setText(link.second);
            text.setGravity(Gravity.LEFT);

            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.clicked((TextView) v, type, link.first, link.second);
                }
            });

            root.addView(text);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        dao.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.delete_me, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    private interface LinkClickedListener {
        void clicked(TextView view, BoardGame.LinkTypes type, int linkValue, String linkName);
    }
}
