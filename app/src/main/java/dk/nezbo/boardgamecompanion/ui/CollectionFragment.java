package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CollectionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CollectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CollectionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private BGGAndroidCache cache;

    private BoardGameAdapter bgAdapter;
    private GamesLoaderTask gamesLoader;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AchievementsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CollectionFragment newInstance(String param1, String param2) {
        CollectionFragment fragment = new CollectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CollectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_collection, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // get cache
        this.cache = new BGGAndroidCache(getActivity());

        // my elements
        final ListView lvCollectionGames = (ListView) getView().findViewById(R.id.lvCollectionGames);
        final EditText etSearch = (EditText)getView().findViewById(R.id.etSearch);
        Button bClear = (Button) getView().findViewById(R.id.bClear);

        // connect everthing
        bgAdapter = new BoardGameAdapter(getActivity(),R.layout.collection_game_item,new AdvancedList<BoardGame>(),cache) {

            @Override
            public void populateView(View root, final BoardGame currentGame, int position, final BGGAndroidCache cache) {
                ImageView image = (ImageView) root.findViewById(R.id.imageView);
                TextView title = (TextView) root.findViewById(R.id.titleView);
                final CheckBox lendOut = (CheckBox) root.findViewById(R.id.cbLendOut);
                final TextView lendTo = (TextView) root.findViewById(R.id.tvLendOut);

                new GameImagePopulator(image,currentGame.getId(),cache).execute();
                title.setText(currentGame.getName());

                // handle lend out
                lendOut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {}
                });

                // Set content
                String personName = cache.getLendOut(Utilities.getUsername(getActivity()),currentGame.getId());
                lendTo.setText(personName != null && !personName.equals("*") ? personName : "");
                lendOut.setChecked(personName != null);

                // SET LISTENERS
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utilities.startGameActivity(getActivity(), currentGame.getId());
                    }
                });

                final CompoundButton.OnCheckedChangeListener checkedListener = new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String text = lendTo.getText().toString();

                        if(isChecked){
                            System.out.println("CHECK! "+text);
                            cache.setLendOut(Utilities.getUsername(getActivity()),currentGame.getId(),text.length() > 0 ? text : "*");
                        }else {
                            System.out.println("UNCHECK! "+text);
                            cache.removeLendOut(Utilities.getUsername(getActivity()),currentGame.getId());
                            if(!text.isEmpty())
                                lendTo.setText("");
                        }
                    }
                };

                lendTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        final EditText editText = new EditText(getActivity());
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

                        builder.setPositiveButton("Done",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = editText.getText().toString();
                                lendTo.setText(name);

                                if(name.isEmpty()){
                                    System.out.println("Empty Text");
                                    if(lendOut.isChecked())
                                        lendOut.setChecked(false);
                                }else{
                                    System.out.println("Text: "+name);
                                    if(lendOut.isChecked()){
                                        checkedListener.onCheckedChanged(lendOut,true);
                                    }else{
                                        lendOut.setChecked(true);
                                    }
                                }
                            }
                        });
                        builder.setView(editText);
                        editText.setText(lendTo.getText());
                        builder.setTitle("Who lend it?");
                        builder.show();
                        Utilities.showKeyboard(editText,getActivity());
                    }
                });

                lendOut.setOnCheckedChangeListener(checkedListener);
            }
        };
        lvCollectionGames.setAdapter(bgAdapter);

        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });

        // load games async
        gamesLoader = new GamesLoaderTask(getActivity(),true){

            @Override
            public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                return cache.getCollection(Utilities.getUsername(getActivity())).keySet();
            }

            @Override
            public AdvancedList<BoardGame> postLoadHandle(AdvancedList<BoardGame> initialGames, BGGAndroidCache cache) {
                AdvancedList<BoardGame> filtered = Utilities.prefCollInclExpansions(getActivity()) ? initialGames : initialGames.filter(new AdvancedList.Filter<BoardGame>() {
                    @Override
                    public boolean filter(BoardGame game) {
                        return game.getType().equals("boardgame");
                    }
                });
                return filtered.sort(new Comparator<BoardGame>() {
                    @Override
                    public int compare(BoardGame lhs, BoardGame rhs) {
                        if(Utilities.prefAlphabetExclude(getActivity())) {
                            return String.CASE_INSENSITIVE_ORDER.compare(Utilities.removeArticles(lhs.getName()),Utilities.removeArticles(rhs.getName()));
                        } else {
                            return String.CASE_INSENSITIVE_ORDER.compare(lhs.getName(),rhs.getName());
                        }
                    }
                });
            }

            @Override
            public void handleGamesInUIThread(final AdvancedList<BoardGame> loadedGames, BGGAndroidCache whatever) {
                updateGames(lvCollectionGames,loadedGames);

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        final String query = s.toString().trim().toLowerCase();
                        AdvancedList<BoardGame> filteredGames = loadedGames.filter(new AdvancedList.Filter<BoardGame>() {
                            @Override
                            public boolean filter(BoardGame game) {
                                return game.getName().trim().toLowerCase().contains(query);
                            }
                        });
                        updateGames(lvCollectionGames,filteredGames);
                    }
                });

                Editable prevText = etSearch.getText();
                if(prevText.length() != 0)
                    etSearch.setText(prevText);
            }
        };
        gamesLoader.execute();
    }

    private void updateGames(ListView listView, AdvancedList<BoardGame> newGames){
        ((BoardGameAdapter)listView.getAdapter()).changeBoardGames(newGames);
        listView.setAdapter(bgAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        if(gamesLoader != null){
            gamesLoader.cancel(true);
            gamesLoader = null;
        }
        cache.close();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
