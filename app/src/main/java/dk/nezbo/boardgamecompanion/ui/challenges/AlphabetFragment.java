package dk.nezbo.boardgamecompanion.ui.challenges;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.CompanionDAO;
import dk.nezbo.boardgamecompanion.ui.views.AutoResizeTextView;
import dk.nezbo.boardgamecompanion.ui.views.RectangleLayout;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class AlphabetFragment extends Fragment {

    private static final String CHALLENGE_ID = "challenge_id";
    private static final char[] letters = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z'};
    private static final char[] digits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    private int challenge_id;

    private BGGAndroidCache dao;
    private CompanionDAO.Challenge challenge;

    private AsyncTask<char[], Void, HashMap<Character, List<Play>>> asynctask;
    private GamesLoaderTask loaderTask;

    private HashMap<Character, List<BoardGameView>> letterRows = new HashMap<>();
    private TextView tvMissing, tvTimeTotal, tvTimeLeft;
    private Set<Character> missingLetters = new HashSet<>(letters.length);
    private char[] chosenChars;

    public static AlphabetFragment newInstance(int challenge_id) {
        AlphabetFragment fragment = new AlphabetFragment();
        Bundle args = new Bundle();
        args.putInt(CHALLENGE_ID, challenge_id);
        fragment.setArguments(args);
        return fragment;
    }

    public AlphabetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            challenge_id = getArguments().getInt(CHALLENGE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_challenge_alphabet, container, false);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dao == null) {
            dao = new BGGAndroidCache(getActivity());
        }
        this.challenge = dao.getChallenge(challenge_id);

        boolean includeDigits = Utilities.prefAlphabetDigits(getActivity());
        this.chosenChars = new char[includeDigits ? letters.length+digits.length : letters.length];
        for(int i = 0; i < letters.length; i++) {
            chosenChars[i] = letters[i];
        }
        if(includeDigits) {
            for(int i = 0; i < digits.length; i++) {
                chosenChars[i+letters.length] = digits[i];
            }
        }
        this.missingLetters.clear();

        // Stats
        this.tvMissing = (TextView) getView().findViewById(R.id.tvChallMissing);
        this.tvTimeTotal = (TextView) getView().findViewById(R.id.tvTimeTotal);
        this.tvTimeLeft = (TextView) getView().findViewById(R.id.tvTimeLeft);

        // Layout
        RectangleLayout cont = (RectangleLayout) getView().findViewById(R.id.rctlChallengeContainer);
        cont.removeAllViews();
        cont.setRelativeHeight(chosenChars.length);
        cont.setRelativeWidth(4);
        cont.setWeightSum(chosenChars.length);

        for (final char c : chosenChars) {
            LinearLayout row = new LinearLayout(getActivity());
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setWeightSum(cont.getRelativeWidth());
            row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

            // The letter
            LinearLayout.LayoutParams weightParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            TextView letter = new AutoResizeTextView(getActivity());
            letter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 100);
            letter.setLayoutParams(weightParams);
            letter.setText(String.valueOf(c));
            letter.setGravity(Gravity.CENTER);
            row.addView(letter);

            List<BoardGameView> games = new ArrayList<>();
            // The images (first empty game, others no image)
            for (int i = 0; i < cont.getRelativeWidth() - 1; i++) {
                ImageView b = null;
                if (i == 0) {
                    b = Utilities.makeGameIcon(getActivity(), true, null);
                } else {
                    b = Utilities.makeGameIcon(getActivity(), false, null);
                }

                row.addView(b);
                games.add(new BoardGameView(b, null));
            }

            // save row to populate async
            letterRows.put(c, games);

            cont.addView(row);
        }

        this.refreshChallengeRows();
    }

    public void refreshChallengeRows(char... params) {
        char[] toUpdate = params.length > 0 ? params : chosenChars;

        if (challenge.isManual()) {
            for (final char c : toUpdate) {
                try {
                    AdvancedList<BoardGame> games = dao.getAlphabetGames(AlphabetFragment.this.challenge_id, c);
                    if (games.isEmpty()) {
                        missingLetters.add(c);
                    } else {
                        missingLetters.remove(c);
                    }

                    List<BoardGameView> views = letterRows.get(c);

                    for (int i = 0; i < views.size(); i++) {
                        ImageView curView = views.get(i).getView();

                        if (i < games.size()) {
                            new GameImagePopulator(curView, games.get(i).getId(), dao).execute();
                            curView.setVisibility(View.VISIBLE);
                            views.get(i).setGame(games.get(i));

                            final int gameid = games.get(i).getId();

                            // listeners for manual entry
                            curView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dao.removeAlphabetGame(challenge_id, c, gameid);
                                    refreshChallengeRows(c);
                                }
                            });

                        } else if (i == games.size()) {
                            curView.setImageResource(R.drawable.unselected);
                            curView.setVisibility(View.VISIBLE);
                            views.get(i).setGame(null);

                            // listeners for manual entry
                            curView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(loaderTask != null) {
                                        loaderTask.cancel(true);
                                        loaderTask = null;
                                    }
                                    loaderTask = Utilities.performSearchForGame(getActivity(), dao, new BoardGameAdapter.OnGameClickListener() {
                                        @Override
                                        public void onClick(BoardGame game, int position) {
                                            dao.setAlphabetGame(challenge_id, c, game.getId());
                                            refreshChallengeRows(c);
                                        }
                                    });
                                }
                            });
                        } else {
                            curView.setVisibility(View.INVISIBLE);
                            views.get(i).setGame(null);
                        }
                    }
                } catch (IOException e) {
                    Utilities.handleIOException(getActivity());
                }
            }

            new AsyncTask<Void,Void,AdvancedList<BoardGame>>() {

                @Override
                protected AdvancedList<BoardGame> doInBackground(Void... params) {
                    try {
                        ArrayList<BoardGame> games = new ArrayList<>();
                        for(char c : chosenChars) {
                            games.addAll(dao.getAlphabetGames(challenge_id, c));
                        }

                        return new AdvancedList<BoardGame>(games);
                    }catch (IOException e) {

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(AdvancedList<BoardGame> boardGames) {
                    super.onPostExecute(boardGames);

                    updateStats(boardGames);
                }
            }.execute();
        } else {
            this.asynctask = new AsyncTask<char[], Void, HashMap<Character, List<Play>>>() {
                @Override
                protected HashMap<Character, List<Play>> doInBackground(char[]... params) {
                    HashMap<Character, List<Play>> result = new HashMap<Character, List<Play>>();
                    try {
                        AdvancedList<Play> plays = dao.getPlays(Utilities.getUsername(getActivity()));
                        plays = Utilities.getValidAlphabetChallengePlays(plays, challenge, getActivity());
                        plays = plays.sort(Utilities.timeComparator);
                        plays = plays.distinctBy(new AdvancedList.Map<Play, Integer>() {
                            @Override
                            public Integer map(Play item) {
                                return item.getGameID();
                            }
                        });

                        final HashMap<Integer, BoardGame> games = Utilities.getGamesForPlays(plays, dao);

                        for (final char c : params[0]) {
                            AdvancedList<Play> forChar = plays.filter(new AdvancedList.Filter<Play>() {
                                @Override
                                public boolean filter(Play item) {
                                    String name = Utilities.prefAlphabetExclude(getActivity()) ? Utilities.removeArticles(games.get(item.getGameID()).getName()) : games.get(item.getGameID()).getName();
                                    return name.startsWith(String.valueOf(c));
                                }
                            });
                            result.put(c, forChar);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return result;
                }

                @Override
                protected void onPostExecute(final HashMap<Character, List<Play>> plays) {
                    super.onPostExecute(plays);

                    for (final Map.Entry<Character, List<Play>> entry : plays.entrySet()) {
                        List<BoardGameView> gameIcons = letterRows.get(entry.getKey());

                        if (entry.getValue().isEmpty()) {
                            missingLetters.add(entry.getKey());
                        } else {
                            missingLetters.remove(entry.getKey());
                        }

                        for (int i = 0; i < gameIcons.size() && i < entry.getValue().size(); i++) {
                            final Play curPlay = entry.getValue().get(i);

                            new GameImagePopulator(gameIcons.get(i).getView(), curPlay.getGameID(), dao).execute();
                            gameIcons.get(i).getView().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(AlphabetFragment.this.getActivity(), curPlay.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    ArrayList<Integer> gathering = new ArrayList<>();
                    for(char c : plays.keySet()) {
                        for(Play p : plays.get(c)) {
                            gathering.add(p.getGameID());
                        }
                    }
                    try {
                        updateStats(dao.getBoardGames(gathering));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            asynctask.execute(toUpdate);
        }
    }

    private void updateStats(AdvancedList<BoardGame> games) {
        if(games == null) {
            this.tvMissing.setText("Unknown");
            this.tvTimeTotal.setText("Unknown");
            this.tvTimeLeft.setText("Unknown");
            return;
        }

        // Missing Letters
        ArrayList<Character> sorted = new ArrayList<>(this.missingLetters);
        Collections.sort(sorted);

        StringBuilder builder = new StringBuilder();
        for (char c : sorted) {
            builder.append(c);
        }

        this.tvMissing.setText(builder.toString());

        // Time
        int totalTime = 0;
        for(int i = 0; i < games.size(); i++) {
            totalTime += games.get(i).getPlayingTime();
        }
        double averageTime = (1.0*totalTime) / games.size();

        this.tvTimeTotal.setText(Utilities.formatTime((int) (averageTime * chosenChars.length)));
        this.tvTimeLeft.setText(Utilities.formatTime((int) (averageTime * missingLetters.size())));
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (asynctask != null) {
            asynctask.cancel(true);
            asynctask = null;
        }
        if(loaderTask != null) {
            loaderTask.cancel(true);
            loaderTask = null;
        }

        dao.close();
        dao = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    // CLASS

    private class BoardGameView {

        private ImageView view;
        private BoardGame game;

        public BoardGameView(ImageView view, BoardGame game) {
            this.game = game;
            this.view = view;
        }

        public ImageView getView() {
            return view;
        }

        public void setView(ImageView view) {
            this.view = view;
        }

        public BoardGame getGame() {
            return game;
        }

        public void setGame(BoardGame game) {
            this.game = game;
        }
    }
}
