/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.nezbo.boardgamecompanion.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.ui.challenges.ChallengeFragment;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * This example illustrates a common usage of the DrawerLayout widget
 * in the Android support library.
 * <p/>
 * <p>When a navigation (left) drawer is present, the host activity should detect presses of
 * the action bar's Up affordance as a signal to open and close the navigation drawer. The
 * ActionBarDrawerToggle facilitates this behavior.
 * Items within the drawer should fall into one of two categories:</p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as
 * list or tab navigation in that a view switch does not create navigation history.
 * This pattern should only be used at the root activity of a task, leaving some form
 * of Up navigation active for activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate
 * parent for Up navigation. This allows a user to jump across an app's navigation
 * hierarchy at will. The application should treat this as it treats Up navigation from
 * a different task, replacing the current task stack using TaskStackBuilder or similar.
 * This is the only form of navigation drawer that should be used outside of the root
 * activity of a task.</li>
 * </ul>
 * <p/>
 * <p>Right side drawers should be used for actions, not navigation. This follows the pattern
 * established by the Action Bar that navigation should be to the left and actions to the right.
 * An action should be an operation performed on the current contents of the window,
 * for example enabling or disabling a data overlay on top of the current content.</p>
 */
public class MainActivity extends FragmentActivity implements CollectionFragment.OnFragmentInteractionListener, ChooseGameFragment.OnFragmentInteractionListener  {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mToolTitles;

    // logic
    private static final int POS_CHOOSE_GAME = 0;
    private static final int POS_CHALLENGE = 1;
    private static final int POS_COLLECTION = 2;

    private HashMap<Class,Fragment> loadedFragments = new HashMap<>();
    private int lastPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_ACTION_BAR);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mToolTitles = getResources().getStringArray(R.array.tool_titles);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mToolTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(Utilities.prefDefaultTool(this));
        }

        // CHECK IF USERNAME HAS BEEN DEFINED

        if (Utilities.getUsername(this).isEmpty()) {
            // get the user to type it in
            Intent i = new Intent(MainActivity.this, UsernameActivity.class);
            startActivityForResult(i, 1337);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        menu.findItem(R.id.action_search).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.action_settings: {
                Intent i = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(i);
                break;
            }
            case R.id.action_search: {
                // get search term
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Search BGG for game");

                final EditText input = new EditText(this);
                alert.setView(input);

                alert.setPositiveButton("Search",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                final String query = input.getText().toString()
                                        .trim();

                                // handle the search
                                new GamesLoaderTask(MainActivity.this, true) {

                                    private AdvancedList<Integer> allGames;

                                    @Override
                                    public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) throws IOException {
                                        this.allGames = new AdvancedList<Integer>(cache.searchBoardGames(query));
                                        return allGames.take(10);
                                    }

                                    @Override
                                    public void handleGamesInUIThread(AdvancedList<BoardGame> loadedGames, BGGAndroidCache cache) {
                                        int[] ids = new int[allGames.size()];
                                        for(int i = 0; i < ids.length; i++) {
                                            ids[i] = allGames.get(i);
                                        }
                                        Utilities.startInfiniteGameListActivity(MainActivity.this, "Results for '"+query+"'", ids);
                                    }
                                }.execute();
                            }
                        });

                alert.show();
                Utilities.showKeyboard(input, this);


                break;
            }
            default: { // if it doesn't match any
                return super.onOptionsItemSelected(item);
            }
        }
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //TODO
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        if(position != lastPosition){
            // update the main content by replacing fragments
            Fragment fragment = getToolFragment(position);
            Bundle args = new Bundle();
            args.putInt("pos", position);
            fragment.setArguments(args);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mToolTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

            lastPosition = position;
        }
    }

    private Fragment getToolFragment(int position){
        //Toast.makeText(this,""+position,Toast.LENGTH_SHORT).show();
        switch (position){
            case POS_CHALLENGE: {
                if(!loadedFragments.containsKey(ChallengeFragment.class))
                    loadedFragments.put(ChallengeHostFragment.class,ChallengeHostFragment.newInstance());
                return loadedFragments.get(ChallengeHostFragment.class);
            }
            case POS_COLLECTION:{
                if(!loadedFragments.containsKey(CollectionFragment.class))
                    loadedFragments.put(CollectionFragment.class,CollectionFragment.newInstance("",""));
                return loadedFragments.get(CollectionFragment.class);
            }
            default:{
                // POS_CHOOSE_GAME
                if(!loadedFragments.containsKey(ChooseGameFragment.class))
                    loadedFragments.put(ChooseGameFragment.class,ChooseGameFragment.newInstance("",""));
                return loadedFragments.get(ChooseGameFragment.class);
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}