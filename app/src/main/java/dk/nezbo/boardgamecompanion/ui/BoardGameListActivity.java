package dk.nezbo.boardgamecompanion.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import dk.nezbo.boardgamecompanion.R;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.database.DBHelper;
import dk.nezbo.boardgamecompanion.utilities.BoardGameAdapter;
import dk.nezbo.boardgamecompanion.utilities.GameImagePopulator;
import dk.nezbo.boardgamecompanion.utilities.GamesLoaderTask;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

public class BoardGameListActivity extends Activity {

    public static final String BOARD_GAME_IDS_EXTRA = "BOARD_GAME_IDS";
    private BGGAndroidCache cache;
    private Thread imageDownloader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boardgame_list);
        cache = new BGGAndroidCache(this);

        String title = getIntent().getExtras().getString("title","Board Games");

        TextView tvTitle = (TextView) findViewById(R.id.tvListTitle);
        final ListView lvGames = (ListView) findViewById(R.id.lvListGames);

        tvTitle.setText(title);

        new GamesLoaderTask(this,false){

            @Override
            public Collection<Integer> boardGamesToDownload(BGGAndroidCache cache) {
                int[] ids = getIntent().getIntArrayExtra(BOARD_GAME_IDS_EXTRA);
                return Utilities.arrayToAdvanced(ids);
            }

            @Override
            public void handleGamesInUIThread(final AdvancedList<BoardGame> loadedGames, BGGAndroidCache whatever) {
                lvGames.setAdapter(new BoardGameAdapter(BoardGameListActivity.this, R.layout.board_game_list_item, loadedGames, cache) {
                    @Override
                    public void populateView(View root, final BoardGame currentGame, int position, BGGAndroidCache cache) {
                        Utilities.populateBoardGameView(BoardGameListActivity.this, root, currentGame, position, cache);
                    }
                });

                imageDownloader = new Thread(){
                    @Override
                    public void run() {
                        try{
                            for(BoardGame game : loadedGames){
                                if(isInterrupted())
                                    break;
                                cache.getBitmap(game.getId(), DBHelper.BoardGameAttribute.thumb_url);
                            }
                            System.out.println("Image download task completed");
                        }catch(NullPointerException e) { /* DB Closed, Whatever */ }
                    }
                };
                imageDownloader.start();
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_board_game_list, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(imageDownloader != null  && !imageDownloader.isInterrupted())
            imageDownloader.interrupt();
        cache.close();
    }
}
