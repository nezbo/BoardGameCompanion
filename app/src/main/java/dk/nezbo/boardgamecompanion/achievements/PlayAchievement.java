package dk.nezbo.boardgamecompanion.achievements;

import bggapi.api.AdvancedList;
import bggapi.model.Play;

/**
 * Created by Emil on 07-06-2015.
 */
public class PlayAchievement extends Achievement{

    private final AdvancedList<Play> matches;

    public PlayAchievement(String title, String description, int[] levels, AdvancedList<Play> matches){
        super(title,description,levels,matches.size());
        this.matches = matches;
    }

    public AdvancedList<Play> getMatches() {
        return matches;
    }
}
