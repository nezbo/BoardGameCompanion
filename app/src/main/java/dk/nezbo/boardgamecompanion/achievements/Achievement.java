package dk.nezbo.boardgamecompanion.achievements;

/**
 *
 * Created by Emil on 06-06-2015.
 */
public abstract class Achievement {

    private final int nextTarget;
    private String title;
    private String description;
    private int progress;
    private int[] levels;

    // calculated
    private final int completedLevel;

    public Achievement(String title, String description, int[] levels, int progress){
        this.title = title;
        this.description = description;
        this.progress = progress;
        this.levels = levels;
        this.completedLevel = calcCompletedLevel();
        this.nextTarget = calcNextTarget();
    }

    // STATS FOR VISUALIZING

    /**
     * Tells how many levels have been completed.
     * @return The integer of how many levels have been completed.
     */
    private int calcCompletedLevel(){
        for(int i = 0; i < levels.length; i++){
            if(progress < levels[i])
                return i;
        }
        // all completed
        return levels.length;
    }

    /**
     * Gets the next target number for the Achievement.
     * @return The next target, or -1 if all has been
     * completed.
     */
    private int calcNextTarget(){
        if(completedLevel == levels.length)
            return -1;
        return levels[completedLevel];
    }

    // BORING GETTERS


    public int getCompletedLevel() {
        return completedLevel;
    }

    public String getDescription() {
        return description;
    }

    public int[] getLevels() {
        return levels;
    }

    public int getNextTarget() {
        return nextTarget;
    }

    public int getProgress() {
        return progress;
    }

    public String getTitle() {
        return title;
    }
}
