package dk.nezbo.boardgamecompanion.achievements;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;

/**
 * Created by Emil on 07-06-2015.
 */
public class GameAchievement extends Achievement{

    private final AdvancedList<BoardGame> matches;

    public GameAchievement(String title, String description, int[] levels, AdvancedList<BoardGame> matches){
        super(title,description,levels,matches.size());
        this.matches = matches;
    }

    public AdvancedList<BoardGame> getMatches() {
        return matches;
    }
}