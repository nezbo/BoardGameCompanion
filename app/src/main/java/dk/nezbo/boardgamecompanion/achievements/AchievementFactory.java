package dk.nezbo.boardgamecompanion.achievements;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import bggapi.api.AdvancedList;
import bggapi.model.BoardGame;
import bggapi.model.Play;
import bggapi.model.Player;
import dk.nezbo.boardgamecompanion.database.BGGAndroidCache;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * Created by Emil on 06-06-2015.
 */
public class AchievementFactory {

    public static AdvancedList<Achievement> getAchievements(AdvancedList<Play> plays, AdvancedList<BoardGame> games, BGGAndroidCache cache){
        ArrayList<Achievement> result = new ArrayList<>();
        final HashMap<Integer,BoardGame> gamesForPlays = Utilities.getGamesForPlays(plays,cache);
        AdvancedList.Map<Play, BoardGame> mapGame = new AdvancedList.Map<Play, BoardGame>() {
            @Override
            public BoardGame map(Play item) {
                return gamesForPlays.get(item.getGameID());
            }
        };

        if(gamesForPlays == null)
            return null;

        result.add(new GameAchievement("Gotta Play 'Em All!", "Play X different games",new int[]{1,10,25,100,500}
                ,plays.mapDistinct(mapGame)
                ));

        result.add(new GameAchievement("The Collector","Own X games",new int[]{10,50,100,500,1000},games));

//        result.add(new GameAchievement("One of Each","Own a game of each 'Type'", new int[]{8},
//                games.groupBy(new AdvancedList.Map<BoardGame, String>() {
//                    @Override
//                    public String map(BoardGame item) {
//                        return item.getBoardGameClass();
//                    }
//                }).map(new AdvancedList.Map<AdvancedList.Group<String, BoardGame>, BoardGame>() {
//                    @Override
//                    public BoardGame map(AdvancedList.Group<String, BoardGame> item) {
//                        return item.getItems().iterator().next();
//                    }
//                })
//                ));

//        result.add(new PlayAchievement("Gaming Buddies","Play X times with the same person", new int[]{5,10,20,40,80},
//                new AdvancedList<>(plays.multiGroupBy(new AdvancedList.Map<Play, Iterable<String>>() {
//                    @Override
//                    public Iterable<String> map(Play item) {
//                        return new AdvancedList<>(item.getPlayers()).map(new AdvancedList.Map<Player, String>() {
//                            @Override
//                            public String map(Player item) {
//                                return item.getName();
//                            }
//                        });
//                    }
//                }).sort(new Comparator<AdvancedList.Group<String, Play>>() {
//            @Override
//            public int compare(AdvancedList.Group<String, Play> lhs, AdvancedList.Group<String, Play> rhs) {
//                return lhs.getItems().size() - rhs.getItems().size();
//            }
//        }, false).get(0).getItems())
//                ));

//        result.add(new GameAchievement("The H-Index","Play X games at least X times each", new int[]{2,5,10,15,20},
//                hIndex(plays,gamesForPlays)
//                ));

        result.add(new GameAchievement("Mr Popular","Own X games from the top 10 ranked games", new int[]{1,3,5,7,10},
                games.filter(new AdvancedList.Filter<BoardGame>() {
                    @Override
                    public boolean filter(BoardGame item) {
                        return item.getRank() < 11;
                    }
                })
                ));

        result.add(new GameAchievement("All-inclusive","Own games allowing up to X players", new int[]{1,3,6,9,12},
                new AdvancedList<>(gamesForPlayers(games))
                ));

        return new AdvancedList<>(result);
    }

    // PRIVAE METHODS FOR SIMPLIFICATIONS

    private static ArrayList<BoardGame> gamesForPlayers(AdvancedList<BoardGame> games){
        ArrayList<BoardGame> result = new ArrayList<>();
        for(int i = 1; i < 13; i++){
            final int finalI = i;
            AdvancedList<BoardGame> filter = games.filter(new AdvancedList.Filter<BoardGame>() {
                @Override
                public boolean filter(BoardGame item) {
                    return item.allowsPlayers(finalI);
                }
            });

            if(filter.isEmpty())
                break;
            result.add(filter.get(0));
        }
        return result;
    }

//    private static AdvancedList<BoardGame> hIndex(AdvancedList<Play> plays, final HashMap<Integer,BoardGame> gamesForPlays){
//        AdvancedList<AdvancedList.Group<BoardGame, Play>> gamesDescOrder = plays.groupBy(new AdvancedList.Map<Play, BoardGame>() {
//            @Override
//            public BoardGame map(Play item) {
//                return gamesForPlays.get(item.getGameID());
//            }
//        }).sort(new Comparator<AdvancedList.Group<BoardGame, Play>>() {
//            @Override
//            public int compare(AdvancedList.Group<BoardGame, Play> lhs, AdvancedList.Group<BoardGame, Play> rhs) {
//                return lhs.getItems().size() - rhs.getItems().size();
//            }
//        }, false);
//
//        // find out where to cut
//        AdvancedList<AdvancedList.Group<BoardGame, Play>> result;
//
//        for(int i = 0; i < gamesDescOrder.size(); i++){
//            if(gamesDescOrder.get(i).getItems().size() < i+1){
//                result = gamesDescOrder.take(i);
//                break;
//            }
//        }
//        // all games have been played more than "number of games" times.
//        result = gamesDescOrder;
//
//        // return the games
//        return result.map(new AdvancedList.Map<AdvancedList.Group<BoardGame,Play>, BoardGame>() {
//            @Override
//            public BoardGame map(AdvancedList.Group<BoardGame, Play> item) {
//                return item.getKey();
//            }
//        });
//    }
}
