package bggapi.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;

import bggapi.model.ProcessingException;
import bggapi.model.UserNotFoundException;


/**
 * Created by Rasmus on 11-02-2015.
 */
public class CollectionParser extends DefaultHandler {
    private final HashMap<Integer,Double> ids;
    private String username;
    public CollectionParser(String username, HashMap<Integer,Double> ids){
        this.ids = ids;
        this.username = username;
    }

    private int lastId = 0;
    private boolean first = true;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (first && qName.equals("errors")){
            throw new UserNotFoundException(username);
        }
        if (first && qName.equals("message")){
            throw new ProcessingException(); //Results in delayed retries
        }
        if (qName.equals("item")){
            lastId = Integer.parseInt(attributes.getValue("objectid"));

        }else if(qName.equals("rating")){
            ids.put(lastId,safeParseDouble(attributes.getValue("value")));
        }
        first = false;
    }

    private double safeParseDouble(String number){
        try{
            return Double.parseDouble(number);
        }catch(NumberFormatException e){
            return 0.0;
        }
    }

}
