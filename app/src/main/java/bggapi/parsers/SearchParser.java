package bggapi.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

import bggapi.model.ProcessingException;
import bggapi.model.UserNotFoundException;


/**
 * Created by Emil on 10-03-2015.
 */
public class SearchParser extends DefaultHandler {
    private final ArrayList<Integer> ids;
    public SearchParser(ArrayList<Integer> ids){
        this.ids = ids;
    }
    boolean first = true;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (first && qName.equals("errors")){
            throw new UserNotFoundException("error");
        }
        if (first && qName.equals("message")){
            throw new ProcessingException(); //Results in delayed retries
        }
        if (qName.equals("item")){
            ids.add(Integer.parseInt(attributes.getValue("id")));
        }
        first = false;
    }

}
