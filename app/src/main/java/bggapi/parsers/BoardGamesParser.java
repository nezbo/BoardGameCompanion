package bggapi.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Collection;

import bggapi.model.BoardGame;
import bggapi.model.Experience;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class BoardGamesParser extends DefaultHandler{

    private Collection<BoardGame> games;

    private BoardGame.Builder builder;
    private StringBuilder stringBuilder;
    private final int POLL_AGE = 1, POLL_NUM_PLAYERS = 2;
    private int poll = 0;
    private String numPlayers;

    public BoardGamesParser(Collection<BoardGame> games){
        this.games = games;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("item")) {
            builder = new BoardGame.Builder();
            builder.setId(Integer.parseInt(attributes.getValue("id")));
            builder.setType(attributes.getValue("type"));
        }
        else if (qName.equals("thumbnail") || qName.equals("image")) stringBuilder = new StringBuilder();
        else if (qName.equals("name") && attributes.getValue("type").equals("primary")) builder.setName(attributes.getValue("value"));
        else if (qName.equals("yearpublished"))     builder.setYear(getAttrInt(attributes,"value",0));
        else if (qName.equals("minplayers"))        builder.setMinPlayers(getAttrInt(attributes, "value",0));
        else if (qName.equals("maxplayers"))        builder.setMaxPlayers(getAttrInt(attributes, "value",0));
        else if (qName.equals("playingtime"))       builder.setPlayingTime(getAttrInt(attributes, "value",0));
        else if (qName.equals("minplaytime"))       builder.setMinPlayingTime(getAttrInt(attributes, "value",0));
        else if (qName.equals("maxplaytime"))       builder.setMaxPlayingTime(getAttrInt(attributes, "value",0));
        else if (qName.equals("minage"))            builder.setMinAge(getAttrInt(attributes, "value",0));
        else if (qName.equals("usersrated"))        builder.setUsersRated(getAttrInt(attributes, "value", 0));
        else if (qName.equals("average"))           builder.setAvgRating(getAttrDouble(attributes,"value",0.0));
        else if (qName.equals("link")){
            String linktype = attributes.getValue("type").substring(9);
            builder.addLink(linktype, getAttrInt(attributes, "id", 0), attributes.getValue("value"));
        }
        else if (qName.equals("poll")){
            if (attributes.getValue("name").equals("suggested_playerage")) poll = POLL_AGE;
            else if (attributes.getValue("name").equals("suggested_numplayers")) poll = POLL_NUM_PLAYERS;
        }
        else if (qName.equals("results") && poll == POLL_NUM_PLAYERS){
            numPlayers = attributes.getValue("numplayers");
        }
        else if (qName.equals("result")){
            if (poll == POLL_AGE){

            }
            else if (poll == POLL_NUM_PLAYERS){
                builder.addSuggestedNumPlayers(numPlayers,
                        Experience.fromString(attributes.getValue("value")),
                        Integer.parseInt(attributes.getValue("numvotes")));
            }
        }
        else if (qName.equals("rank")){
            if(attributes.getValue("type").equals("subtype")){
                builder.setRank(getAttrInt(attributes,"value", Integer.MAX_VALUE));
            }else if(attributes.getValue("type").equals("family")){
                builder.setBoardGameClass(attributes.getValue("friendlyname").replace(" Rank",""));
                builder.setClassRank(getAttrInt(attributes,"value",Integer.MAX_VALUE));
            }
        }
        else if (qName.equals("averageweight")){
            builder.setWeight(getAttrDouble(attributes,"value",0.0));
        }
    }

    private double getAttrDouble(Attributes attributes, String attrName, double defaultValue){
        try{
            return Double.parseDouble(attributes.getValue(attrName));
        }catch(NumberFormatException e){
            return defaultValue;
        }
    }

    private int getAttrInt(Attributes attributes, String attrName, int defaultValue){
        try{
            return Integer.parseInt(attributes.getValue(attrName));
        }catch(NumberFormatException e){
            return defaultValue;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (stringBuilder != null) stringBuilder.append(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("thumbnail")){
            builder.setThumbnailUrl(stringBuilder.toString());
            stringBuilder = null;
        }
        else if (qName.equals("image")) {
            builder.setImageUrl(stringBuilder.toString());
            stringBuilder = null;
        }
        else if (qName.equals("item")){
            games.add(builder.build());
        }
    }
}
