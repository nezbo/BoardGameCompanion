package bggapi.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Collection;

import bggapi.model.Play;
import bggapi.model.Player;


/**
 * Created by Rasmus on 11-02-2015.
 */
public class PlaysParser extends DefaultHandler {
    private final Collection<Play> plays;

    private int total;
    private StringBuilder stringBuilder;

    public PlaysParser(Collection<Play> plays){
        this.plays = plays;
    }

    private Play.Builder playBuilder;
    private Player.Builder playerBuilder;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("play")){
            playBuilder = new Play.Builder();
            playBuilder.setDate(attributes.getValue("date"));
            playBuilder.setQuantity(Integer.parseInt(attributes.getValue("quantity")));
            playBuilder.setDuration(Integer.parseInt(attributes.getValue("length")));
            playBuilder.setLocation(attributes.getValue("location"));
            playBuilder.setIncomplete(parseBoolean(attributes.getValue("incomplete")));
        }
        else if (qName.equals("item")){
            playBuilder.setGameID(Integer.parseInt(attributes.getValue("objectid")));
        }
        else if (qName.equals("player")){
            playerBuilder = new Player.Builder();
            playerBuilder.setUsername(attributes.getValue("username"));
            playerBuilder.setName(attributes.getValue("name"));
            playerBuilder.setColor(attributes.getValue("color"));
            if (attributes.getValue("score") != null && !attributes.getValue("score").equals(""))
                playerBuilder.setScore(safeParseDouble(attributes.getValue("score")));
            playerBuilder.setNewPlayer("1".equals(attributes.getValue("new")));
            playerBuilder.setRating(Double.parseDouble(attributes.getValue("rating")));
            playerBuilder.setWin("1".equals(attributes.getValue("win")));

            playBuilder.addPlayer(playerBuilder.build());
        }
        else if(qName.equals("plays")){
            this.total = Integer.parseInt(attributes.getValue("total"));
        }
        else if(qName.equals("comments")){
            stringBuilder = new StringBuilder();
        }

    }

    private double safeParseDouble(String value){
        try{
            return Double.parseDouble(value);
        }catch (NumberFormatException e){
            System.err.println("NumberFormatException: Unable to parse a double: "+value);
        }
        return -1.0;
    }

    private boolean parseBoolean(String zeroOrOne) {
        switch(zeroOrOne){
            case "0": return false;
            case "1": return true;
        }
        throw new NumberFormatException("Unable to parse boolean from '"+zeroOrOne+"'.");
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (stringBuilder != null) stringBuilder.append(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("play")){
            plays.add(playBuilder.build());
        }else if(qName.equals("comment")){
            playBuilder.setComment(stringBuilder.toString());
            stringBuilder = null;
        }
    }

    public int getTotal() {
        return total;
    }
}
