package bggapi.test;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import bggapi.api.AdvancedList;
import bggapi.api.BoardGameGeek;
import bggapi.model.Play;
import bggapi.model.UserNotFoundException;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class Test {

    public static void main(String[] args){

        //Usage of the cache:
        AdvancedList<Play> plays = null;
        try {
            plays = BoardGameGeek.getPlays("emiljj");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collection<Integer> gameIds = new HashSet<Integer>();
        for (Play play : plays){
            gameIds.add(play.getGameID());
        }
        //Collection<BoardGame> games = BoardGameCache.Get(gameIds);

/*
        long time = System.nanoTime();
        BoardGame game = BoardGameCache.Get(526);
        System.out.println("Time: " + ((System.nanoTime()-time)/1000000.0) + "ms");
        time = System.nanoTime();
        game = BoardGameCache.Get(526);
        System.out.println("Time: " + ((System.nanoTime()-time)/1000000.0) + "ms");
        */

        try {
            /*System.out.println("Fetching plays for emiljj");
            Collection<Play> plays = BoardGameGeek.GetPlays("emiljj");
            Collection<Integer> gameIds = new HashSet<Integer>();
            for (Play play : plays){
                gameIds.add(play.getGameID());
            }
            System.out.println("Fetching " + gameIds.size() + " games for " + plays.size() + " plays");
            Collection<BoardGame> playedBoardGames = BoardGameGeek.GetBoardGame(gameIds);
            System.out.println("Fetching owned games for emiljj");
            Collection<BoardGame> ownedBoardGames = BoardGameGeek.GetCollection("emiljj");
            System.out.println("Found " + ownedBoardGames.size() + " games");
            Collection<BoardGame> notPlayed = new ArrayList<BoardGame>();
            for (BoardGame owned : ownedBoardGames){
                if ("boardgame".equals(owned.getType()))
                    if (!playedBoardGames.contains(owned))
                        notPlayed.add(owned);
            }
            System.out.println("Games owned, but not played ("+notPlayed.size()+") : ");
            for (BoardGame game : notPlayed){
                System.out.println("\t " + game.getName());
            }
            */

            /*Collection<BoardGame> games = BoardGameGeek.GetCollection("emiljj");
            System.out.println("Board games for user " + "emiljj");
            for (BoardGame bg : games){
                System.out.println(bg);
                for (String k : bg.getSuggestedNumPlayers().keySet())
                System.out.println("\t how many think its best to be "+k+": " + bg.getSuggestedNumPlayers().get(k).get(Experience.Best));
            }*/
        } catch (UserNotFoundException e) {

        }
    }
}
