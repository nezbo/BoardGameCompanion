package bggapi.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import bggapi.model.BoardGame;
import bggapi.model.Play;
import bggapi.model.UserNotFoundException;
import bggapi.util.BaseDownloader;

/**
 * Created by Rasmus Boll Greve on 11-02-2015.
 */
public class BoardGameGeek {

    private BoardGameGeek() {} //only static methods

    /**
     * Get the board game collection for a given user
     * @param userName The name of the user
     * @return The users collection
     * @throws UserNotFoundException If the user is not found
     */
    public static HashMap<Integer,Double> getCollection(String userName) throws UserNotFoundException,IOException {
        HashMap<Integer,Double> ids = BaseDownloader.getCollection(userName);
        return ids;
        //return new AdvancedList<BoardGame>(BaseDownloader.getGamesCount(ids));
    }

    /**
     * Get all the plays for a given user
     * @param userNames The name of the user whose plays to fetch
     * @return A collection of plays that the user has on his account
     * @throws UserNotFoundException If the user is not found
     */
    public static AdvancedList<Play> getPlays(String userNames) throws UserNotFoundException,IOException{
        return new AdvancedList<Play>(BaseDownloader.getPlays(userNames));
    }

    /**
     * Fetch the board game data from a given id
     * NOTICE: Calling this method multiple times will result in throttling by the BGG API
     * @param ids The id of the game to fetch information about
     * @return The board game associated with the given id
     */
    public static Collection<BoardGame> getBoardGames(Integer... ids) throws IOException{
        return getBoardGames(Arrays.asList(ids));
    }

    /**
     * Fetch the board game data for some IDs
     * NOTICE: Calling this method multiple times will result in throttling by the BGG API
     * @param ids The id(s) of the game to fetch information about
     * @return The board games associated with the given ids
     */
    public static Collection<BoardGame> getBoardGames(Collection<Integer> ids) throws IOException{
        return BaseDownloader.getGames(ids);
    }

    /**
     * Searching board games on BGG using the query
     * @param query The text to use for searching
     * @return The board games given as matches by BGG
     */
    public static Collection<BoardGame> searchBoardGames(String query) throws IOException{
        return getBoardGames(searchBoardGameIds(query));
    }

    /**
     * Searching board games on BGG using the query. Will only get their
     * ids
     * @param query The text to use for searching
     * @return The board game ids given as matches by BGG
     */
    public static ArrayList<Integer> searchBoardGameIds(String query) throws IOException{
        return BaseDownloader.getSearch(query);
    }

    // PRIVATE HELPER METHODS

    private static String flattenUserNames(String... userNames) throws UserNotFoundException{
        StringBuilder sb = new StringBuilder();
        for (String s : userNames){
            if (!bggapi.util.Util.validUsername(s)) throw new UserNotFoundException(s);
            sb.append(s).append(',');
        }
        return sb.substring(0,sb.length()-1);
    }

}
