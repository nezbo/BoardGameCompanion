package bggapi.api;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Emil on 12-02-2015.
 */
public class AdvancedList<T> implements List<T>{

    protected List<T> list;

    public AdvancedList(T... ts){
        this.list = new ArrayList<T>();
        for(T item : ts)
            list.add(item);
    }

    public AdvancedList(List<T> list){
        this.list = list;
    }

    public AdvancedList(){
        this.list = new ArrayList<>();
    }

    public AdvancedList(AdvancedList<T> other){
        this.list = new ArrayList<T>(other.list);
    }

    public AdvancedList(Collection<T> collection) {
        this.list = new ArrayList<T>(collection);
    }

    public AdvancedList<T> filter(Filter<T> filter){
        List<T> result = new ArrayList<T>();
        for (T bg : list){
            if (filter.filter(bg))
                result.add(bg);
        }
        return new AdvancedList(result);
    }

    public int count(Filter<T> filter) {
        int result = 0;
        for(T bg : list){
            if (filter.filter(bg))
                result++;
        }
        return result;
    }

    public boolean any(Filter<T> filter) {
        for(T item : list)
            if(filter.filter(item))
                return true;
        return false;
    }

    public boolean all(Filter filter) {
        for(T item : list)
            if(!filter.filter(item))
                return false;
        return true;
    }

    public <V> AdvancedList<V> map(Map<T, V> map){
        List<V> result = new ArrayList<V>();
        for (T bg : list) result.add(map.map(bg));
        return new AdvancedList<>(result);
    }

    public <V> AdvancedList<V> mapDistinct(Map<T, V> map){
        List<V> result = new ArrayList<V>();
        for(T bg : list){
            V cur = map.map(bg);
            if(!result.contains(cur))
                result.add(cur);
        }
        return new AdvancedList<>(result);
    }

    public <K> HashMap<K,T> toHashMap(Map<T,K> map){
        HashMap<K,T> result = new HashMap<>();
        for(T bg : list){
            result.put(map.map(bg),bg);
        }
        return result;
    }

    public <V> HashMap<V, AdvancedList<T>> groupBy(final Map<T, V> groupBy){
        HashMap<V,ArrayList<T>> hashMap = new HashMap<>();
        for(T item : list){
            V key = groupBy.map(item);
            if(!hashMap.containsKey(key))
                hashMap.put(key,new ArrayList<T>());
            hashMap.get(key).add(item);
        }

        HashMap<V,AdvancedList<T>> resultMap = new HashMap<>();
        for(HashMap.Entry<V,ArrayList<T>> entry : hashMap.entrySet()){
            resultMap.put(entry.getKey(), new AdvancedList<T>(entry.getValue()));
        }
        return resultMap;
    }

    public <V> HashMap<V, AdvancedList<T>> multiGroupBy(Map<T,Iterable<V>> groupBy){
        HashMap<V,ArrayList<T>> hashMap = new HashMap<>();
        for(T item : list){
            Iterable<V> keys = groupBy.map(item);
            for(V key : keys){
                if(!hashMap.containsKey(key))
                    hashMap.put(key,new ArrayList<T>());
                hashMap.get(key).add(item);
            }
        }

        HashMap<V,AdvancedList<T>> resultMap = new HashMap<>();
        for(HashMap.Entry<V,ArrayList<T>> entry : hashMap.entrySet()){
            resultMap.put(entry.getKey(), new AdvancedList<T>(entry.getValue()));
        }
        return resultMap;
    }

    public AdvancedList<T> sort(Comparator<T> comparator, boolean ascending){
        ArrayList newList = new ArrayList(list);
        Collections.sort(newList,ascending ? comparator : Collections.reverseOrder(comparator));
        return new AdvancedList(newList);
    }

    public AdvancedList<T> sort(Comparator<T> comparator){
        return sort(comparator,true);
    }

    public AdvancedList<T> take(int amount){
        return new AdvancedList<T>(list.subList(0,list.size() < amount ? list.size() : amount));
    }

    public AdvancedList<T> union(AdvancedList<T> other){
        ArrayList<T> result = new ArrayList<>();
        result.addAll(this);
        result.addAll(other);
        return new AdvancedList<T>(result);
    }

    public AdvancedList<T> shuffle(){
        ArrayList<T> result = new ArrayList<T>(list);
        Collections.shuffle(result);
        return new AdvancedList(result);
    }

    public AdvancedList<T> distinct() {
        ArrayList<T> result = new ArrayList<T>();
        for(T t : list) {
            if(!result.contains(t)) {
                result.add(t);
            }
        }
        return new AdvancedList(result);
    }

    public <V> AdvancedList<T> distinctBy(Map<T, V> map) {
        HashMap<V,T> mapped = new HashMap<>();

        for(T t : list) {
            V v = map.map(t);
            if(!mapped.containsKey(v))
                mapped.put(v,t);
        }
        return new AdvancedList(mapped.values()).sort(new AdvancedList.OriginalOrder<T>(this));
    }

    // List methods

    @Override
    public void add(int location, T object) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean add(T object) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean addAll(int location, Collection<? extends T> collection) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean contains(Object object) {
        return list.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return list.containsAll(collection);
    }

    @Override
    public T get(int index){
        return list.get(index);
    }

    @Override
    public int indexOf(Object object) {
        return list.indexOf(object);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public int lastIndexOf(Object object) {
        return list.lastIndexOf(object);
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator() {
        return list.listIterator();
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator(int location) {
        return list.listIterator(location);
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    @Override
    public T remove(int location) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public T set(int location, T object) {
        throw new UnsupportedOperationException("sorry dude");
    }

    @Override
    public int size() {
        return list.size();
    }

    @NonNull
    @Override
    public List<T> subList(int start, int end) {
        return new AdvancedList<T>(list.subList(start,end));
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @NonNull
    @Override
    public <T1> T1[] toArray(T1[] array) {
        return list.toArray(array);
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("AdvancedList(");
        builder.append(list.size());
        builder.append(")[");
        for(T bg : list){
            builder.append("'");
            builder.append(bg.toString());
            builder.append("', ");
        }
        builder.delete(builder.length()-2,builder.length());
        builder.append("]");
        return builder.toString();
    }

    // OBJECTS

    public interface Filter<T>{
        public boolean filter(T item);
    }

    public interface Map<K,V>{
        public V map(K item);
    }

    public static class Group<V,T> implements Iterable<T>{
        private V key;
        private Collection<T> items;

        public Group(V key, Collection<T> items) {
            this.key = key;
            this.items = items;
        }

        public V getKey() {
            return key;
        }

        public Collection<T> getItems() {
            return items;
        }

        @Override
        public Iterator<T> iterator() {
            return items.iterator();
        }
    }

    private static class OriginalOrder<T> implements Comparator<T> {

        private final AdvancedList<T> original;

        public OriginalOrder(AdvancedList<T> original) {
            this.original = original;
        }

        @Override
        public int compare(T lhs, T rhs) {
            return original.indexOf(lhs) - original.indexOf(rhs);
        }
    }
}
