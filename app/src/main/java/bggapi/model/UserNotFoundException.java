package bggapi.model;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class UserNotFoundException extends RuntimeException {

    private final String username;

    public UserNotFoundException(String username){
        super();
        this.username = username;
    }

    public String getUsername(){
        return username;
    }
}
