package bggapi.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Emil on 05-04-2015.
 */
public enum BoardGameWeight {
    Light("Light"), MediumLight("Medium Light"), Medium("Medium"), MediumHeavy("Medium Heavy"), Heavy("Heavy");

    private final String title;
    BoardGameWeight(String title){
        this.title = title;
    }

    @Override
    public String toString(){
        return title;
    }

    public static BoardGameWeight fromDouble(double value){
        if(value < 1.5){ return Light;
        }else if(value < 2.5) { return MediumLight;
        }else if(value < 3.5) { return Medium;
        }else if(value < 4.5) { return MediumHeavy;
        }else{ return Heavy; }
    }

    public static Set<BoardGameWeight> fromIndices(Collection<Integer> indices){
        BoardGameWeight[] values = BoardGameWeight.values();
        HashSet<BoardGameWeight> result = new HashSet<>();
        for(Integer i : indices){
            result.add(values[i]);
        }
        return result;
    }
}
