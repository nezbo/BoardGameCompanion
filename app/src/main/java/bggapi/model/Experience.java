package bggapi.model;

/**
 * Created by Rasmus on 11-02-2015.
 */
public enum Experience {
    Best, Recommended, NotRecommended;

    public static Experience fromString(String str){
        if (str.toLowerCase().equals("best")) return Best;
        if (str.toLowerCase().equals("recommended")) return Recommended;
        if (str.toLowerCase().equals("not recommended")) return NotRecommended;
        throw new IllegalArgumentException("The argument could not be matched with an experience");
    }

    public static Experience fromInt(int integer){
        switch(integer){
            case 0: return Best;
            case 1: return Recommended;
            case 2: return NotRecommended;
        }
        throw new IllegalArgumentException("The argument could not be matched with an experience");
    }

    public static String toString(Experience current){
        switch (current){
            case Best: return "best";
            case Recommended: return "recommended";
            case NotRecommended: return "not recommended";
        }
        return null;
    }

    public static int toInt(Experience current){
        switch (current){
            case Best: return 0;
            case Recommended: return 1;
            case NotRecommended: return 2;
        }
        return -1;
    }
}
