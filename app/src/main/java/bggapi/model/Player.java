package bggapi.model;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class Player {

    //region Fields
    private final String username;
    private final String name;
    private final String color;
    private final double score;
    private final boolean newPlayer;
    private final double rating;
    private final boolean win;
    //endregion

    private Player(String username, String name, String color, double score, boolean newPlayer, double rating, boolean win) {
        this.username = username;
        this.name = name;
        this.color = color;
        this.score = score;
        this.newPlayer = newPlayer;
        this.rating = rating;
        this.win = win;
    }

    public static class Builder{
        private String username;
        private String name;
        private String color;
        private double score;
        private boolean newPlayer;
        private double rating;
        private boolean win;

        public void setUsername(String username) {
            this.username = username;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public void setNewPlayer(boolean newPlayer) {
            this.newPlayer = newPlayer;
        }

        public void setRating(double rating) {
            this.rating = rating;
        }

        public void setWin(boolean win) {
            this.win = win;
        }

        public Player build(){
            return new Player(username, name, color, score, newPlayer, rating, win);
        }
    }

    //region Getters
    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public double getScore() {
        return score;
    }

    public boolean isNewPlayer() {
        return newPlayer;
    }

    public double getRating() {
        return rating;
    }

    public boolean isWin() {
        return win;
    }
    //endregion


    @Override
    public String toString() {
        return this.getName();
    }
}
