package bggapi.model;

import android.util.Pair;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Rasmus Boll Greve on 11-02-2015.
 */
public final class BoardGame {

    public enum LinkTypes {
        CATEGORY,
        MECHANIC,
        FAMILY,
        EXPANSION,
        DESIGNER,
        ARTIST,
        PUBLISHER,
        COMPILATION,
        INTEGRATION,
        IMPLEMENTATION
    }

    //region Fields
    private final int id;
    private final String name;
    private final String thumbnailUrl;
    private final String imageUrl;
    private final String type;
    private final int year;
    private final int playingTime;
    private final int minPlayers;
    private final int maxPlayers;
    private final int minPlayingTime;
    private final int maxPlayingTime;
    private final int minAge;
    private final int rank;
    private final double avgRating;
    private final int usersRated;
    private final HashMap<String, HashMap<Experience, Integer>> suggestedNumPlayers;
    private final EnumMap<LinkTypes,Set<Pair<Integer,String>>> links;
    private double userRating = 0.0;
    private String boardGameClass;
    private int classRank;
    private double weight;
    //endregion


    public BoardGame(int id, String name, String thumbnailUrl, String imageUrl, String type, int year, int playingTime, int minPlayers, int maxPlayers, int minPlayingTime, int maxPlayingTime, int minAge, int rank, double avgRating, HashMap<String, HashMap<Experience, Integer>> suggestedNumPlayers, EnumMap<LinkTypes,Set<Pair<Integer,String>>> links, String boardGameClass, int classRank, double weight, int usersRated) {
        this.id = id;
        this.name = name;
        this.thumbnailUrl = thumbnailUrl;
        this.imageUrl = imageUrl;
        this.type = type;
        this.year = year;
        this.playingTime = playingTime;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.minPlayingTime = minPlayingTime;
        this.maxPlayingTime = maxPlayingTime;
        this.minAge = minAge;
        this.rank = rank;
        this.avgRating = avgRating;
        this.suggestedNumPlayers = suggestedNumPlayers;
        this.links = links;
        this.boardGameClass = boardGameClass;
        this.classRank = classRank;
        this.weight = weight;
        this.usersRated = usersRated;
    }

    public static class Builder{
        private int id;
        private String name;
        private String thumbnailUrl;
        private String imageUrl;
        private String type;
        private int year;
        private int playingTime;
        private int minPlayers;
        private int maxPlayers;
        private int minPlayingTime;
        private int maxPlayingTime;
        private int minAge;
        private int rank;
        private double avgRating;
        private int usersRated;
        private HashMap<String, HashMap<Experience, Integer>> suggestedNumPlayers = new HashMap<String, HashMap<Experience, Integer>>();
        private EnumMap<LinkTypes,Set<Pair<Integer,String>>> links = new EnumMap<LinkTypes,Set<Pair<Integer,String>>>(LinkTypes.class);
        private String boardGameClass = "";
        private int classRank = Integer.MAX_VALUE;
        private double weight;

        public void setId(int id) {
            this.id = id;
        }

        public void setAvgRating(double avgRating) {
            this.avgRating = avgRating;
        }
        public void setName(String name) {
            this.name = name;
        }
        public void setThumbnailUrl(String thumbnailUrl) {
            this.thumbnailUrl = thumbnailUrl;
        }
        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
        public void setType(String type) {this.type = type; }
        public void setYear(int year) {
            this.year = year;
        }
        public void setPlayingTime(int playingTime) {
            this.playingTime = playingTime;
        }
        public void setMinPlayers(int minPlayers) {
            this.minPlayers = minPlayers;
        }
        public void setMaxPlayers(int maxPlayers) {
            this.maxPlayers = maxPlayers;
        }
        public void setMinPlayingTime(int minPlayingTime) {
            this.minPlayingTime = minPlayingTime;
        }
        public void setMaxPlayingTime(int maxPlayingTime) {
            this.maxPlayingTime = maxPlayingTime;
        }
        public void setMinAge(int minAge) {
            this.minAge = minAge;
        }
        public void setRank(int rank) { this.rank = rank; }
        public void setWeight(double weight){this.weight = weight;}

        public void addLink(String type, int id, String name) {
            LinkTypes enumType = LinkTypes.valueOf(type.toUpperCase());
            if(!links.containsKey(enumType)) {
                links.put(enumType, new HashSet<Pair<Integer,String>>());
            }
            links.get(enumType).add(new Pair<Integer, String>(id, name));
        }

        public void addSuggestedNumPlayers(String numPlayers, Experience exp, int numVotes){
            if (!suggestedNumPlayers.containsKey(numPlayers))
                suggestedNumPlayers.put(numPlayers, new HashMap<Experience, Integer>());
            suggestedNumPlayers.get(numPlayers).put(exp,numVotes);
        }
        public void setUsersRated(int usersRated) {
            this.usersRated = usersRated;
        }

        public BoardGame build(){
            return new BoardGame(id,name, thumbnailUrl, imageUrl, type, year, playingTime, minPlayers, maxPlayers, minPlayingTime, maxPlayingTime, minAge, rank, avgRating, suggestedNumPlayers, links, boardGameClass,classRank, weight, usersRated);
        }

        public void setBoardGameClass(String boardGameClass) {
            this.boardGameClass = boardGameClass;
        }

        public void setClassRank(int value) {
            classRank = value;
        }
    }

    //region Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getType() {return type;}

    public double getAvgRating() {return avgRating;}

    public int getUsersRated() {
        return usersRated;
    }

    public int getYear() {
        return year;
    }

    public int getPlayingTime() {
        return playingTime;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getMinPlayingTime() {
        return minPlayingTime;
    }

    public int getMaxPlayingTime() {
        return maxPlayingTime;
    }

    public int getMinAge() {
        return minAge;
    }

    public int getRank(){ return rank; }

    public Double getUserRating() {
        return userRating;
    }

    public String getBoardGameClass() {
        return boardGameClass;
    }

    public int getClassRank() {
        return classRank;
    }

    public double getWeight() { return weight; }

    public BoardGameWeight getWeightEnum(){ return BoardGameWeight.fromDouble(getWeight()); }

    /**
     * Map of #players -> (experience -> votes)
     */
    public HashMap<String, HashMap<Experience, Integer>> getSuggestedNumPlayers() {
        return suggestedNumPlayers;
    }

    public Set<Pair<Integer,String>> getLinks(LinkTypes type) {
        if(!links.containsKey(type))
            return new HashSet<>();
        return links.get(type);
    }

    public Set<String> getLinkNames(LinkTypes type) {
        HashSet<String> result = new HashSet<>();
        for(Pair<Integer,String> match : getLinks(type)) {
            result.add(match.second);
        }
        return result;
    }

    //endregion


    @Override
    public String toString() {
        return "BoardGame{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", type='" + type + '\'' +
                ", year=" + year +
                ", playingTime=" + playingTime +
                ", minPlayers=" + minPlayers +
                ", maxPlayers=" + maxPlayers +
                ", minPlayingTime=" + minPlayingTime +
                ", maxPlayingTime=" + maxPlayingTime +
                ", minAge=" + minAge +
                ", rank=" + rank +
                ", avgRating=" + avgRating +
                ", suggestedNumPlayers=" + suggestedNumPlayers +
                ", categories=" + links.toString() +
                ", userRating=" + userRating +
                ", boardGameClass='" + boardGameClass + '\'' +
                ", classRank=" + classRank +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardGame boardGame = (BoardGame) o;
        if (id != boardGame.id) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    // AGGREGATE METHODS

    public String getPlayersString() {
        int min = getMinPlayers();
        int max = getMaxPlayers();

        if(min == max){
            return String.valueOf(min);
        }else {
            return min +" to "+max;
        }
    }

    public boolean isExpansion() {
        return !getType().equals("boardgame");
    }

    public void setUserRating(Double userRating) {
        this.userRating = userRating;
    }

    public boolean hasUserRating(){
        return this.getUserRating() > 0.5;
    }

    public boolean hasRank() { return getRank() != Integer.MAX_VALUE; }

    public boolean hasYear() { return getYear() != 0; }

    public boolean allowsPlayers(int numberOfPlayers){
        return numberOfPlayers >= this.getMinPlayers() && numberOfPlayers <= this.getMaxPlayers();
    }
}
