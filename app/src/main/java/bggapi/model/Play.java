package bggapi.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import bggapi.util.Util;
import dk.nezbo.boardgamecompanion.utilities.Utilities;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class Play implements Comparable<Play> {

    //region Fields
    private final int gameID;
    private final Calendar date;
    private final Collection<Player> players;
    private final int quantity;
    private final int duration;
    private final String location;
    private String comment = "";
    private boolean incomplete;

    //private Calendar dateCal = null;
    //endregion

    private Play(int gameID, Calendar date, Collection<Player> players, int quantity, int duration, String location, String comment, boolean incomplete) {
        this.gameID = gameID;
        this.date = date;
        this.players = players;
        this.quantity = quantity;
        this.duration = duration;
        this.location = location;
        this.comment = comment;
        this.incomplete = incomplete;
    }

    @Override
    public int compareTo(Play another) {
        return this.getDate().compareTo(another.getDate());
    }

    public static class Builder{
        private int gameID;
        private Calendar date;
        private Collection<Player> players = new ArrayList<Player>();
        private int quantity;
        private int duration;
        private String location;
        private String comment = "";
        private boolean incomplete;

        public void setGameID(int gameID) {
            this.gameID = gameID;
        }

        public void setDate(Calendar date) {
            this.date = date;
        }

        public void setDate(String date){
            Calendar result = Calendar.getInstance();
            try {
                result.setTime(Utilities.dateformat.parse(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            this.date = result;
        }

        public void addPlayer(Player player) {
            players.add(player);
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Play build(){
            return new Play(gameID, date, players, quantity, duration, location, comment, incomplete);
        }

        public void setIncomplete(boolean incomplete) {
            this.incomplete = incomplete;
        }
    }

    //region Getters

    public int getGameID() {
        return gameID;
    }

    public String getDateString() {
        return Utilities.dateformat.format(date.getTime());
    }

    public Calendar getDate(){
        return date;
    }

    public Collection<Player> getPlayers() {
        return players;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getDuration() {
        return duration;
    }

    public String getLocation() {
        return location;
    }

    public String getComment() {
        return comment;
    }

    //endregion

    public boolean hasPlayers(){
        return players != null && players.size() > 0;
    }

    public boolean hasDuration(){
        return duration > 0;
    }

    public boolean isIncomplete() {
        return incomplete;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("On ");
        builder.append(this.getDateString());
        if(this.hasDuration()) {
            builder.append(" for ");
            builder.append(durationToHours(this.getDuration()/this.getQuantity()));
        }
        if(this.hasPlayers()){
            builder.append(" with ");
            builder.append(Util.listToVerbalString(this.getPlayers()));
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Play play = (Play) o;

        if (duration != play.duration) return false;
        if (gameID != play.gameID) return false;
        if (quantity != play.quantity) return false;
        if (!comment.equals(play.comment)) return false;
        if (!date.equals(play.date)) return false;
        if (!location.equals(play.location)) return false;
        if (!players.equals(play.players)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = gameID;
        result = 31 * result + date.hashCode();
        result = 31 * result + players.hashCode();
        result = 31 * result + quantity;
        result = 31 * result + duration;
        result = 31 * result + location.hashCode();
        result = 31 * result + comment.hashCode();
        return result;
    }

// PRIVATE HELPER METHODS

    private String durationToHours(int minutes){
        int remainder = minutes%60;
        return minutes/60+":"+(remainder < 10 ? "0":"")+remainder;
    }
}
