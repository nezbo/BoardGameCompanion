package bggapi.util;

import java.util.Collection;
import java.util.regex.Pattern;

/**
 * Created by Rasmus on 11-02-2015.
 */
public class Util {

    static Pattern usernamePattern = Pattern.compile("[a-zA-Z0-9_-]*");

    public static boolean validUsername(String username){
        return usernamePattern.matcher(username).matches();
    }

    public static <T> String listToCommaString(Collection<T> ids){
        StringBuilder sb = new StringBuilder();
        for (T id : ids)
            sb.append(id.toString()).append(',');
        return sb.substring(0,sb.length()-1);
    }

    public static <T> String listToVerbalString(Collection<T> items){
        StringBuilder builder = new StringBuilder();
        int i = 0;
        int lastIndex = items.size()-1;
        for(T item : items){
            if(i == items.size() - 1 && items.size() > 1){ // last element
                builder.append(" and ");
            }else if(i > 0){ // the rest
                builder.append(", ");
            }
            builder.append(item.toString());
            i++;
        }
        return builder.toString();
    }
}
