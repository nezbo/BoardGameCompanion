package bggapi.util;

import android.text.TextUtils;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import bggapi.model.BoardGame;
import bggapi.model.Play;
import bggapi.model.ProcessingException;
import bggapi.model.UserNotFoundException;
import bggapi.parsers.BoardGamesParser;
import bggapi.parsers.CollectionParser;
import bggapi.parsers.PlaysParser;
import bggapi.parsers.SearchParser;

/**
 * Created by Emil on 3/10/2015.
 */
public class BaseDownloader {

    private static final int sleepDuration = 800;
    private static final int retries = 3;
    private static final int chunkSize = 100;

    private static final String plays_url = "https://boardgamegeek.com/xmlapi2/plays&username=";
    private static final String search_url = "https://boardgamegeek.com/xmlapi2/search?type=boardgame&query=";
    private static final String collection_url = "https://boardgamegeek.com/xmlapi2/collection?own=1&stats=1&username=";
    private static final String thing_url = "https://boardgamegeek.com/xmlapi2/thing?stats=1&id=";

    public static Collection<Play> getPlays(String username) throws IOException{
        username = URLEncoder.encode(username, "UTF-8");

        Collection<Play> plays = new ArrayList<Play>();
        PlaysParser parser = new PlaysParser(plays);
        download(plays_url+username+"&page=1",parser);

        // check for more pages
        int totalLeft = parser.getTotal() - 100;
        for(int i = 2; totalLeft > 0; i++,totalLeft-=100){
            download(plays_url+username+"&page="+i,parser);
        }
        return plays;
    }

    public static ArrayList<Integer> getSearch(String query) throws IOException{
        ArrayList<Integer> ids = new ArrayList<Integer>();
        try {
            download(search_url + URLEncoder.encode(query, "UTF-8"), new SearchParser(ids));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ids;
    }

    public static HashMap<Integer,Double> getCollection(String username) throws IOException{
        username = URLEncoder.encode(username, "UTF-8");

        HashMap<Integer,Double> ids = new HashMap<Integer,Double>();
        download(collection_url + username,new CollectionParser(username,ids));
        System.out.println(ids.size()+" games found in collection");
        return ids;
    }

    public static Collection<BoardGame> getGames(Collection<Integer> ids) throws IOException{
        Collection<BoardGame> games = new ArrayList<BoardGame>();
        Collection<Integer> chunk = new ArrayList<Integer>();
        for (int id : ids){
            chunk.add(id);
            if (chunk.size() == chunkSize) {
                download(thing_url + Util.listToCommaString(chunk), new BoardGamesParser(games));
                chunk.clear();
            }
        }
        if (chunk.size() != 0) {
            //long before = System.currentTimeMillis();
            download(thing_url + Util.listToCommaString(chunk), new BoardGamesParser(games));
            //System.out.println("Time elapsed: "+(System.currentTimeMillis()-before)/1000.0+" sec");
        }
        return games;
    }

    public static Collection<BoardGame> getGames(HashMap<Integer,Double> idsAndRating) throws IOException{
        Collection<BoardGame> result = getGames(idsAndRating.keySet());
        for(BoardGame game : result){
            game.setUserRating(idsAndRating.get(game.getId()));
        }
        return result;
    }

    // PRIVATE HELPER

    private static  void download(String url, DefaultHandler parser) throws IOException{
        int attempt = 0;
        do {
            InputStream stream = null;
            try {
                stream = new URL(url).openStream();
                InputSource input = new InputSource(stream);
                SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
                SAXParser newSAXParser = saxParserFactory.newSAXParser();
                XMLReader myReader = newSAXParser.getXMLReader();
                myReader.setContentHandler(parser);
                myReader.parse(input);
                attempt = retries; //No need to try any more
            } catch (ProcessingException e) {
                System.err.println("Parsing attempt used");
                attempt++;
                try {
                    Thread.sleep(sleepDuration);
                } catch (InterruptedException e1) {}
            } catch (SAXException|ParserConfigurationException|MalformedURLException e) {
                attempt++;
                //throw new UserNotFoundException(url); //TODO: Something else?
            } finally {
                try {
                    if (stream != null)
                        stream.close();
                } catch (IOException e) {
                }
            }
        } while (attempt < retries);
    }
}
